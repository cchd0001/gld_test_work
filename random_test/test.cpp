#include "Random.h"
#include <iostream>

using namespace Tools;

int main() {
	Random::RandomInt i1(0,1);
	for(int i = 0 ; i < 10 ; i++){
		std::cout<<i1()<<std::endl;
	}
	Random::RandomLongLong i2(10000000000ul,10000000001ul);
	for(int i = 0 ; i < 10 ; i++){
		std::cout<<i2()<<std::endl;
	}
	//Random::RandomFloat i3(0.0,1.0);
	Random::RandomInt i3;
	for(int i = 0 ; i < 10 ; i++){
		std::cout<<i3()<<std::endl;
	}

	std::uniform_int_distribution<int> a;
	int rand = a(Random::GetRandomEngine());
	std::cout<<rand<<std::endl;
	return 0;
}
