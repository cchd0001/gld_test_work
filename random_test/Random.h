#include <random>

#define USE_RANDOM_DEVICE 1

namespace Tools {
namespace Random {

#if USE_RANDOM_DEVICE 
	inline std::random_device & GetRandomEngine() {
		static std::random_device rd;
		return rd;
	}
#else 
	inline std::knuth_b & GetRandomEngine() {
		static std::knuth_b rb;
		return rd;
	}
#endif

	template<class T>
		class Random {
		public:
			using return_type = typename T::result_type ;
			
			Random() : distribution () {} 

			Random(return_type start , return_type end) : distribution(start,end) {}

			return_type operator()() {
				return distribution(GetRandomEngine());
			}
			
		protected:

			T distribution ;
		};

	typedef Random<std::uniform_int_distribution<int>> RandomInt;
	typedef Random<std::uniform_int_distribution<long long>> RandomLongLong;
	typedef Random<std::uniform_real_distribution<float>> RandomFloat;
	typedef Random<std::uniform_real_distribution<double>> RandomDouble;

} // namespace Random
} // namespace Tools
