#include <iostream>
#include <stdint.h>
#include <cassert>
#include <typeinfo>

int main()
{
    uint32_t now = time(0) ;
    float gap = 104 ;
    uint32_t should = now + 104 ;
    uint32_t but = now + gap ;
    assert ( typeid(now+gap) == typeid(float) );
    assert (typeid(now +104) == typeid(uint32_t) ) ;
    assert(but == should);
    return 0 ;
}
