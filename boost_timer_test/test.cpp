#include <boost/timer/timer.hpp>
#include <chrono>
#include <thread>
int main() {
    boost::timer::cpu_timer the_timer;
    do {
        std::cout<<the_timer.elapsed().wall<<std::endl;
        the_timer.stop();
        the_timer.resume();
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }while(1);
    return 0 ;
}

