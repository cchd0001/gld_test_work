#include <iostream>
#include <sstream>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

std::string input("[{\"id\":\"78\",\"stime\":\"1452845040\",\"etime\":\"1452931440\",\"activity_name\":\"活动名字一\",\"activity_description\":\"活动的描述啊\",\"activity_icon\":\"icon4\",\"achieve_conditions\":\"而特特\",\"achieve_description\":\"十多个人皇太后体育教育\",\"reward_1_id\":\"242\",\"reward_1_count\":\"5\",\"reward_2_id\":\"28\",\"reward_2_count\":\"10\",\"reward_3_id\":\"91\",\"reward_3_count\":\"4\",\"reward_4_id\":\"242\",\"reward_4_count\":\"99\",\"reward_5_id\":\"10009\",\"reward_5_count\":\"77\",\"reward_6_id\":\"737\",\"reward_6_count\":\"10\",\"server_id\":\"1\",\"activity_id\":\"1\",\"active_type\":\"2\"},{\"id\":\"79\",\"stime\":\"1452845040\",\"etime\":\"1452931440\",\"activity_name\":\"活动名字一\",\"activity_description\":\"活动的描述啊\",\"activity_icon\":\"icon3\",\"achieve_conditions\":\"疯二哥\",\"achieve_description\":\"十多个人特别\",\"reward_1_id\":\"280\",\"reward_1_count\":\"3\",\"reward_2_id\":\"358\",\"reward_2_count\":\"9\",\"reward_3_id\":\"65\",\"reward_3_count\":\"8\",\"reward_4_id\":\"38\",\"reward_4_count\":\"7\",\"reward_5_id\":\"60\",\"reward_5_count\":\"36\",\"reward_6_id\":\"68\",\"reward_6_count\":\"9\",\"server_id\":\"1\",\"activity_id\":\"1\",\"active_type\":\"2\"},{\"id\":\"80\",\"stime\":\"1452845040\",\"etime\":\"1452931440\",\"activity_name\":\"活动名字一\",\"activity_description\":\"活动的描述啊\",\"activity_icon\":\"icon2\",\"achieve_conditions\":\"gew\",\"achieve_description\":\"各位\",\"reward_1_id\":\"33\",\"reward_1_count\":\"4\",\"reward_2_id\":\"40\",\"reward_2_count\":\"6\",\"reward_3_id\":\"15\",\"reward_3_count\":\"8\",\"reward_4_id\":\"114\",\"reward_4_count\":\"6\",\"reward_5_id\":\"15\",\"reward_5_count\":\"7\",\"reward_6_id\":\"36\",\"reward_6_count\":\"9\",\"server_id\":\"1\",\"activity_id\":\"1\",\"active_type\":\"2\"},{\"id\":\"81\",\"stime\":\"1452845040\",\"etime\":\"1452931440\",\"activity_name\":\"活动名字一\",\"activity_description\":\"活动的描述啊\",\"activity_icon\":\"icon1\",\"achieve_conditions\":\"达成提哦\",\"achieve_description\":\"圣达菲\",\"reward_1_id\":\"6\",\"reward_1_count\":\"2\",\"reward_2_id\":\"20\",\"reward_2_count\":\"3\",\"reward_3_id\":\"100022\",\"reward_3_count\":\"4\",\"reward_4_id\":\"21\",\"reward_4_count\":\"5\",\"reward_5_id\":\"119\",\"reward_5_count\":\"5\",\"reward_6_id\":\"33\",\"reward_6_count\":\"8\",\"server_id\":\"1\",\"activity_id\":\"1\",\"active_type\":\"2\"}]");


int main()
{
    boost::property_tree::ptree m_datas;
    std::stringstream ss;
    ss << input;
    boost::property_tree::read_json(ss, m_datas);
    for(auto & i : m_datas){
        auto & datas = i.second;
        std::cout<<"..............."<<std::endl;
        std::cout<<"id : "<<datas.get<int>("idd")<<std::endl;
    }
    return 0;
}
