#ifndef __REDISCLIENT_H__
#define __REDISCLIENT_H__

#include <mutex>
#include <map>
#include <set>
#include <vector>
#include <string>
#include <list>
#include <sstream>
#include <iostream>
#include <cstdint>

#include <hiredis/hiredis.h>

typedef std::vector<std::string> StrVec;
typedef std::map<std::string,std::string> StrMap;

// Deal nullptr macro for freeReplayObject
#define freeReplyObject_Safe(redis_reply) \
    if( nullptr != redis_reply) {\
        freeReplyObject(redis_reply); \
    }\
    redis_reply = nullptr

class RedisClient
{
public:
    RedisClient();
    void Init(const std::string& ip, const std::string& port);
    ~RedisClient();

    
   /******************************************************
    *@Brief     :
    *              Execute a command and return reply.
    *
    *@Param     :
    *      command --  Command that we want to execute.
    *  expect_type --  Expect REDIS_REPLY_XXX type.  
    *                   REDIS_REPLY_ERROR means all kinds
    *                   of type .
    *
    *@Return    :
    *      nullptr --  Command return error , (nil) or type
    *                   not as expect_type.
    *                  Program  may EXIT while reconnecting.
    *
    *    otherwise --  command successful .
    *
    *****************************************************/
    redisReply * RedoCommand(const std::string & command, int32_t expect_type);


/**************************************************************************
 *
 * Below are interfaces for key - value .
 *
 **************************************************************************/

    // SET command
    template<class T>
    void Set(const std::string & key, const T & value , uint32_t ttl = -1){
        std::stringstream os;
        if(key.empty())
            return ;
        os<<"SET "<<key<<" "<<value;
        if(-1 != ttl){
            os<<" EX "<<ttl;
        }
        redisReply * redis_reply = RedoCommand(os.str(),REDIS_REPLY_STATUS);
        freeReplyObject_Safe(redis_reply);
    }

    // SETEX command
    template<class T>
    void SetEx(const std::string& key
                          , const int duration
                          , const T & value){
        if( key.empty()) 
            return;
        std::stringstream sm;
        sm << "SETEX " << key << " " << duration << " "<<value;

        redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_STATUS);
        freeReplyObject_Safe(redis_reply);
    }
    
    uint32_t      Ttl(const std::string& key); // 返回值：0，无效.
    std::string Get(const std::string& key);
    void        Del(const std::string& key);
    void        Expire(const std::string& key, int seconds);
    StrVec      Keys(const std::string & pattern );
    bool        Exists(const std::string& key);


/************************************************************************
 *
 *  Below are interfaces for hash table .
 *
 ************************************************************************/

    template<class T>
    void HSet(const std::string& map
            , const std::string& key
            , const T& value){
        if ( map.empty() || key.empty() ) {
            return ;
        }
        std::stringstream sm;
        sm<<"HSET "<<map<<" "<<key<<" "<<value;
        redisReply *redis_reply =  RedoCommand(sm.str(),REDIS_REPLY_INTEGER);
        freeReplyObject_Safe(redis_reply);
    }
    
    std::string HGet(const std::string& map, const std::string& key);
    void        HDel(const std::string & name, const StrVec & keys );
    bool        HMSet(const std::string& key, const StrMap& map);
    StrMap      HMGet(const std::string& key, const StrVec& map);
    StrVec      HKeys(const std::string & hask_name);
    
    /************************************
     *@ HExists Return 
     *          1 -- Exists.
     *          0 -- Not exists.
     *         -1 -- Eror happened.
     ************************************/
    int         HExists(const std::string & map, const std::string & key);
    uint32_t      HIncrBy(const std::string&map, const std::string& key, int gap);

  
/************************************************************************
 *
 *  Below are interfaces for Set .
 *
 ************************************************************************/
    void        SRem(const std::string& key, const std::string& value);
    int         SAdd(const std::string& key, const StrVec & value);
    StrVec      SMembers(const std::string& key);
    int         SIsmember(const std::string& key, const std::string& value);

/************************************************************************
 *
 *  Below are interfaces for Ordered Set .
 *
 ************************************************************************/
    void        ZIncrBy(const std::string& key, const int incr, const std::string& member);
    void        ZAdd(const std::string& key, const int score, const std::string& member);
    StrVec      ZRange(const std::string& key, const int start, const int stop); 
    StrVec      ZRevrange(const std::string& key, const int start, const int stop); 
    uint32_t      ZRevrank(const std::string& key,const std::string& member);
    uint32_t      ZScore(const std::string& tbl, const std::string& key);
private:
    bool Connecting();
    bool TryConnecting();

    redisContext * m_redis_conn; // the handler that hiredis provide.
    std::mutex m_conn_mutex; 
    std::string m_ip;
    int m_port;
}; // RedisClient


#include "Singleton.h"
GET_SINGLETON_MGR(RedisClient);

#endif // __REDISCLIENT_H__
