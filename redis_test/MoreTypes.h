#ifndef __MORETYPES_H__
#define __MORETYPES_H__

#include <list>
#include <vector>
#include <map>
#include <mutex>

typedef std::list<std::string>                  StrList;
typedef std::vector<std::string>                StrVec;
typedef std::map<std::string, std::string>      StrMap;
typedef std::lock_guard<std::mutex>				Lock;

#endif // __MORETYPES_H__
