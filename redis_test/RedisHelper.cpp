#include "RedisHelper.h"

#include <cstdlib> // for std::atoi
#include <sstream>
#include <set>
#include <atomic>

#define LOG_TAG "RedisClient"

RedisClient::RedisClient(const std::string& ip, const std::string& port): m_redis_conn(NULL){
    if( ip.empty() || port.empty()) {
        LOGE("RedisClient Error : empty param !");
    }
    m_ip = ip;
    m_port = std::atoi(port.c_str());
    LOGD("RedisClient try to connect redis: " << m_ip << " : " <<  m_port);
    TryConnecting();
}

RedisClient::~RedisClient(){
    if(m_redis_conn) {
        redisFree(m_redis_conn);
    }
}

bool RedisClient::Connecting(){
    std::lock_guard<std::mutex> l(m_conn_mutex);
    if(m_redis_conn) {
        redisFree(m_redis_conn);
    }
    m_redis_conn = redisConnect(m_ip.c_str(), m_port);
    if(NULL == m_redis_conn || m_redis_conn->err) {
        LOGE("Connecting fail connect to redis.");
        if(NULL != m_redis_conn){
            LOGE("Connecting error message: " << (m_redis_conn->errstr));
        }
        return false ;
    } else {
        LOGT("Connecting success connect to redis "<< m_ip << " : " <<  m_port);
    }
    return true;
}

bool RedisClient::TryConnecting(){
    static std::atomic<int> curr_retry(0);
    int zero = 0;
    if( ! std::atomic_compare_exchange_strong(&curr_retry, &zero,1) ){
        return false;
    }
    do{
        LOGT("TryConnecting : try connecting ... "<<curr_retry);
        if(Connecting())
            curr_retry  = 0;
        else{
            curr_retry++;
            if(curr_retry == 10000) { 
                LOGE("Fatal Error : can't connect to redis "<< m_ip << " : " <<  m_port);
                exit(0);
            }
        }
    }while(curr_retry);
    return true;
}

redisReply * RedisClient::RedoCommand(const std::string & command, int32 expect_type){
    if(nullptr == m_redis_conn){
        TryConnecting();
    }
    if( command.empty())
        return nullptr;

    redisReply *redis_reply = nullptr;
    do{
        {
            std::lock_guard<std::mutex> l(m_conn_mutex); // For test
            redis_reply = static_cast<redisReply*>(redisCommand(m_redis_conn, command.c_str() ));
        }
        if( nullptr == redis_reply ){
            TryConnecting();
            continue;
        }
        if( expect_type == REDIS_REPLY_ERROR ){
            break;
        }
        if( REDIS_REPLY_ERROR == redis_reply->type ) {
            LOGE("RedoCommand -- redis error msg="<< redis_reply->str\
                  << ", cmd=" << command);
        }else if ( REDIS_REPLY_NIL == redis_reply->type ) {
            LOGW("RedoCommand -- REDIS_REPLY_NIL! command=" << command);
        }else if ( expect_type == redis_reply->type ){
            if(expect_type != REDIS_REPLY_STATUS )
                break;
            if( redis_reply->str == std::string("OK") )
                break;
            LOGE("RedoCommand -- get status not OK ! command=" << command\
                  <<" got status  "<< redis_reply->str);
        }else{
            LOGE("RedoCommand -- get type wrong ! command=" << command \
                 <<" type "<< redis_reply->type );
        }
        freeReplyObject_Safe(redis_reply);
        break;
    }while(1);
    return redis_reply;
}

//////////////////////////////////////////////////////////////////////
//
//  Key -- Value
//
//////////////////////////////////////////////////////////////////////

uint32 RedisClient::Ttl(const std::string& key){
    uint32 seconds = 0;
    if( key.empty() ) 
        return seconds;
    std::stringstream sm;
    sm << "TTL " << key;
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_INTEGER);
    if ( redis_reply ) {
        long long v = redis_reply->integer;
        if(v > 0) seconds = (uint32)v;
    }
    freeReplyObject_Safe(redis_reply);
    return seconds;
}

std::string RedisClient::Get(const std::string& key){
    std::stringstream sm;
    if(key.empty())
        return std::string("");
    sm << "Get " << key;
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_STRING);
    std::string s("");
    if ( redis_reply ) {
        s = redis_reply->str;
        freeReplyObject_Safe(redis_reply);
    }
    return s;
}

void RedisClient::Del(const std::string& key){
    if(key.empty())
        return;
    std::stringstream sm;
    sm << "DEL " << key;

    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_INTEGER);
    if(redis_reply->integer < 1){
        LOGW("DEL command failed : "<<sm.str());
    }
    freeReplyObject_Safe(redis_reply);
}

void RedisClient::Expire(const std::string& key, int seconds){
    if(key.empty())
        return;
    std::stringstream sm;
    sm << "EXPIRE " << key << " " << seconds; 
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_INTEGER);
    if(redis_reply->integer != 1){
        LOGW("EXPIRE command failed : "<<sm.str());
    }
    freeReplyObject_Safe(redis_reply);
}

StrVec RedisClient::Keys(const std::string & pattern ){
    StrVec vec;
    if(pattern.empty())
        return vec;
    std::stringstream sm;
    sm << "KEYS " << pattern;
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_ARRAY);
    if ( redis_reply ) {
        for (size_t i=0; i<redis_reply->elements; ++i) {
            auto reply = redis_reply->element[i];
            if (reply->type == REDIS_REPLY_STRING) {
                vec.push_back(reply->str);
            }
        }
    }
    freeReplyObject_Safe(redis_reply);
    return vec;
}

bool RedisClient::Exists(const std::string& key){
    bool flag = true;
    if( key.empty() )
        return flag;
    std::stringstream sm;
    sm << "EXISTS " << key;
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_INTEGER);
    if ( redis_reply ) {
        long long v = redis_reply->integer;
        if(v == (long long)0)  flag = false;
    }
    freeReplyObject_Safe(redis_reply);
    return flag; 
}

//////////////////////////////////////////////////////////////////////
//
//  Hash
//
//////////////////////////////////////////////////////////////////////

std::string RedisClient::HGet(const std::string& key, const std::string& field){
    std::string s;
    if(key.empty() || field.empty()){
        return s;
    }
    std::stringstream sm;
    sm << "HGet " << key << " " << field;
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_STRING);
    if ( redis_reply ) {
        s = redis_reply->str;
    }
    freeReplyObject_Safe(redis_reply);
    return s;
}

void RedisClient::HDel(const std::string & hask_name, const StrVec & keys){
    if( keys.empty() ) {
        LOGE("HDEL warning : keys is empty vector");
        return ;
    }
    std::stringstream sm;
    sm << "HDEL "<< hask_name ;
    bool avaliad_param = false ;
    for( const auto & key : keys ) {
        if(key.empty()) 
            continue;
        sm<<" "<<key;
        avaliad_param = true ;

    }
    if( !avaliad_param ) {
        LOGE("HDEL warning :all key are empty string .keys has " <<keys.size());
        return ;
    }
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_INTEGER);
    freeReplyObject_Safe(redis_reply);
}

StrVec RedisClient::HKeys(const std::string & hask_name){
    StrVec vec;
    if(hask_name.empty())
        return vec;
    std::stringstream sm;
    sm << "HKEYS " << hask_name;
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_ARRAY);
    if ( redis_reply ) {
        for (size_t i=0; i<redis_reply->elements; ++i) {
            auto reply = redis_reply->element[i];
            if (reply->type == REDIS_REPLY_STRING) {
                vec.push_back(reply->str);
            }
        }
    }
    freeReplyObject_Safe(redis_reply);
    return vec;
}

int RedisClient::HExists(const std::string & map , const std::string & key){
    if(map.empty() || key.empty()){
        LOGE("HExists param error : map = "<<map<<" key= "<<key);
        return -1;
    }
    std::stringstream sm;
    sm<<"HEXISTS "<<map<<" "<<key;
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_INTEGER);
    if( redis_reply ) {
        long long v = redis_reply->integer;
        if(v == (long long)0)  return 0;
        return 1;
    }
    freeReplyObject_Safe(redis_reply);
    return -1;
}

uint32 RedisClient::HIncrBy(const std::string&map, const std::string& key, int gap){
    uint32 times = 0;
    if( key.empty() || map.empty() )
        return times;
    std::stringstream sm;
    sm << "HINCRBY "<<map<< " "<<key<<" "<< gap;
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_INTEGER);
    if ( redis_reply ) {
        long long v = redis_reply->integer;
        times = (v > 0) ? v : 0;
    }
    freeReplyObject_Safe(redis_reply);
    return times;
}

//////////////////////////////////////////////////////////////////////
//
//  Set
//
//////////////////////////////////////////////////////////////////////

void RedisClient::SRem(const std::string& key, const std::string& value){
    if (value.empty() || key.empty()) return;
    std::stringstream sm;
    sm << "SREM " << key << " " << value;
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_INTEGER);
    freeReplyObject_Safe(redis_reply);
}

int RedisClient::SAdd(const std::string& key, const StrVec& value){
    int result = 0;
    if(key.empty() || value.empty()) 
        return result;
    std::stringstream sm;
    sm << "SADD " << key ;
    bool valid_param = false;
    for(auto & a : value) {
        if(a.empty())
            continue;
        valid_param = true;
        sm<<" "<<a;
    }
    if(!valid_param){
        LOGW("SAdd got zero valid param for set : "<<key);
        return 0;
    }
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_INTEGER);
    if ( redis_reply ) {
        result = redis_reply->integer;
    }
    freeReplyObject_Safe(redis_reply);
    return result;
}

StrVec RedisClient::SMembers(const std::string& key){
    StrVec vec;
    if(key.empty()) 
        return vec;
    std::stringstream sm;
    sm << "SMEMBERS " << key;
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_ARRAY);
    if ( redis_reply ) {
        for (size_t i=0; i<redis_reply->elements; ++i) {
            auto reply = redis_reply->element[i];
            if (reply->type == REDIS_REPLY_STRING) {
                vec.push_back(reply->str);
            }
        }
    }
    freeReplyObject_Safe(redis_reply);
    return vec;
}

int RedisClient::SIsmember(const std::string& key, const std::string& value){
    if (key.empty() || value.empty()) return 0;
    int result = 0;
    std::stringstream sm;
    sm << "SISMEMBER " << key << " " << value;
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_INTEGER);
    if ( redis_reply ) {
        result = (int)redis_reply->integer;
    }
    freeReplyObject_Safe(redis_reply);
    return result;
}

bool RedisClient::HMSet(const std::string& key, const StrMap& map){
    if( key.empty() || map.empty()) {
        return true;
    }
    std::stringstream sm;
    sm << "HMSET " << key <<" ";
    bool valid_param = false;
    for(auto & pair : map) {
        if ( pair.first.empty() || pair.second.empty())
            continue;
        sm << " " << pair.first<< " " << pair.second;
        valid_param = true;
    }
    if(!valid_param) {
        LOGW("HMSET got zero valid param for : "<<key);
        return true;
    }
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_STATUS);
    if( redis_reply ) {
        freeReplyObject_Safe(redis_reply);
        return true;
    }
    return false;
}

StrMap RedisClient::HMGet(const std::string& key, const StrVec& vec){
    StrMap map;
    if( key.empty() || vec.empty()) {
        return map;
    }
    std::stringstream sm;
    sm << "HMGET " << key;
    for(auto & i : vec) {
        sm << " " << i;
    }
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_ARRAY);
    if( redis_reply ) {
        for (size_t i=0; i<redis_reply->elements; ++i) {
            auto reply = redis_reply->element[i];
            if (reply->type == REDIS_REPLY_STRING) {
                map[vec[i]] = redis_reply->element[i]->str;
            }
            else if (reply->type == REDIS_REPLY_NIL) {
                LOGW("HMGet element" << vec[i] <<" is REDIS_REPLY_NIL! command=");
                map[vec[i]] ="";
            }
        }
    }
    freeReplyObject_Safe(redis_reply);
    return map;
}

uint32 RedisClient::ZScore(const std::string& tbl, const std::string& key){
    if(tbl.empty() || key.empty())
        return 0;
    std::stringstream sm;
    sm << "ZSCORE " << tbl << " " << key;
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_STRING) ;
    std::string s;
    if ( redis_reply ) {
        s = redis_reply->str;
    }
    freeReplyObject_Safe(redis_reply);
    return (uint32)atoi(s.c_str());
}

void RedisClient::ZIncrBy(const std::string& key, const int incr, const std::string& member){
    if(key.empty() || member.empty())
        return;
    std::stringstream sm;
    sm << "ZINCRBY " << key << " " << incr << " " << member; 
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_STRING);
    freeReplyObject_Safe(redis_reply);
}

void RedisClient::ZAdd(const std::string& key, const int score, const std::string& member){
    if(key.empty() || member.empty())
        return;
    std::stringstream sm;
    sm << "ZADD " << key << " " << score << " " << member; 
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_INTEGER);
    freeReplyObject_Safe(redis_reply);
}

StrVec RedisClient::ZRange(const std::string& key, const int start, const int stop){
    StrVec vec;
    if(key.empty())
        return vec;
    std::stringstream sm;
    sm << "ZRANGE " << key << " " << start << " " << stop;
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_ARRAY);
    if ( redis_reply ) {
        for (uint32 i=0 ; i<redis_reply->elements; i++ ) {
            auto reply = redis_reply->element[i];
            if (reply!=NULL && reply->type==REDIS_REPLY_STRING) {
                vec.push_back(reply->str);
            }
        }
    }
    freeReplyObject_Safe(redis_reply);
    return vec;
}

StrVec RedisClient::ZRevrange(const std::string& key, const int start, const int stop){
    StrVec vec;
    if(key.empty())
        return vec;
    std::stringstream sm;
    sm << "ZREVRANGE " << key << " " << start << " " << stop;
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_ARRAY);
    if ( redis_reply ) {
        for (uint32 i=0 ; i<redis_reply->elements; i++ ) {
            auto reply = redis_reply->element[i];
            if (reply!=NULL && reply->type==REDIS_REPLY_STRING) {
                vec.push_back(reply->str);
            }
        }
    }
    freeReplyObject_Safe(redis_reply);
    return vec;
}

uint32 RedisClient::ZRevrank(const std::string& key,const std::string& member){
    uint32 ret(-1);
    if(key.empty() || member.empty())
        return ret;
    std::stringstream sm;
    sm << "ZREVRANK " << key << " " << member;
    redisReply *redis_reply = RedoCommand(sm.str(),REDIS_REPLY_INTEGER);
    if ( redis_reply ) {
        ret = redis_reply->integer;
    }
    freeReplyObject_Safe(redis_reply);
    return ret;
}
