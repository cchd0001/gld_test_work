#include "RedisHelper.h"
#include <chrono>

struct ProfileTimer
{
    ProfileTimer(const char* name) : _name(name), _startTime(std::chrono::high_resolution_clock::now())
    {
        LOGD( "enter " << _name);
    }
    ~ProfileTimer()
    {
        auto _endTime = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(_endTime-_startTime).count();
        LOGD("leave " << _name << " " << duration << "us.");
    }
    const char *_name;
    std::chrono::high_resolution_clock::time_point _startTime;
};

int main()
{
    RedisClient r_client("127.0.0.1","6379");
    RedisClient r_client_slave("127.0.0.1","6479");
    {
        ProfileTimer t("redis ");
        for(int i = 0 ; i < 10000 ; i++){
            uint32 prev = std::stoi(r_client_slave.Get("r_client").c_str());
            r_client.Set("r_client",prev+1);
        }
    }
    
    {
        ProfileTimer t("redis 1 ");
        for(int i = 0 ; i < 10000 ; i++){
            uint32 prev = std::stoi(r_client.Get("r_client").c_str());
            r_client.Set("r_client",prev+1);
        }
    }

    {
        ProfileTimer t("redis 2 ");
         for(int i = 0 ; i < 500000 ; i++){
            uint32 prev = std::stoi(r_client.Get("r_client").c_str());
        }
    }
    {
        ProfileTimer t("redis 3 ");
         for(int i = 0 ; i < 500000 ; i++){
            uint32 prev = std::stoi(r_client_slave.Get("r_client").c_str());
        }
    }
    //static int r_client = 0;
    //{
    //    ProfileTimer t("memory ");
    //    //for(int i = 0 ; i < 10000 ; i++){
    //        int prev = r_client;
    //        r_client = prev + 1 ;
    //    //}

    //}
    //printf("r_client = %d\n  ", r_client);
    return 0;
}


