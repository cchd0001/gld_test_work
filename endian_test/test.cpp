#include <iostream>

union Test {
	int i;  // normally i need 4 bytes . 
	char c; // language guarentee c is in the low address of test . 
};

int main() {
	Test t;
	t.i = 0x11223344;   // t.1 = 0x 11 22 33 44 . endian is 44
	if( t.c == 0x44 ) {
		std::cout<<"LSB"<<std::endl;
	} else if ( t.c == 0x11 ) {
		std::cout<<"HSB"<<std::endl;
	} else {
		std::cout<<"Woo ... I don't know now ."<<std::endl;
	}
}
