#ifndef __MD5_H__
#define __MD5_H__ 

typedef unsigned int MD5_word;
typedef unsigned long long MD5_uint64;

struct MD5Content {
	MD5_word A;
	MD5_word B;
	MD5_word C;
	MD5_word D;

};

union MD5Result {
   struct MD5Content content;
   unsigned char buff[16];
};

struct MD5Content MD5(const unsigned char * buff , MD5_uint64 size );


#endif //__MD5_H__ 
