#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <string>
#include 
/*****************************************************
 *@Brief :
 *          禁用拷贝构造和赋值运算.
 *          保证数据只有一份.
 * ***************************************************/
struct IDiscardCopy {

        IDiscardCopy & operator == ( const IDiscardCopy & )  = delete ;

        IDiscardCopy( const IDiscardCopy & )  = delete ;

        virtual ~IDiscardCopy() {}
};

/*****************************************************
 *@Brief :
 *          玩家的数据模板类 . 
 *   任何模块的玩家数据都需要继承这个模板并实现接口.
 * ***************************************************/

struct IData : public IDiscardCopy {

    public:

        virtual bool Load( const std::string & uuid ) = 0 ;

        virtual void Unload( const std::string & uuid) = 0 ; 

        virtual ~IData() {}

    private:

};


struct UserData : public IDiscardCopy {

    const std::string uuid ;

    void Load() ;

    void Unload() ;

    static 
};



#endif
