#include <boost/asio/coroutine.hpp>
#include <iostream>
#include <atomic>
#include <mutex>

std::mutex mutex;
class TestCoroutine : public boost::asio::coroutine
{
    public :
        void operator()(){
            BOOST_ASIO_CORO_REENTER(this)
            {
                do
                {
                    std::cout<<"Begin fork"<<std::endl;
                    BOOST_ASIO_CORO_FORK TestCoroutine(*this)();
                    std::cout<<"End fork : is parent "<<is_parent()<<std::endl;

                } while (is_parent() );

                std::cout<<"Begin childfork"<<std::endl;
                BOOST_ASIO_CORO_FORK TestCoroutine(*this)();
                std::cout<<"End childfork : is parent "<<is_parent()<<std::endl;

            }
        }
};

struct TestCoroutine1{
    boost::asio::coroutine cor;
    void GetInput(){
        std::cout<<"Lock it 3"<<std::endl;
        std::lock_guard<std::mutex> m(mutex);
        std::cout<<"Lock it 4"<<std::endl;
        int i;
        std::cin>>i;
        std::cout<<"i is "<<i<<std::endl;
    }
    void Run() {
        std::lock_guard<std::mutex> m(mutex);
        BOOST_ASIO_CORO_REENTER(cor)
        {
            {
                BOOST_ASIO_CORO_YIELD return GetInput();
                std::cout<<"Begin childfork"<<std::endl;
                BOOST_ASIO_CORO_FORK Run();
                std::cout<<"End childfork : is parent "<<cor.is_parent()<<std::endl;
            }
        }

    }
};

using namespace std;
struct interleave :  public boost::asio::coroutine
{
    void p1(){std::cout<<"111111111111111111111111111"<<endl;}
    void p2(){std::cout<<"222222222222222222222222222"<<endl;}
  void operator()()
  {
    boost::asio::coroutine cor(*this);
    std::cout<<" _value is "<<int(boost::asio::detail::coroutine_ref(cor))<<std::endl;
    BOOST_ASIO_CORO_REENTER (this) 
    {
        
    std::cout<<"1-----------"<<std::endl;
      BOOST_ASIO_CORO_YIELD  p1();
    std::cout<<"2-----------"<<std::endl;
      BOOST_ASIO_CORO_YIELD  p2();
    std::cout<<"3-----------"<<std::endl;
    }
  }
};

struct TestCoroutine2{
    boost::asio::coroutine cor;
    void GetInput(){
        int i;
        std::cin>>i;
        std::cout<<"i is "<<i<<std::endl;
    }
    void Run() {
        BOOST_ASIO_CORO_REENTER(cor)
        {
            {
                BOOST_ASIO_CORO_FORK {
                    std::cout<<" in fork "<<std::endl;
                }
                std::cout<<"Begin childfork"<<std::endl;
                std::cout<<"End childfork : is parent "<<cor.is_parent()<<std::endl;
            }
        }

    }
};

int main(){
    interleave tst;
    tst();
    tst();
    tst();
    tst();
    tst();
    TestCoroutine2 t1, t2;
    t1.Run();
    std::cout<<"--|----"<<std::endl;
    t2.Run();
    std::cout<<"------"<<std::endl;
    t1.Run();
    std::cout<<"--|----"<<std::endl;
    t2.Run();
    std::cout<<"------"<<std::endl;
    return 0;
}
