#include <map>
#include <iostream>

std::multimap<int ,int> the_map;

int main() {
    for( int i = 0 ; i < 22 ; i ++) {
        the_map.insert(std::make_pair( i,i));
    }
    for( auto i = the_map.begin() ; i != the_map.end() ; i++ ) {
        std::cout<<i->first<<" -- "<<i->second<<std::endl;
    }
    return 0;
}
