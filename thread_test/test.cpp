#include <thread>
#include <atomic>
#include <iostream>

struct Test {
    static std::atomic<int> id;
    Test() { id ++ ; std::cout<<id<<std::endl;  }
};

std::atomic<int> Test::id{0};


int main()
{
    auto thr = std::thread([](){
            Test t;
            });
    thr.join();
    return 0;
}
