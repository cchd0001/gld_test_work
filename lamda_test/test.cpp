#include <iostream>
#include <thread>
#include <unistd.h>

struct Test {
    Test() { std::cout<<"1"<<std::endl;  } 
    ~Test(){ std::cout<<"~1"<<std::endl; } 
    Test(const Test &) { std::cout<<"2"<<std::endl; } 
    Test & operator =(const Test & ) { std::cout<<"3"<<std::endl; } 
    void Print() const { std::cout<<"4"<<std::endl;  } 
};

int  main(){
    Test t;
    Test & t_ref(t);
    auto * tr =new  std::thread([t_ref]() {
                t_ref.Print();
            });
    tr->join();
    delete tr;
    return 0;
}
