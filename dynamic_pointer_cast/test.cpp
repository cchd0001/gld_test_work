#include <memory>
#include <iostream>

struct Test {
	virtual void P() { 
		std::cout<<"P from Test"<<std::endl;
	}
};

struct Test_1 : public Test {
	virtual void P() { 
		std::cout<<"P from Test_1 "<<std::endl;
	}
	void PP() {
		std::cout<<"PP from Test_1 "<<std::endl;
	}
};




/*********************
  * OUTPUT
  * 
  *	P from Test_1 
  *	PP from Test_1 
  *	2
  *
  *******************/

int main() {
	
	std::shared_ptr<Test> t_p ;
	t_p = std::make_shared<Test_1>() ;
	t_p->P();

	std::shared_ptr<Test_1> t_1_p  = std::dynamic_pointer_cast<Test_1>(t_p);
	t_1_p->PP();
	std::cout<<t_1_p.use_count()<<std::endl;
	return 0;
}



