#include "misc.h"

void maopao_normal(int * src , size_t len)
{
    assert( NULL != src );
    size_t i , j ;
    for( i = len ; i > 0  ; i-- )
    {
        for( j = 0 ; j < i -1 ; j++ )
        {
            if( src[j] > src[j+1] )
            {
                swap(&src[j] , &src[j+1]);
            }
        }
    }
}

void maopao_recursion(int * src , size_t len)
{
    assert( NULL != src );
    if( len <2 ) 
        return ;
    size_t i = 0 ;
    for(i = 0 ; i < len -1 ; i++ )
    {
        if( src[i] > src[i+1] )
        {
            swap(&src[i] , &src[i+1]);
        }
    }
    maopao_recursion(src,len-1);
    return ;
}

int main()
{
    int src[10]= { 1 , 3, 6,2,7,3,5,4,10,8};
    //maopao_normal(src,sizeof(src)/sizeof(src[0]));
    maopao_recursion(src,sizeof(src)/sizeof(src[0]));
    size_t i ;
    for( i=0 ; i< sizeof(src)/sizeof(src[0]); i++ )
    {
        printf(" %d ",src[i]);
    }
    printf("\n");
    return 0 ;

}
