#include "misc.h"

void insert_sort_normal(int * src , size_t len) 
{
    assert(NULL != src) ;
    size_t i = 0 ;
    if( len == 1 )
    {
        return ;
    }
    for(i = 1 ; i < len - 1 ; i++)
    {
        size_t curr_len = i +1 ;
        int curr_inserter = src[i+1];
        size_t lower = lower_bound(src , curr_len , curr_inserter);
        if( lower < curr_len )
        {
            insert_at_index(src,curr_len , lower , curr_inserter);
        }
        else
        {
            //在正确的位置.
        }
    }
    return ;
}

void insert_sort_recursion(int * src , size_t len)
{
    assert(NULL != src) ;
    if( len == 1 )
    {
        return ;
    }
    else
    {
        size_t pre_len = len -1 ;
        insert_sort_recursion(src,pre_len);
        int curr_inserter = src[pre_len];
        size_t lower = lower_bound(src , pre_len , curr_inserter);
        if( lower < pre_len)
        {
            insert_at_index(src, pre_len , lower , curr_inserter);
        }
        else
        {
            //在正确的位置.
        }
    }
    return ;
}

int main()
{
    int src[10]= { 1 , 3, 6,2,7,3,5,4,10,8};
    //maopao_normal(src,sizeof(src)/sizeof(src[0]));
    insert_sort_recursion(src,sizeof(src)/sizeof(src[0]));
    PRINT_INTERGER_ARRAY(src);
    return 0 ;
}
