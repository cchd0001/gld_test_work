#include <stdio.h>
#include <assert.h>

char * strcpy(char * src , char * desc)
{
    assert( NULL != src && NULL != desc );
    char * ret = desc ;
    while(*src) { *desc++ = *src++ ;  }
    *desc = 0 ;
    return ret ;
}

int main()
{
    char desc[100] ;
    char src[] = "Hello world!";
    printf("%s\n",strcpy(src,desc));
    return 0 ;
}
