#ifndef _TCPCONNECTION_H
#define _TCPCONNECTION_H

#include <deque>
#include "ServerCommon.h"
#include "Socket.h"
/**
 *  @addgroup TcpCommon 
 *  @{
 */
#define INVALID_CONN_ID -1

//! Connection 断开的回调.
typedef std::function<void(uint32_t)> DisconnectCallBack;
//! Connection Read 的缓存指针.
typedef std::shared_ptr<std::vector<uint8_t>> BufferPtr;

enum TcpConnStatus
{
    Invaid = 0 ,
    Connecting = 1,
    Connected = 2,
    Shutdowning= 3,
};
class TcpConnection
: private boost::noncopyable,
    public std::enable_shared_from_this<TcpConnection>
{
    public:
    TcpConnection(IoService& io_service, DisconnectCallBack disconnect_func);
    ~TcpConnection();

    //! 设置唯一id, 标记为connected .
    void            Init(uint32_t id);

    public:
    uint32_t        GetId();
    Socket&         GetSocket();
    IoService&      GetIoService();
    TcpConnStatus   GetStatus() const ;

    public:
    void            SetCallBacks(const ConnectionCallBacks& callbacks);
    //! 将数据加入发送队列，开始异步发送.
    void            Send(const uint8_t *buf, uint32_t len);
    //! 开始异步接收数据.
    void            Recv();
    //! 进入关闭流程 , 不再收发消息，发完发送队列之后关闭socket.
    void            Shutdown();

    private:
    void            RealSend(const BufferPtr& buffer);
    void            SafeShutdown();
    public:
    void            OnConnected(const ErrorCode& e);
    void            OnRead(const ErrorCode& e, size_t len);
    void            OnWrite(const ErrorCode& e, size_t len);
    void            OnDisconnect(const ErrorCode& e);

    protected:

    uint32_t                      conn_id_;
    IoService&                  io_service_;
    Socket                      socket_;
    ConnectionCallBacks         cb_;
    bool                        writting_msg_;
    std::deque<BufferPtr>       send_buf_;
    std::vector<uint8_t>        recv_buf_;
    DisconnectCallBack          disconnect_func_;
    TcpConnStatus               status_;
};

inline uint32_t TcpConnection::GetId()
{
    return conn_id_;
}

inline Socket& TcpConnection::GetSocket()
{
    return socket_;
}
inline IoService& TcpConnection::GetIoService()
{
    return io_service_;
}
inline void TcpConnection::SetCallBacks(const ConnectionCallBacks& callbacks)
{
    cb_ = callbacks;
}

inline void TcpConnection::Shutdown()
{
    io_service_.post(std::bind(&TcpConnection::SafeShutdown, shared_from_this()));
}

inline TcpConnStatus TcpConnection::GetStatus() const 
{
    return status_;
}

/** @} */

#endif // _TCPCONNECTION_H
