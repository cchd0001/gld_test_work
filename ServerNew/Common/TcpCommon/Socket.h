#ifndef _SOCKET_H
#define _SOCKET_H

#include "ServerCommon.h"

/**
 *  @addgroup TcpCommon 
 *  @{
 */

/** @brief 对 boost::asio::ip::tcp::socket 的简单包装.
 * 
 *  包装 boost::asio::ip::tcp::socket , 仅提供异步接口 .
 *  本类无锁，一个socket 基于一个 io_server .
 */
class Socket
{
    public:

        Socket(IoService& io_service);

        ~Socket();

    public:

        TcpSocket& socket() ;

        uint8_t *GetBuf();

        std::string RemoteIP() const;

    public:

        bool IsOpen() const;

        void Shutdown();

    public:
        void Connect(const std::function<void(const ErrorCode&)>& cb, const std::string& ip, uint16_t port);
        void Send(const std::function<void(const ErrorCode&, size_t)>& cb, const uint8_t *data, size_t size);
        void Recv(const std::function<void(const ErrorCode&, size_t)>& cb);
    private:
        TcpSocket                           socket_;
        boost::array<uint8_t, MAX_RECV_BUF> buffer_;
};

inline TcpSocket& Socket::socket() 
{
    return socket_;
}

inline uint8_t *Socket::GetBuf()
{
    return buffer_.data();
}

inline bool Socket::IsOpen() const
{
    return socket_.is_open();
}

inline void Socket::Shutdown()
{
    // NO EXCEPTION
    ErrorCode ec;
    socket_.shutdown(boost::asio::socket_base::shutdown_both, ec);
    socket_.close(ec);
}

/** @} */
#endif // _SOCKET_H
