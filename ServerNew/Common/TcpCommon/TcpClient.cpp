#include "TcpClient.h"

TcpClient::TcpClient(IoService& service) : TcpConnection(service, [](uint32_t){})
{
}

void TcpClient::Connect(const std::string& ip, const std::string& port)
{
    GetIoService().post(std::bind(&TcpClient::RealConnect , this , ip , port )) ;
}

void TcpClient::RealConnect(const std::string& ip, const std::string& port)
{
    if( status_ != TcpConnStatus::Invaid ) {
        return ;
    }
    Resolver resolver(io_service_);
    Resolver::query query(ip, port);
    Resolver::iterator endpoint_it = resolver.resolve(query);

    auto handle_connected = [&](const ConnectionPtr& conn, const ErrorCode& e) {
        if (!e) {
            if( status_ == TcpConnStatus::Connecting ) {
                status_ = TcpConnStatus::Connected ;
            }
            Recv();
        } else {
            status_ = TcpConnStatus::Invaid ;
        }
        conn->OnConnected(e);
    };
    boost::asio::async_connect(GetSocket().socket(), endpoint_it,
        std::bind(handle_connected, shared_from_this(), std::placeholders::_1));
    status_ = TcpConnStatus::Connecting ;
}
