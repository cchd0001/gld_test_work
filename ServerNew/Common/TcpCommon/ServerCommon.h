#ifndef _SERVERCOMMON_H
#define _SERVERCOMMON_H

#include <mutex>
#include <functional>

#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/noncopyable.hpp>

#define MAX_PKT_SIZE (64*1024)
#define MAX_RECV_BUF (MAX_PKT_SIZE*2)

#ifdef __APPLE__
#   define __UNUSED __attribute__ ((unused))
#else
#   define __UNUSED
#endif

class TcpConnection;
typedef std::lock_guard<std::mutex>				Lock;
typedef boost::system::error_code				ErrorCode;
typedef boost::asio::io_service					IoService;
typedef boost::asio::ip::tcp::endpoint			EndPoint;
typedef boost::asio::ip::tcp::resolver			Resolver;
typedef boost::asio::ip::tcp::resolver::query	Query;
typedef boost::asio::ip::tcp::acceptor			Acceptor;
typedef boost::asio::ip::tcp::socket			TcpSocket;
typedef std::function<void()>					TimerFunc;
typedef std::shared_ptr<TcpConnection>			ConnectionPtr;

/// async call back 
typedef std::function<void(const ConnectionPtr&)>							    FuncOnAccept;
typedef std::function<void(const ConnectionPtr&, bool)>						    FuncOnConnect;
typedef std::function<size_t(const ConnectionPtr&, const uint8_t *, int32_t)>	FuncOnRead;
typedef std::function<void(const ConnectionPtr&, int32_t)>					    FuncOnWrite;
typedef std::function<void(const ConnectionPtr&)>							    FuncOnDisconnect;


struct ConnectionCallBacks
{
    FuncOnConnect       OnConnected;
    FuncOnRead          OnRead;
    FuncOnWrite         OnWrite;
    FuncOnDisconnect    OnDisconnect;
};

#endif // _SERVERCOMMON_H
