#include "Socket.h"

Socket::Socket(IoService& io_service) : socket_(io_service)
{
}

Socket::~Socket()
{
}

void Socket::Connect(const std::function<void(const ErrorCode&)>& cb, const std::string& ip, uint16_t port)
{
    if (IsOpen()) {
        Shutdown();
    }

    boost::asio::ip::tcp::endpoint endpoint(
        boost::asio::ip::address::from_string(ip), port);

    socket_.async_connect(endpoint, cb);
}

void Socket::Send(const std::function<void(const ErrorCode&, size_t)>& cb, const uint8_t *data, size_t size)
{
    if (data == 0 || size == 0) {
        return;
    }
    boost::asio::async_write(socket_, boost::asio::buffer(data, size), cb);
}

void Socket::Recv(const std::function<void(const ErrorCode&, size_t)>& cb)
{
    socket_.async_read_some(boost::asio::buffer(buffer_), cb);
}

std::string Socket::RemoteIP() const
{
	return socket_.remote_endpoint().address().to_string();
}
