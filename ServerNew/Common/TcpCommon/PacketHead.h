#ifndef __TCPCOMMON_PACKETHEAD_H__
#define __TCPCOMMON_PACKETHEAD_H__

#include <cstdint>
#include <cstring>
#include "../Include/ExceptionMacro.h"

union EndianInt { uint8_t c[4]; int32_t l; };
static EndianInt endian_test = { { 'l', '?', '?', 'b' } };
#define ENDIANNESS ((char)endian_test.l)

#pragma pack(push, 1)
#ifdef _MSC_VER
#   pragma warning(disable:4200)
#endif // _MSC_VER

struct PacketHeader
{
    uint8_t len[4];
    uint8_t buf[0];
};

#pragma pack(pop)

inline int32_t GetSize(const uint8_t  * const c) 
{
    MUST_NOT_NULL(c);

    EndianInt size ;
    if( ENDIANNESS == 'b' ) 
    {
        size.c[0] = c[3] , size.c[1] = c[2] , size.c[2] = c[1] , size.c[3] = c[0] ;
    }
    else 
    {
        memcpy(size.c , c , sizeof(int32_t));
    }
    return size.l ;
}

inline void PutSize(uint8_t * const c , int32_t s) 
{
    MUST_NOT_NULL(c);

    EndianInt size ;
    size.l = s;
    if( ENDIANNESS == 'b' ) 
    {
        c[3] = size.c[0] , c[2] = size.c[1]  , c[1] = size.c[2] , c[0] = size.c[3] ;
    }
    else
    {
        memcpy(c, size.c , sizeof(int32_t));
    }
}

#endif //__TCPCOMMON_PACKETHEAD_H__
