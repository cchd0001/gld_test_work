#ifndef _SERVER_H
#define _SERVER_H

#include <atomic>
#include <unordered_set>

#include "ServerCommon.h"
#include "io_service_pool.hpp"
#include "../Include/IndexMgr.h"
#include <map>
/**
 *  @addgroup TcpCommon 
 *  @{
 */
typedef std::function<void()> SignalCallback; 

class TcpServer : private boost::noncopyable
{
public:
    explicit TcpServer(const std::string& address, const std::string& port,
        std::size_t io_service_pool_size, const FuncOnAccept& func);

    void    SetCallBacks(const ConnectionCallBacks& cb);

    void    Run();

    boost::asio::io_service& IoService() { return io_service_pool_.get_io_service(); }

    void RegisterWaitSignals(int i , SignalCallback f) { signals_.add(i) ; sigcalls_[i] = f; }

    void Stop(){ io_service_pool_.stop(); }

    void SetIndexInterface( const IndexInterface & i ) { conn_id_mgr_ = i ; };
private:
    void    StartAccept();
    void    ConnectionDown(uint32_t id);
    void    ListenSignal(const boost::system::error_code&, int);
private:
    io_service_pool                     io_service_pool_;
    boost::asio::signal_set             signals_;
    std::map<int,SignalCallback>        sigcalls_;
    Acceptor                            acceptor_;
    FuncOnAccept                        OnAccept;
    ConnectionCallBacks                 cb_;

    IndexInterface conn_id_mgr_;
};

inline void TcpServer::SetCallBacks(const ConnectionCallBacks& cb)
{
    cb_ = cb;
}

inline void TcpServer::Run()
{
    io_service_pool_.run();
}

inline void TcpServer::ConnectionDown(uint32_t id)
{
    conn_id_mgr_.putter(id);
}

/** @} */
#endif // _SERVER_H
