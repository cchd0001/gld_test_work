#ifndef _TCPCLIENT_H
#define _TCPCLIENT_H

#include "ServerCommon.h"
#include "TcpConnection.h"

/**
 *  @addgroup TcpCommon 
 *  @{
 */

class TcpClient : public TcpConnection
{
public:
    TcpClient(IoService& service);

public:
    //! 向指定地址发起链接请求.
    /**
     *  向指定地址发起链接请求. 
     *  如果存在对方尚未处理的链接请求，那么不会重复发送.
     */
    void            Connect(const std::string& ip, const std::string& port);

    bool            Connected() const ;

private:

    void            RealConnect(const std::string& ip, const std::string& port);

    EndPoint        endpoint_;
};

bool inline TcpClient::Connected() const 
{
    TcpConnStatus status =  GetStatus();
    return TcpConnStatus::Invaid != status && TcpConnStatus::Connecting != status ;
}

typedef std::shared_ptr<TcpClient> TcpClientPtr;

/** @} */
#endif // _TCPCLIENT_H
