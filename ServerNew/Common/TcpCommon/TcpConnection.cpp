#include "TcpConnection.h"

#define CHECK_ERROR(e) \
    if (e) { \
        socket_.Shutdown(); \
        OnDisconnect(e); \
        return; \
    }

TcpConnection::TcpConnection(IoService& io_service, DisconnectCallBack disconnect_func) :
    conn_id_(INVALID_CONN_ID),
    io_service_(io_service),
    socket_(io_service),
    disconnect_func_(disconnect_func),
    status_(TcpConnStatus::Invaid)
{
}

void TcpConnection::Init(uint32_t id)
{
    conn_id_ = id;
    status_ = TcpConnStatus::Connected ;
}

TcpConnection::~TcpConnection()
{
    printf("%s\n", __FUNCTION__);
}

void TcpConnection::Recv()
{
    socket_.Recv(std::bind(&TcpConnection::OnRead, shared_from_this(),
        std::placeholders::_1, std::placeholders::_2));
}

void TcpConnection::Send(const uint8_t *buf, uint32_t len)
{
    if( TcpConnStatus::Connected == status_ )
    {
        BufferPtr buffer(new std::vector<uint8_t>(buf, buf + len));
        io_service_.post(std::bind(&TcpConnection::RealSend, shared_from_this(), buffer));
    }
}
void TcpConnection::RealSend(const BufferPtr& buffer)
{
    send_buf_.push_back(buffer);

    socket_.Send(std::bind(&TcpConnection::OnWrite, shared_from_this(),
        std::placeholders::_1, std::placeholders::_2), &(*buffer->begin()), buffer->size());
}

void TcpConnection::OnConnected(const ErrorCode& e)
{
    if (cb_.OnConnected) {
        cb_.OnConnected(shared_from_this(), !e);
    }
}

void TcpConnection::OnRead(const ErrorCode& e, size_t len)
{
    CHECK_ERROR(e);
    if( status_ != TcpConnStatus::Connected ) {
        return ;
    }
    if (cb_.OnRead) {
        uint8_t *buf = socket_.GetBuf();
        int cur_pos = recv_buf_.size();
        recv_buf_.resize(cur_pos + len);
        memcpy(&recv_buf_[0] + cur_pos, buf, len);
        size_t proc_len = cb_.OnRead(shared_from_this(), (uint8_t*)&recv_buf_[0], recv_buf_.size());
        if (proc_len != 0) {
            recv_buf_.erase(recv_buf_.begin(), recv_buf_.begin() + proc_len);
        }
    }
    Recv();
}
void TcpConnection::SafeShutdown() {
    if( status_ != TcpConnStatus::Connected) {
        return ;
    }
    status_ = TcpConnStatus::Shutdowning;
    if(send_buf_.empty()){
        status_ = TcpConnStatus::Invaid;
        socket_.Shutdown();
    }
}
void TcpConnection::OnWrite(const ErrorCode& e, size_t len)
{
    CHECK_ERROR(e);

    if (cb_.OnWrite) {
        cb_.OnWrite(shared_from_this(), len);
    }
    // remove finished buffer
    if( ! send_buf_.empty() ) {
        send_buf_.pop_front();
    }

    // continue send
    if (send_buf_.empty()) {
        if( status_ == TcpConnStatus::Shutdowning){
            socket_.Shutdown() ;
            status_ = TcpConnStatus::Invaid;
        } 
    } else {
        auto buffer = send_buf_.front();
        socket_.Send(std::bind(&TcpConnection::OnWrite, shared_from_this(),
            std::placeholders::_1, std::placeholders::_2), &(*buffer->begin()), buffer->size());
    }
}

void TcpConnection::OnDisconnect(const ErrorCode& /*e*/)
{
    printf("disconnect: %d\n", GetId());
    if (cb_.OnDisconnect) {
        cb_.OnDisconnect(shared_from_this());
    }
    if (disconnect_func_) {
        disconnect_func_(GetId());
    }
}
