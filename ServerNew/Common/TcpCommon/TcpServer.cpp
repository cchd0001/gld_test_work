#include "TcpServer.h"

#include "TcpConnection.h"

TcpServer::TcpServer(const std::string& address, const std::string& port,
    std::size_t io_service_pool_size, const FuncOnAccept& func)
    : io_service_pool_(io_service_pool_size),
      signals_(io_service_pool_.get_io_service()),
      acceptor_(io_service_pool_.get_io_service()),
      OnAccept(func)
{
    signals_.add(SIGINT);
    signals_.add(SIGTERM);
#if defined(SIGPIPE)
	signals_.add(SIGPIPE);
#endif // defined(SIGPIPE)
#if defined(SIGQUIT)
    signals_.add(SIGQUIT);
#endif // defined(SIGQUIT)

    
	signals_.async_wait(std::bind(&TcpServer::ListenSignal, this , std::placeholders::_1 , std::placeholders::_2));

    Resolver resolver(acceptor_.get_io_service());
    Query query(address, port);
    EndPoint endpoint = *resolver.resolve(query);
    acceptor_.open(endpoint.protocol());
    acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
    acceptor_.bind(endpoint);
    acceptor_.listen();

    //logger_.reset(new services::logger(io_service_pool_.get_io_service(), ""));

    StartAccept();
}

void TcpServer::ListenSignal(const boost::system::error_code& e, int sig) {
    auto deal =  [this](const boost::system::error_code&, int sig) {
#if defined(SIGPIPE)
		if (SIGPIPE == sig)
		{
			return;
		}
#endif
        try {
            if(sigcalls_.find(sig) != sigcalls_.end()){
                sigcalls_[sig](); 
                return ;
            }
        } catch( ... ) {
            ; 
        }
        Stop();
    };

    deal(e,sig);
	signals_.async_wait(std::bind(&TcpServer::ListenSignal, this , std::placeholders::_1 , std::placeholders::_2));
}
void TcpServer::StartAccept()
{
    ConnectionPtr new_connection(new TcpConnection(io_service_pool_.get_io_service(),
        std::bind(&TcpServer::ConnectionDown, this, std::placeholders::_1)));
    try {
        new_connection->Init(conn_id_mgr_.getter());
    }catch( ... ) {
        // used all ids . can't service any more .
        ;
    }
    auto handle_accept = [&](const ConnectionPtr& conn, const ErrorCode& e) {
        if (!e) {
            if (OnAccept) {
                OnAccept(conn);
            }
            conn->SetCallBacks(cb_);
            conn->Recv();
        }
        StartAccept();
    };
    acceptor_.async_accept(new_connection->GetSocket().socket(),
        std::bind(handle_accept, new_connection, std::placeholders::_1));
}
