#ifndef __GATEWAY_ID_H__
#define __GATEWAY_ID_H__

#include <cstdint>
/**
 * addgroup Session
 * @{
 */
namespace Session{

    //! SessionID , 用于GameServer和Gateway之间交互.
    /**
     *  H                            L
     *
     *  +---------------------------------------+\n
     *  |31-----------------16|15--------------0|\n
     *  +---------------------------------------+\n
     *
     *  31-16 bits means the room id for multicast .
     *
     *  15-0 bits means the session id for singlecast .
     */
    typedef uint32_t SessionID ;
    //! 30-16 bits
    constexpr uint32_t MulticastMark = 0xffff0000;
    //! 15-0bits 
    constexpr uint32_t SinglecastMark= 0x0000ffff;

    constexpr uint32_t MaxSinglecastSize = SinglecastMark ;
    //< 14-0 bits
    constexpr uint32_t MaxMulticastSize = ( MulticastMark >> 16 & 0x0000ffff ) ;

    //! @return the multicast id from i
    uint32_t inline GetMulticastID(SessionID i) 
    {
        return ( i & MulticastMark ) >>16 ;
    }

    //! @return the signlecast id from i
    uint32_t inline GetSinglecastID(SessionID i)
    {
        return (i & SinglecastMark) ;
    }

    //! @return the new session id that cotain i and the multicast id  .
    uint32_t inline SetMulticastID( SessionID  i , uint32_t id ) 
    {
        return i |( ( id & MaxMulticastSize )<< 16 );
    }

    //! @return the new session id that cotain i and the singlecast id  .
    uint32_t inline SetSinglecastID( SessionID i , uint32_t id )
    {
        return i | (id & MaxSinglecastSize);
    }

    //! ServerID ,  作为GameServer 的唯一ID .
    typedef  int32_t GameSeverID;

}
/** @} */
#endif //__GATEWAY_ID_H__
