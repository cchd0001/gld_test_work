#ifndef __COMMON_INCLUDE_STROP_H__
#define __COMMON_INCLUDE_STROP_H__
#include <string>


#ifndef UNUSED
#define UNUSED(x)
#endif

template<typename T>
std::string JoinString(const std::string & UNUSED(spliter),const T & value) {
    return value;
}
//
// Example : 
//           JoinString("|" , 123, " Hello " ,"World",456 ); ==> 123| Hello |World|456
//
//

template<typename T, typename... Args>
std::string JoinString(const std::string & spliter, const T & value, Args... args) {
    return value+spliter+JoinString(spliter, args...);
}

#endif //__COMMON_INCLUDE_STROP_H__
