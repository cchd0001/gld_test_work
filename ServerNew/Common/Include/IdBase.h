#ifndef __INCLUDE_IDBASE_H__
#define __INCLUDE_IDBASE_H__

#include <cstdint>
/**
 *  @addgroup Misc_Include
 *  @{
 */
namespace IDBASE {
    template< class T , class IdType = T> 
        class IdGetter {
            public:
                IdType IGetId(const T & d) { return IdType(d) ; }
        };
    //! Things that only identified by id .
    template< class T , class I >
        class IdBase_Implement
        {
            public:
                IdBase_Implement() {}

                typedef T DataType ;
                typedef I IdType ;

            public:
                IdBase_Implement( const IdBase_Implement & a ) {
                    data = a.data ;
                    fake_id = a.fake_id;
                    fake_flag = a.fake_flag ;
                }

                IdBase_Implement & operator = ( const IdBase_Implement& b ) {
                    if( this != &b) {
                        data = b.data ;
                        fake_id = b.fake_id;
                        fake_flag = b.fake_flag ;
                    }
                    return *this;
                }
                IdType GetId() const
                {
                    return fake_flag ? fake_id : IdGetter<DataType,IdType>().IGetId(data);
                }

                bool operator == ( const IdBase_Implement& b ) const 
                {
                    return GetId() == b.GetId() ;
                }

                bool operator < ( const IdBase_Implement & b  ) const 
                {
                    return GetId() < b.GetId() ;
                }

                T data ;

                bool IsFake() const { return fake_id; }

            protected:

                I fake_id ;

                bool fake_flag;
        };

    template< class T , class I >
        class IdBase : public IdBase_Implement< T  , I > 
    {
        public:


            typedef IdBase_Implement<T,I> Super;

            typedef typename Super::DataType DataType;

            typedef typename Super::IdType IdType;

            IdBase() {}

            IdBase( const DataType & d )
            {
                Super::data = d ;
                Super::fake_flag = false ;
            }

            IdBase( const IdType & d )
            {
                Super::fake_id = (d);
                Super::fake_flag = (false);
            }

    };

    template< class T > 
        class IdBase< T , T > : public IdBase_Implement< T , T >
        {
            public:

                typedef IdBase_Implement<T,T> Super;

                typedef typename Super::DataType DataType;

                IdBase() {}

                IdBase( const DataType & d )
                { 
                    Super::data = d ;
                    Super::fake_flag  = false ; 
                }
        };
}
/** @} */
#endif //__INCLUDE_IDBASE_H__
