#include "IndexMgr.h"
#include "UnitTest++/UnitTestPP.h"

SUITE(CommonInclude) 
{
    typedef IndexMgr<int32_t , 11 , 21>  TestIndex ;
    TEST_FIXTURE(TestIndex , GetPutId) {
        CHECK_EQUAL(11,GetId());
        CHECK_EQUAL(12,GetId());
        CHECK_EQUAL(13,GetId());
        CHECK_EQUAL(14,GetId());
        CHECK_EQUAL(15,GetId());
        CHECK_EQUAL(16,GetId());
        CHECK_EQUAL(17,GetId());
        CHECK_EQUAL(18,GetId());
        CHECK_EQUAL(19,GetId());
        CHECK_EQUAL(20,GetId());
        PutId(12);
        CHECK_EQUAL(12,GetId());
        PutId(11);
        CHECK_EQUAL(11,GetId());
        bool throw_flag = false ;
        try{ 
            GetId() ;
        } catch ( ... ) {
            throw_flag = true ;
        }
        CHECK_EQUAL(true , throw_flag) ;
    }
}
