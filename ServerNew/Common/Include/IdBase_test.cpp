#include "UnitTest++/UnitTestPP.h"
#include "IdBase.h"
namespace IDBASE {
    struct TestId1 {
        int i ;
        int j ;
    } ;

    template<>
        class IdGetter<TestId1,uint8_t>{
            public:
                uint8_t IGetId(const TestId1 & i ) {
                    return i.i + i.j;
                }
        };

    struct TestId {
        int i ;
        int j ;
    } ;
    template<>
        class IdGetter<TestId,uint32_t>{
            public:
                uint32_t IGetId(const TestId & i ) {
                    return i.i + i.j;
                }
        };

    struct TestId2 {
        int i ;
        int j ;
    } ;
    template<>
        class IdGetter<TestId2,int32_t>{
            public:
                int32_t IGetId(const TestId2 & i ) {
                    return i.i ;
                }
        };
}

SUITE(CommonInclude) 
{
    typedef IDBASE::IdBase<int,int> IbaseInt;
    TEST(IdBaseGetId) {
        CHECK_EQUAL(5, IbaseInt(5).GetId());
        CHECK_EQUAL(48 , IbaseInt('0').GetId());
    }


    typedef IDBASE::IdBase<IDBASE::TestId,uint32_t> TestIdI;
    typedef IDBASE::TestId TestId_;
    TEST_FIXTURE(TestId_ , IdBaseGetId) {
        i = 100 ;
        j = 50 ;
        CHECK_EQUAL(150,TestIdI(*this).GetId());
    }


    typedef IDBASE::IdBase<IDBASE::TestId1,uint8_t> TestId1I;
    typedef IDBASE::TestId1 TestId1_;
    TEST_FIXTURE(TestId1_ , IdBaseGetId) {
        i = 100 ;
        j = 150 ;
        CHECK_EQUAL(250,TestId1I(*this).GetId());
        CHECK(TestId1I(*this) == TestId1I(*this));
        i = 200 ;
        CHECK_EQUAL(350-256,TestId1I(*this).GetId());
    }
    typedef IDBASE::IdBase<IDBASE::TestId2,int32_t> TestId2I;
    typedef IDBASE::TestId2 TestId2_;
    TEST(IdBaseCmp) {
        bool flag ;
        flag = IDBASE::IdBase<IDBASE::TestId2,int32_t>(TestId2_{1,1}) == IDBASE::IdBase<IDBASE::TestId2,int32_t>(TestId2_{1,0}) ;
        CHECK_EQUAL( true ,flag  ) ;
        flag = IDBASE::IdBase<TestId2_,int32_t>(TestId2_{0,1}) == IDBASE::IdBase<TestId2_,int32_t>(TestId2_{1,1}) ;
        CHECK_EQUAL( false,flag) ;
        flag = IDBASE::IdBase<TestId2_,int32_t>(TestId2_{0,1}) < IDBASE::IdBase<TestId2_,int32_t>(TestId2_{1,1}) ;
        CHECK_EQUAL( true ,flag) ;
        flag = IDBASE::IdBase<TestId2_,int32_t>(TestId2_{1,0}) < IDBASE::IdBase<TestId2_,int32_t>(TestId2_{1,1}) ;
        CHECK_EQUAL( false,flag) ;
        flag = IDBASE::IdBase<TestId2_,int32_t>(TestId2_{2,0}) < IDBASE::IdBase<TestId2_, int32_t>(TestId2_{1,7}) ;
        CHECK_EQUAL( false,flag) ;
    }
}
