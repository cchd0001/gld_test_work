#ifndef __INCLUDE_FASTBUF_H__
#define __INCLUDE_FASTBUF_H__

template <typename T>
class FastBuf
{
    static const int max_size_ = 20 * 1024;
    public:
    FastBuf(int size)
    {
        if (size > max_size_) {
            buf = new T[size];
        } else {
            buf = buf_;
        }
    }
    ~FastBuf()
    {
        if (buf != buf_) {
            delete buf;
        }
    }
    T *buf;

    private:
    T  buf_[max_size_];
};

#endif //__INCLUDE_FASTBUF_H__
