#include "GateID.h"
#include "UnitTest++/UnitTestPP.h"

SUITE(ID_TEST)
{


    TEST(GetMulticastID) 
    {
        CHECK_EQUAL( 0x1234 , Session::GetMulticastID(0x12345678) );
        CHECK_EQUAL( 0x1234 , Session::GetMulticastID(0x12340000) );
        CHECK_EQUAL( 0x9234 , Session::GetMulticastID(0x92345678) );
        CHECK_EQUAL( 0x9234 , Session::GetMulticastID(0x92340000) );
    }

    TEST(GetSinglecastID)
    {
        CHECK_EQUAL( 0x1234 , Session::GetSinglecastID(0x12341234) ) ;
        CHECK_EQUAL( 0x1234 , Session::GetSinglecastID(0x00001234) ) ;
        CHECK_EQUAL( 0x1234 , Session::GetSinglecastID(0x92341234) ) ;
    }

    TEST(SetMulticastID)
    {
        CHECK_EQUAL( 0x12341234 , Session::SetMulticastID(0x00001234,0x1234) ) ;
        CHECK_EQUAL( 0x92341234 , Session::SetMulticastID(0x00001234,0x9234) ) ;
        CHECK_EQUAL( 0x92341234 , Session::SetMulticastID(0x80001234,0x1234) ) ;
        CHECK_EQUAL( 0x92340000 , Session::SetMulticastID(0x80000000,0x1234) ) ;
    }

    TEST(SetSinglecastID)
    {
        CHECK_EQUAL( 0x12341234 , Session::SetSinglecastID(0x12340000, 0x1234) ) ;
    }
}
