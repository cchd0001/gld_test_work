#ifndef __COMMON_INCLUDE_EXCEPTIONMACRO_H__
#define __COMMON_INCLUDE_EXCEPTIONMACRO_H__

#include <exception>
#include <stdexcept>
#include "Strop.h"

#define MUST_NOT_NULL(x) \
    if ( nullptr == (x) ) \
    {\
        throw std::runtime_error(JoinString(" " ,"argument",#x,"from", __FUNCTION__ ,"who must not be nullptr is nullptr now ."));\
    }

#endif //__COMMON_INCLUDE_EXCEPTIONMACRO_H__
