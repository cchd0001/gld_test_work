#ifndef _INDEXMGR_H
#define _INDEXMGR_H

#include <unordered_set>
#include <cstdint>
#include <functional>
#include <mutex>
/**
 *  @addgroup Misc_Include
 *  @{
 */

//! IndexMgr that provide valid index from [min,max)
template <typename T ,T min , T max>
class IndexMgr
{
    public:
        IndexMgr() : id_(min)
    {
    }

    public:
        T GetId()
        {
            T prev = id_ ;
            while ( id_ == max || IdExist(id_)) 
            {
                id_ >= max ?  id_ = min : ++id_ ;
                if ( prev == id_ )
                {
                    throw std::runtime_error("IndexMgr run out of ids.") ;
                }
            }
            id_set_.insert(id_);
            prev = id_ ;
            //! assume next id is valid . Decr the times of IdExist called .
            id_ >= max ?  id_ = min : ++id_ ; 
            return prev;
        }

        void PutId(T id)
        {
            id_set_.erase(id);
        }

    protected:
        bool IdExist(T id)
        {
            return id_set_.find(id) != id_set_.end() ;
        }

    protected:
        T                           id_;
        std::unordered_set<T>       id_set_;
};



typedef std::function< uint32_t() > IndexGetterFunc ;

typedef std::function< void (uint32_t) > IndexPutterFunc ; 

struct IndexInterface {
    IndexGetterFunc getter ;
    IndexPutterFunc putter ;
};

// A thread safe version of IndexMgr
template <typename T ,T min , T max>
struct IndexMgr_ThreadSafe : public IndexMgr<T,min,max>
{
    typedef IndexMgr<T,min,max> Super;

    T GetId()
    {
        std::lock_guard<std::mutex> l(mutex);
        return Super::GetId();
    }

    void  PutId(const T id)
    {
        std::lock_guard<std::mutex> l(mutex);
        Super::PutId(id);
    }
    private:
        std::mutex mutex;
};

/** @} */
#endif // !_INDEXMGR_H
