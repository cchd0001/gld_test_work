#include "Session.h"

namespace Session 
{
    void ClientSessionMgr::SendTo(SessionID id, const uint8_t *buf, size_t len ) 
    {
        if( GetSinglecastID(id) && HaveSession(id) )
        {
            GetSessionType(id).data.SendTo(buf,len);
        }
        if( GetMulticastID(id) ) {
            auto sessions = GetSessionsByRoom(id) ;
            for( auto const & session : sessions ) 
            {
                session.data.SendTo(buf,len);
            }
        }
    }
}
