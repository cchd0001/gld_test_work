SET(GATE_SOURCE_H Session.h Gateway.h)
SET(GATE_SOURCE_CPP  Session.cpp 
    Gateway.cpp
    GameServer.cpp
    Main.cpp
    )

add_executable(Gateway  ${GATE_SOURCE_H} ${GATE_SOURCE_CPP})

target_link_libraries(Gateway
    boost_system
    TcpCommon
    pthread
    )

source_group("" FILES ${GATE_SOURCE_H} ${GATE_SOURCE_CPP} )
