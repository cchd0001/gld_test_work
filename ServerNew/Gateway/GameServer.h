#ifndef __GATEWAY_GAMESERVER_H__
#define __GATEWAY_GAMESERVER_H__
#include <cstdint>
#include "../Common/Include/GateID.h"
#include "Session.h"
#include <mutex>
#include "SessionMgr.h"

namespace Session
{

    struct ServerSessionMgr : public Session::SessionMgr<Session::GameSeverID,Session::ATcpSession,Session::GameSeverID,uint16_t> 
    {
        void SendTo(Session::GameSeverID to,const uint8_t * data , size_t len);
    };

}

struct ServerSessionMgrMutex
{
    std::mutex mutex;
    Session::ServerSessionMgr mgr;
};

inline ServerSessionMgrMutex & GetServerSessionMgrMutexMgr() 
{
    static ServerSessionMgrMutex s ;
    return s;
}

#endif //__GATEWAY_GAMESERVER_H__
