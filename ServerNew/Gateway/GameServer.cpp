#include "GameServer.h"
/*
void  GameServerMgr::ServerLogin(Session::GameSeverID id, const Session::ATcpSession & s)
{
    std::lock_guard<std::mutex> l(m_mutex);
    servers[id] = s;
}

void GameServerMgr::ServerLogoff( Session::GameSeverID id ) 
{
    std::lock_guard<std::mutex> l(m_mutex);
    servers.erase(id);
}*/
namespace Session {

    void ServerSessionMgr::SendTo(Session::GameSeverID id, const uint8_t * data , size_t len){
        auto sessions = GetSessionsByRoom(id);
        for( const auto & session : sessions) 
        {
            session.data.SendTo(data,len);
        }
    }

}
