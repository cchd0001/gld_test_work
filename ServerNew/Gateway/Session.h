#ifndef __SESSION_H__
#define __SESSION_H__

#include <memory>
#include "SessionMgr.h"
#include "../Common/Include/IdBase.h"
#include "../Common/Include/GateID.h"
#include "../Common/TcpCommon/TcpConnection.h"

/**
 * @addgroup Session
 * @{
 */
namespace Session {

    typedef std::shared_ptr<TcpConnection> SessionBase;

    enum GatewaySessionStatus {
        Unknown = 0 ,
        GameServer = 1 ,
        GameClient = 2 ,
        StatusMax 
    };

    struct ATcpSession {

        GatewaySessionStatus status ;

        SessionBase session_base;

        int server_id ;

        void SendTo(const uint8_t * data , size_t len ) const
        {
            if(Valid()) 
            {
                return session_base->Send(data, len ) ;
            }
        }
        bool Valid() const
        {
            return nullptr != session_base ;
        }
    };
}
namespace IDBASE {
    template<>
        class IdGetter<::Session::ATcpSession , uint16_t>
        {
            public : 
                uint16_t IGetId(const ::Session::ATcpSession &  data) const {
                    if( data.session_base ) {
                        return data.session_base->GetId();
                    } else {
                        throw std::runtime_error("invalid TcpSession . TcpConnection is nullptr ");
                    }
                }
        };
}

namespace Session {

    //typedef SessionMgr<uint16_t , ATcpSession , uint16_t , uint16_t >  ClientSessionMgr ;
    
    struct ClientSessionMgr : public SessionMgr<uint16_t , ATcpSession , uint16_t , uint16_t >
    {
        void SendTo(SessionID id , const uint8_t * buf , size_t len ) ;

    };

    struct ClientSessionMgrMutex {
        ClientSessionMgr mgr ;
        std::mutex mutex ;
    };

}

inline Session::ClientSessionMgrMutex & GetClientSessionMgrMutexMgr() {
    static Session::ClientSessionMgrMutex s ;
    return s ;
}

/** @} */

#endif //__SESSION_H__
