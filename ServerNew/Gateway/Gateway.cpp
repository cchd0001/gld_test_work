#include <iostream>
#include "SessionMgr.h"
#include "Gateway.h"
#include "Session.h"
#include "GateRoom.h"
#include "../Common/Include/ExceptionMacro.h"
#include "../Common/Include/GatePacket.h"
#include "../Common/TcpCommon/PacketHead.h"
#include "GameServer.h"
#include "../Common/Include/FastBuf.h"
#include <cassert>

GatewayTcpServer::GatewayTcpServer( int16_t client_port
                ,int16_t server_port 
                ,size_t client_pool_size
                ,size_t server_pool_size )
            : m_game_client(local_host, std::to_string(client_port) , client_pool_size , OnClientAccept)
              , m_game_server(local_host, std::to_string(server_port) , server_pool_size, OnServerAccept)
    {
        m_game_client.SetIndexInterface(
                {
                std::bind(&ConnIndexType::GetId, &m_conn_index) ,
                std::bind(&ConnIndexType::PutId, &m_conn_index, std::placeholders::_1)
                }
                );
        m_game_server.SetIndexInterface(
                {
                std::bind(&ConnIndexType::GetId, &m_conn_index) ,
                std::bind(&ConnIndexType::PutId, &m_conn_index, std::placeholders::_1)
                }
                );
    }


#define LOCKGET_CLIENTSESSIONMGR \
    std::lock_guard<std::mutex> l(GetClientSessionMgrMutexMgr().mutex) ;\
    auto & mgr = GetClientSessionMgrMutexMgr().mgr;

#define LOCKGET_SERVERSESSIONMGR \
    std::lock_guard<std::mutex> l(GetServerSessionMgrMutexMgr().mutex) ;\
    auto & mgr = GetServerSessionMgrMutexMgr().mgr;

void OnClientAccept( const ConnectionPtr& ptr) 
{
    MUST_NOT_NULL(ptr)

    Session::ATcpSession s ;
    s.status = Session::GatewaySessionStatus::GameClient;
    s.session_base = ptr ;
    LOCKGET_CLIENTSESSIONMGR;
    mgr.EnterRoom(Session::UNKOWN_SESSION_ROOM , s );
}

void OnServerAccept( const ConnectionPtr& ptr )
{
    MUST_NOT_NULL(ptr)
    Session::ATcpSession s ;
    s.status = Session::GatewaySessionStatus::GameServer;
    s.session_base = ptr ;
    LOCKGET_SERVERSESSIONMGR;
    mgr.EnterRoom(Session::UNKOWN_SESSION_ROOM, s );
}

void GatewayTcpServer::OnDisconnect(const ConnectionPtr& ptr)
{
    MUST_NOT_NULL(ptr)
    //TODO : check if client and send info to server.
    Session::ATcpSession s ;
    s.session_base = ptr ;
    LOCKGET_CLIENTSESSIONMGR;
    mgr.ReleaseSession( IDBASE::IdBase<Session::ATcpSession , uint16_t>(s).GetId());
}

size_t GatewayTcpServer::SplitRawData(const ConnectionPtr& conn, const uint8_t *buf, size_t len , std::function<void(const ConnectionPtr& conn, const uint8_t *buf, size_t len)> f) 
{
    size_t len_left = len ;
    const uint8_t * buff_curr = buf ;
    while( len_left >  8 )
    {
        int32_t total_len = GetSize(buff_curr) + sizeof( int32_t) ;
        if( total_len > (int32_t)len_left ) 
        {
            break;
        }
        f(conn, buff_curr , total_len) ;
        buff_curr += total_len ;
        len_left -= total_len ;
    }

    return len - len_left ;
}

void GatewayTcpServer::OnReadClient(const ConnectionPtr& conn, const uint8_t *buf, size_t len)
{
    if( len <= 8 ) 
    {
        return  ;
    }
    int32_t server = GetSize(buf+4) ;
    FastBuf<uint8_t>  buff(len) ;
    memcpy(buff.buf ,buf,len );
    PutSize(buff.buf + 4 , conn->GetId());
    {
        LOCKGET_SERVERSESSIONMGR;
        mgr.SendTo(server,buff.buf ,len);
    }

}

void GatewayTcpServer::OnReadServer(const ConnectionPtr& conn , const uint8_t *buf, size_t len)
{
    if( len <= 8 )
    {
        return ;
    }
    const uint8_t * buff_begin = buf + 4 ;
    Session::SessionID session= GetSize(buff_begin);
    if( session == 0 ) 
    { // This is a Game Server command for Gateway.
        HandleRawCommand( buf + 8 , len - 8);
    }
    else
    { // This is a packet for client.
        LOCKGET_CLIENTSESSIONMGR ;
        mgr.SendTo(session , buf + 8 , len - 8 );
    }
}

void GatewayTcpServer::HandleRawCommand(const uint8_t *buf, size_t len)
{
    
}
