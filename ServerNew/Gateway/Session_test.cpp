#include "SessionMgr.h"
#include "UnitTest++/UnitTestPP.h"
#include <set>

typedef Session::SessionMgr<uint32_t , uint32_t >  TestSession;
SUITE(GatewayTest)
{

    TEST_FIXTURE(TestSession ,TestAll) {
        // EnterRoom
        EnterRoom(1,1);
        EnterRoom(1,2);
        EnterRoom(1,3);
        EnterRoom(2,1);
        EnterRoom(2,4);
        CHECK_EQUAL( true ,HaveRoom(1));
        CHECK_EQUAL( true ,HaveRoom(2));
        CHECK_EQUAL( false,HaveRoom(3));
        CHECK_EQUAL( true ,HaveSession(1) );
        CHECK_EQUAL( false,HaveSession(5) );
        EnterRoom(3,5);
        CHECK_EQUAL( true,HaveRoom(3));
        //  LeaveRoom
        LeaveRoom(5,3);
        CHECK_EQUAL( true,HaveRoom(3));
        LeaveRoom(3,5);
        CHECK_EQUAL( true,HaveRoom(3));
        ReleaseRoom(3);
        CHECK_EQUAL( false,HaveRoom(3));
        // GetSessionsByRoom
        std::set<uint32_t> ret1{1,2,3} ;
        if( ret1 != GetSessionIdsByRoom(1)) {
            CHECK(false);
        }
        typedef IDBASE::IdBase<uint32_t,uint32_t > III ;
        std::set<III>  ret11 {III(1),III(2),III(3)} ;
        if( ret11 != GetSessionsByRoom(1)) {
            CHECK(false);
        }

        std::set<uint32_t> ret2{1,4} ;
        if( ret2 != GetSessionIdsByRoom(2)) {
            CHECK(false);
        }
        //GetRoomIdsBySession
        std::set<uint32_t> ret3{1,2} ; 
        if( ret3 != GetRoomIdsBySession(1) ) {
            CHECK(false);
        }
        std::set<uint32_t> ret4{1} ; 
        if( ret4 != GetRoomIdsBySession(2)) {
            CHECK(false);
        }
        std::set<uint32_t> ret5{2} ; 
        std::set<III> ret55{2} ; 
        if( ret5 != GetRoomIdsBySession(4)) {
            CHECK(false);
        }
        if( ret55 != GetRoomsBySession(4)) {
            CHECK(false);
        }
        std::set<uint32_t> ret6{} ; 
        if( ret6 != GetRoomIdsBySession(5)) {
            CHECK(false);
        }
        //ReleaseRoom 
        ReleaseRoom(2);
        if( ret4 != GetRoomIdsBySession(1)) {
            CHECK(false);
        }
        CHECK_EQUAL(false , HaveRoom(2)) ;

        // ReleaseSession
        EnterRoom(6,1);
        EnterRoom(7,1);
        ReleaseSession(1);
        CHECK_EQUAL(true, HaveSession(2)) ;
        CHECK_EQUAL(false , HaveSession(1)) ;
        std::set<uint32_t> ret311{} ; 
        if( ret311 != GetRoomIdsBySession(1) ) {
            CHECK(false);
        }
    }
}
