#ifndef __GATEWAY_SESSIONMGR_H__
#define __GATEWAY_SESSIONMGR_H__

#include <memory>
#include <set>
#include <unordered_map>
#include <map>
#include "boost/bimap/bimap.hpp"
#include "boost/bimap/multiset_of.hpp"
/**
 * addgroup Session
 * @{
 */
#include "../Common/Include/IdBase.h"
namespace Session {

    struct roomtag {} ;

    struct sessiontag {} ;

    template<class ARoom, class ASession , class I1 = uint32_t , class I2 = uint32_t>
        class SessionMgr
        {
            public:

                typedef IDBASE::IdBase<ARoom , I1> RoomType ;

                typedef IDBASE::IdBase<ASession, I2> SessionType ;

                typedef std::pair<RoomType , SessionType> DataPair;

                typedef boost::bimaps::bimap< 
                    boost::bimaps::multiset_of< boost::bimaps::tagged<I1 , roomtag> >
                    ,boost::bimaps::multiset_of< boost::bimaps::tagged<I2, sessiontag > >
                    ,boost::bimaps::left_based 
                    /*,boost::bimaps::with_info<DataPair> */
                    >  bm;

                typedef typename bm::value_type bm_value;

                typedef typename bm::left_value_type bm_lvalue;

                typedef typename bm::right_value_type bm_rvalue;

            public:

                void CreateRoom(const RoomType & i )
                {
                    if( rooms.find(i.GetId()) == rooms.end() ) 
                    {
                        rooms[i.GetId()] = i ;
                    }
                }

                //! Release a room , all sessions in that room leave romm .
                void ReleaseRoom( const I1 & i ) 
                {
                    auto & left =  r_s_map.left;
                    auto range = left.equal_range(i);
                    left.erase(range.first , range.second);
                    rooms.erase(i);
                }

                void ReleaseRoom( const RoomType & i)
                {
                    ReleaseRoom(i.GetId());
                }

                void ReleaseSession(const I2 & i ) 
                {
                    auto & right =  r_s_map.right;
                    auto range = right.equal_range(i);
                    right.erase(range.first , range.second);
                    sessions.erase(i) ;
                }

                /*! 
                 * @return 
                 *      true if room had created but not realsed .
                 *      false otherwise .
                 */
                bool HaveRoom(const I1 & i)
                {
                    return rooms.find(i) != rooms.end() ;
                }
                /*! let s enter room i .
                 * create a room if room i not exsit.
                 */
                void EnterRoom(const RoomType & i , const SessionType & s )
                {
                    if( rooms.find(i.GetId()) == rooms.end() ) 
                    {
                        rooms[i.GetId()] = i ;
                    }
                    if( sessions.find( s.GetId()) == sessions.end() )
                    {
                        sessions[s.GetId()] = s ;
                    }
                    r_s_map.insert(
                            bm_value( i.GetId() , s.GetId() ) 
                            ) ;

                }
                /*! let s leave room i .
                 */
                void LeaveRoom(const I1 & i , const SessionType & s )
                {
                    auto & right = r_s_map.right ;
                    auto range = right.equal_range(s.GetId()) ;
                    if( range.first == right.end() ) 
                    {
                        return ;
                    }
                    for( auto itr = range.first ; itr != range.second ; itr ++ ) 
                    {
                        if( itr->second == i )
                        {
                            right.erase(itr);
                        }
                    }
                }

                std::set<SessionType> GetSessionsByRoom(I1 i )
                {
                    auto & left =  r_s_map.left;
                    auto range = left.equal_range(i);
                    std::set<SessionType> ret ;
                    if( range.first == left.end() ) 
                    {
                        return ret;
                    }
                    for ( auto itr = range.first ; itr != range.second ; itr ++ )
                    {
                        ret.insert( GetSessionType(itr->second));
                    }
                    return ret ;
                }

                std::set<SessionType> GetSessionsByRoom(const RoomType & i)
                {
                    return GetSessionsByRoom(i.GetId());
                }

                std::set<I2> GetSessionIdsByRoom(I1 i)
                {
                    auto & left =  r_s_map.left;
                    auto range = left.equal_range(i);
                    std::set<I2> ret ;
                    if( range.first == left.end() ) 
                    {
                        return ret;
                    }
                    for ( auto itr = range.first ; itr != range.second ; itr ++ )
                    {
                        ret.insert(itr->second);
                    }
                    return ret ;
                }

                std::set<I2> GetSessionIdsByRoom(const RoomType & i) 
                {
                    return GetSessionIdsByRoom(i.GetId() );
                }


                std::set<I1> GetRoomIdsBySession(I2 i) 
                {
                    auto & right =  r_s_map.right;
                    auto range = right.equal_range(i);
                    std::set<I2> ret ;
                    if( range.first == right.end() ) 
                    {
                        return ret ;
                    }
                    for ( auto itr = range.first ; itr != range.second ; itr ++ )
                    {
                        ret.insert(itr->second);
                    }
                    return ret ;
                }
                //! Get rooms ids that session i in.
                std::set<RoomType> GetRoomsBySession(I2 i) 
                {
                    auto & right =  r_s_map.right;
                    auto range = right.equal_range(i);
                    std::set<RoomType> ret ;
                    if( range.first == right.end() ) 
                    {
                        return ret ;
                    }
                    for ( auto itr = range.first ; itr != range.second ; itr ++ )
                    {
                        ret.insert(GetRoomById(itr->second));
                    }
                    return ret ;
                }
                std::set<RoomType> GetRoomsBySession(const SessionType&i)
                {
                    return GetRoomsBySession(i.GetId());
                }

                std::set<I1> GetRoomIdsBySession(const SessionType & s ) 
                {
                    return GetRoomIdsBySession(s.GetId());
                }

                RoomType GetRoomById( const I1 & i ) 
                {
                    return rooms.at(i);
                }

                SessionType GetSessionType( const I2 & i)
                {
                    return sessions.at(i);
                }

                bool HaveSession( const I2 & i ) 
                {
                    return sessions.find(i) != sessions.end() ;
                }

            protected:

                bm r_s_map;

                std::unordered_map<I1, RoomType>  rooms ;

                std::unordered_map<I2, SessionType> sessions;
        };
}
/** @} */
#endif //__GATEWAY_SESSIONMGR_H__
