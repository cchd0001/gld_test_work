#ifndef __GATEWAY_GATEROOM_H__
#define __GATEWAY_GATEROOM_H__
#include <string>
#include <cstdint>
#include "../Common/Include/IndexMgr.h"
#include "../Common/Include/GateID.h"

namespace Session{

    //! Room id for sessions that accepted but not authoritied .
    constexpr int  UNKOWN_SESSION_ROOM = 0 ;
    //! Room id for sessions that treat as game server.
    constexpr int  SERVER_SESSION_ROOM = 1 ;
    //! Room id for sessions that detected invalid.
    constexpr int  INVALID_SESSION_ROOM = 2 ;

    /**
     *  ID 3 - 29 was for broadcast room of each server.
     *  Here assume no more that a gateway can deal no more 
     *  than 27 servers .
     */

    //! Costom room id start by this .
    constexpr int  COSTOM_SESSION_ROOM_STARTID = 30 ;

    typedef uint16_t GateRoom ;

    struct RoomIdGenerator
    {

        typedef IndexMgr_ThreadSafe<uint16_t , 30 , Session::MaxMulticastSize> CustomRoomIndexType ;

        typedef IndexMgr_ThreadSafe<uint16_t , 3 , 30> BroadcastRoomIndexType ;

        CustomRoomIndexType m_room_index ;

        BroadcastRoomIndexType m_broadcast_index ;
    };

}

#endif //__GATEWAY_GATEROOM_H__
