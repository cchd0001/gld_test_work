#ifndef __GATEWAY_GATEWAY_H__
#define __GATEWAY_GATEWAY_H__

#include "../Common/TcpCommon/TcpServer.h"
#include "../Common/TcpCommon/ServerCommon.h"
#include <string>
#include "../Common/Include/IndexMgr.h"
#include "../Common/Include/GateID.h"

void OnClientAccept( const ConnectionPtr&) ;

void OnServerAccept( const ConnectionPtr&) ;

constexpr char  local_host [] = "127.0.0.1";

class GatewayTcpServer
{
    public:

        GatewayTcpServer( int16_t client_port
                ,int16_t server_port 
                ,size_t client_pool_size
                ,size_t server_pool_size ) ;

    public:

        void Run() ;

        void SetClientCallBacks(const ConnectionCallBacks& cb);

        void SetServerCallBacks(const ConnectionCallBacks& cb);

        void Stop();

    private:

        // Nothing to to for server.
        void OnConnected(const ConnectionPtr& /*conn*/, bool /*success*/) {}

        void OnWrite(const ConnectionPtr& /*conn*/, size_t /*len*/){}

        void OnDisconnect(const ConnectionPtr& conn);

        size_t SplitRawData(const ConnectionPtr& conn,
                            const uint8_t *buf,
                            size_t len ,
                            std::function<void(const ConnectionPtr& conn, const uint8_t *buf, size_t len)> f
                            );

        void OnReadClient(const ConnectionPtr& conn, const uint8_t *buf, size_t len) ;

        void OnReadServer(const ConnectionPtr& conn, const uint8_t *buf, size_t len) ;

        void HandleRawCommand( const uint8_t *buf, size_t len ) ;
    private:

        TcpServer  m_game_client;

        TcpServer  m_game_server;

        typedef IndexMgr_ThreadSafe<Session::SessionID,1,Session::SinglecastMark +1> ConnIndexType ;

        ConnIndexType m_conn_index;
};

#endif //__GATEWAY_GATEWAY_H__
