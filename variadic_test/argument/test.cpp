#include <iostream>
#include <cstdarg>
#include <string>
/*
////////////////////////////////////////////////
// 
// Access variadic argument one by one .
//
void print_args_all_int( int num , ... ) {
    va_list va ;
    va_start(va,num);
    std::cout<<" Total "
             <<num
             <<" variadic int argument : ";
    for( int i = 0; i < num ; i ++ ) {
        std::cout<<va_arg(va,int)<<" ";
    }
    std::cout<<std::endl;
}
////////////////////////////////////////////////
// 
// Print variadic twice.
//
void print_args_all_int_twice( int num , ... ) {
    va_list va , va_another;
    va_start(va,num);
    // Copy the va_list variable before start any va_xxx function
    va_copy(va_another, va);
    std::cout<<" Total "
             <<num
             <<" variadic int argument : ";
    for( int i = 0; i < num ; i ++ ) {
        std::cout<<va_arg(va,int)
                 <<" ";
    }
    std::cout<<std::endl;
    // Print again : 
    std::cout<<" Total "
             <<num
             <<" variadic int argument : ";
    for( int i = 0; i < num ; i ++ ) {
        std::cout<<va_arg(va_another,int)
                 <<" ";
    }
    std::cout<<std::endl;
    va_end(va);
    va_end(va_another);
    return ;
}

////////////////////////////////////////////////
// 
// pass va_list as argument.
//

void print_first_arg_recursive( int num ,  va_list va ) {
    if( num < 1 ) {
        return ;
    }
    std::cout<<num
             <<" arg is :"
             <<va_arg(va,int)
             <<std::endl;
    if(num > 1) {
        print_first_arg_recursive(num - 1, va ) ;
        va_end(va);
    }
}

void print_args_all_int_1( int num , ... ) {
    va_list va ;
    va_start(va,num);
    print_first_arg_recursive(num,va);
    va_end(va);
}

int main() {
    std::cout<<"-----------------------------------"<<std::endl;
    print_args_all_int(5,1,2,3,4,5);
    std::cout<<"-----------------------------------"<<std::endl;
    print_args_all_int_twice(5,1,2,3,4,5);
    std::cout<<"-----------------------------------"<<std::endl;
    print_args_all_int_1(5,1,2,3,4,5);
    std::cout<<"-----------------------------------"<<std::endl;
}*/


template <class ... arg>
class T {
    public:
    T() {
        std::cout<<sizeof...(arg)<<std::endl;
    }
} ;

template <>
class T <int , int > {
    public:
    T() {
        std::cout<<" 2 int "<<std::endl;
    }
} ;

template <>
class T <int , int , int  >{
    public:
    T() {
        std::cout<<" 3 int "<<std::endl;
    }
} ;
template <class I>
class T <I, int , int  >{
    public:
    T() {
        std::cout<<" x and 2 int "<<std::endl;
    }
} ;


//template<class T , class ...arg>
//void Print_first(T t, arg...va){
//    std::cout<<t<<std::endl;
// //   Print_first(va...);
//}
//
//template<class T>
//void Print_first(T t){
//    std::cout<<t<<std::endl;
//}
//
template<class ...arg>
size_t get_argument_size(arg...va) { 
    return sizeof...(va);
}


template<class ... arg>
void pass_arguments(arg ...va){
    std::cout<<get_argument_size(va...)<<std::endl;
}



template<class T ,  class ...Arg>
void pass_arg_by_recursive(T head , Arg ... va ){
    std::cout<<"One arg is "<<head<<std::endl;
    pass_arg_by_recursive( va...);
}

template<>
void pass_arg_by_recursive(int head  ){
    std::cout<<"Last one arg is i "<<head<<std::endl;
}
template<>
void pass_arg_by_recursive(float head  ){
    std::cout<<"Last one arg is f"<<head<<std::endl;
}

void printf1(const char *s)
{
    while (*s) {
        if (*s == '%') {
            if (*(s + 1) == '%') {
                ++s;
            }
            else {
                //throw std::runtime_error("invalid format string: missing arguments");
            }
        }
        std::cout << *s++;
    }
}

template<typename T, typename... Args>
void printf1(const char *s, T value, Args... args)
{
    while (*s) {
        if (*s == '%') {
            if (*(s + 1) == '%') {
                ++s;
            }
            else {
                std::cout << value;
                s += 2; // this only works on 2 characters format strings ( %d, %f, etc ). Fails miserably with %5.4f
                printf(s, args...); // call even when *s == 0 to detect extra arguments
                return;
            }
        }
        std::cout << *s++;
    }    
}


template <class T , class ... arg>
class Test : public Test<arg...>{
public:

    Test(T t, arg ...va) : t_(t) , Test<arg...>(va...) {}

    void PrintT() {
        std::cout<<t_<<std::endl;
        Test<arg...>::PrintT();
    }

private :

    T t_;
};

template<>
class Test<std::string>{
public:

   Test(std::string t) :t_(t) {}

    void PrintT() {
        std::cout<<t_<<std::endl;
    }

private :

    std::string t_;
};



template <class size_t , class T , class ... arg>
class Test1 : public Test1<size_t, arg...>{
public:
    Test1(T t, arg ...va) : t_(t) , Test1<size_t,arg...>(va...) {}

    void PrintT() {
        std::cout<<t_<<std::endl;
        Test1<size_t,arg...>::PrintT();
    }

private :

    T t_;
};

template<class size_t ,class T>
class Test1<size_t,T>{
public:
    
    Test1(T t) : t_(t) {}

    void PrintT() {
        std::cout<<t_<<std::endl;
    }

private :

    T t_;
};

#define CALL_F(...)  f(__VA_ARGS__)

template<class ...Arg>
void f(Arg...va) {
    std::cout<<sizeof...(va)<<std::endl;
}

int main() {
    Test<int,float,std::string> m{1,1.1f,std::string("test")};
    m.PrintT();
    Test1<size_t,int,float,std::string> m1{1,1.1f,std::string("test")};
    m1.PrintT();
    CALL_F(1,2,3,4,5,6,67);
    return 0;

   // std::cout<<get_argument_size(1,2,3,4,5,6)<<std::endl;
   // pass_arguments(1,2,3);
   // pass_arg_by_recursive(1,2,34,4,5,67);
   // pass_arg_by_recursive(1.3f,67.2f );
   //// T<int, int ,int >  m;
   //// T<short, int ,int >  e;
   //// T<int, int  >  n;
   // T<double> l;
   // printf("%s,%s","aaa","bbbb");
   // //Print_first(1,2);
}
