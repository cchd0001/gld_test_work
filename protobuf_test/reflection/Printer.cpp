#include "Printer.h"
#include <map>
#include <iostream>


#include <google/protobuf/descriptor.h>
#include <google/protobuf/message.h>


namespace Printer {
    //! 利用类型名字构造对象.
    /*!
     * @Param type_name 类型名字，比如 "Test.TestMessage".
     * @Return 对象指针，new 出来的，使用者负责释放.
     */
    inline google::protobuf::Message* createMessage(const std::string& type_name)
    {
        google::protobuf::Message* message = NULL;
        const google::protobuf::Descriptor* descriptor =
            google::protobuf::DescriptorPool::generated_pool()->FindMessageTypeByName(type_name);
        if (descriptor)
        {
            const google::protobuf::Message* prototype =
                google::protobuf::MessageFactory::generated_factory()->GetPrototype(descriptor);
            if (prototype)
            {
                message = prototype->New();
            }
        }
        return message;
    }

    void Print(const std::string & str , const std::string &data) {
        auto message = createMessage(str);
        if(message->ParseFromString(data))
        {
            std::cout<<message->Utf8DebugString()<<std::endl;
        }
        else
        {
            std::cout<<" Parse to message "<<str<<" failed ! Raw data is : "<< data <<std::endl;
        }
    }
}
