#include "cpp/test.pb.h"
#include <iostream>
#include <google/protobuf/compiler/importer.h>
#include <google/protobuf/dynamic_message.h>
#include "source_proto/test1.pb.h"
int main()
{
    std::cout<<"Begin : "<<std::endl;
    Test::Person p_test ;
    auto descriptor = p_test.GetDescriptor() ;
    auto reflecter = p_test.GetReflection() ;
    auto field = descriptor->FindFieldByName("id");
    reflecter->SetInt32(&p_test , field , 5 ) ;
    std::cout<<reflecter->GetInt32(p_test , field)<< std::endl ;
    std::cout<<p_test.Utf8DebugString()<< std::endl ;
    std::cout << descriptor->DebugString();
    std::cout<<"End: "<<std::endl;

    google::protobuf::compiler::DiskSourceTree sourceTree;

    sourceTree.MapPath("A","./");

    google::protobuf::compiler::Importer importer(&sourceTree, NULL);

    //runtime compile foo.proto

    importer.Import("A/source_proto/test1.proto");
    auto descriptor1 = importer.pool()->FindMessageTypeByName("T.Test");
    google::protobuf::DynamicMessageFactory factory;
    auto proto1 = factory.GetPrototype(descriptor1);
    auto message1= proto1->New();
    auto test = dynamic_cast<T::Test*>(message1);
    //test->set_id(3);
    auto reflection1 = message1->GetReflection();
    auto filed1 = descriptor1->FindFieldByName("id");
    reflection1->SetInt32(message1,filed1,1);
    std::cout << message1->DebugString();
    delete message1 ;
    return 0 ;
}
