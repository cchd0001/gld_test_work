﻿#ifndef __ZBASE64_H__
#define __ZBASE64_H__

#include <string>

class ZBase64
{
public:
	static std::string Encode(const unsigned char* Data,int DataByte);
	static std::string Decode(const char* Data,int DataByte,int& OutByte);
};

#endif
