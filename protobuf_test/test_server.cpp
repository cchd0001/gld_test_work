#include "map.pb.h"
#include <google/protobuf/service.h>


class ExampleSearchService : public servers_test::SearchService {
 public:
  void Search(google::protobuf::RpcController* controller,
              const servers_test::SearchRequest* request,
              servers_test::SearchResponse* response,
              google::protobuf::Closure* done) {
    if (request->id() == 1) {
      response->set_id_ret(11);
    } else {
      response->set_id_ret(22);
    }
    done->Run();
  }
};

int main() {
  // You provide class MyRpcServer.  It does not have to implement any
  // particular interface; this is just an example.
  google::MyRpcServer server;

  google::protobuf::Service* service = new google::ExampleSearchService;
  server.ExportOnPort(1234, service);
  server.Run();

  delete service;
  return 0;
}
