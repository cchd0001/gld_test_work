#include <stdio.h>
#include <fstream>
#include <string.h>
#include "timeLog.h"
#include "timeLog.h"

#define BLOCK_SIZE 100
#define LOOP 10000000

void write_c_file()
{

   char test02[BLOCK_SIZE+1];
    memset(test02,'A',BLOCK_SIZE*sizeof(char));
    test02[BLOCK_SIZE] = 0;
    FILE * fd = fopen("test02","w");
    for(int i = 0 ; i< LOOP; i++)
    {
        fprintf(fd,"%s\n",test02);
    }
    fclose(fd);

}

void write_cpp_file()
{

    std::ofstream of;
    //std::string test01(BLOCK_SIZE,'A');
   char test02[BLOCK_SIZE+1];
    memset(test02,'A',BLOCK_SIZE*sizeof(char));
    test02[BLOCK_SIZE] = 0;
    of.open("test01",std::ios::out);
    for( int i = 0 ; i < LOOP; i++ )
    {
        of<<test02<<std::endl;
    }
    of.close();

}

int main()
{
    TASK_BEGIN(Benchmark);
    write_cpp_file();
    CHECKPOINT(cpp write file end);

    write_c_file();
    CHECKPOINT(c write file end);
    TASK_END; 
}
