#include <stdio.h>
#include <ctime>
#include <cassert>
long GetUnixTimestampBy(int year , int month , int day, int hour, int min , int sec){
    std::tm tm;
    tm.tm_sec = sec;
    tm.tm_min = min;
    tm.tm_hour = hour;
    tm.tm_wday = 0;
    tm.tm_yday = 0;

    tm.tm_year = year - 1900 ;
    tm.tm_mon = month -1 ;
    tm.tm_mday = day;
    return static_cast<long>(mktime(&tm));
}

constexpr long minute = 60 ;
constexpr long hour= 3600 ;
constexpr long day = 3600 * 24 ;

time_t GetStartOfMonday( time_t stamp )
{
    std::tm *tm = std::localtime(&stamp);
    if( NULL == tm ) 
    {
        return 0 ;
    }

    int day_pass = tm->tm_wday == 0 ? 6 : tm->tm_wday -1 ;

    return 
         stamp - day_pass*day 
            - tm->tm_hour * hour 
            - tm->tm_min * minute 
            - tm->tm_sec ;
}


time_t GetThisWeekHourStamp(time_t now  , int dweek , int the_hour ) 
{
    assert( 0<=dweek && dweek < 7 ) ;
    assert( the_hour > 0 && the_hour < 24 ) ;
    time_t monday_start = GetStartOfMonday( now ) ;

    int day_pass =  dweek== 0 ? 6 : dweek -1 ;

    return monday_start + day_pass * day + the_hour * hour ;
}

int main(){

    printf("%d\n" ,GetUnixTimestampBy(2016,1,1,12,30,50));
    auto a  = GetStartOfMonday( time(0) );
    auto b  = GetStartOfMonday( a - 1 ) ;
    auto c  = GetStartOfMonday( a + 1 ) ;
    auto d  = GetStartOfMonday( b + 1 ) ;
    auto e  = GetStartOfMonday( b - 1 ) ;


    printf(" a %d\n" , a);
    printf(" b %d\n" , b);
    printf(" c = a %d\n" , c);
    printf(" d = b %d\n" , d);
    printf(" e %d\n" , e);


    printf(" 1 , 11  %d\n", GetThisWeekHourStamp( a, 1 , 11) );
    printf(" 7 , 11  %d\n", GetThisWeekHourStamp(a, 0 , 11) );
    printf(" 3 , 0  %d\n", GetThisWeekHourStamp( a,3, 0) );
    return 0;
}
