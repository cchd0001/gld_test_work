 #include <ctime>
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
int main(){
    timeval start , end;
    gettimeofday(&start,0);
    sleep(3);
    gettimeofday(&end,0);
    printf("Sleep %d millseconds\n",(end.tv_sec-start.tv_sec)*1000 + (end.tv_usec - start.tv_usec)/1000);
    return 0;
}
