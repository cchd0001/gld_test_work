#include <stdio.h>
#include <ctime>

int main(){
    std::time_t rawtime;
    struct std::tm * timeinfo;

    std::time(&rawtime);
    timeinfo = std::localtime(&rawtime);

    int year = timeinfo->tm_year;
    int mon = timeinfo->tm_mon + 1; 
    int day = timeinfo->tm_day;
    printf("Year %d Mon %d Day %d \n",year,mon,day);
    return 0;
}
