#ifndef _TIMER_HELPER_H
#define _TIMER_HELPER_H

#include <ctime>

namespace TimerHelper
{

    constexpr int SECONDS_ONE_DAY = 24 * 60 * 60;
    constexpr int SECONDS_ONE_HOUR = 60 * 60;
    constexpr int SECONDS_ONE_MINUTE = 60;

    int GetWaitSeconds(); // seconds till end of the day

    int GetYearMonDay(); // example: 20150520

    int GetYear(); // like 2015

    int GetMonth(); // 0 - 11

    int GetDayOfMonth();  // 1-31

    int GetDayOfWeek(); // 0-6 means Sunday - Saturday

    time_t GetUnixTimestamp(); // unix timestamp

    time_t GetUnixTimestampBy(int year , int month , int day); // get the unix timestamp of the 0:00:00 of a special day

    // get the unix timestamp of a special time
    time_t GetUnixTimestampBy(int year , int month , int day, int hour, int min , int sec) ;

    int GetYearMonDay(time_t timestamp); // example: 20150520

    int GetYear(time_t timestamp);

    int GetMonth(time_t timestamp); // 0 - 11

    int GetDayOfMonth(time_t timestamp);  // 1-31

    int GetDayOfWeek(time_t timestamp); // 0-6

    // Return n ( n> 0) means timestamp1 is n day later then timestamp2 in local time;
    // Return -n ( n>0 ) means timestamp2 is n day later then timestamp1 in local time;
    // Return 0 means they are in the same local day ;
    int DiffDay(time_t timestamp1, time_t timestamp2);

    //! calculate the 00:00:00 of Monday by given timestamp.
    time_t GetStartOfMonday( time_t stamp ); 

    //! calculate the timestamps of give wday and hour by given timestamp.
    time_t GetThisWeekHourStamp(time_t now  , int dweek , int the_hour ) ;
}

#endif
