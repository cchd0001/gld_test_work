#include "TimerHelper.h"
#include <cassert>

namespace TimerHelper {
    int GetWaitSeconds() {
        std::time_t t = std::time(nullptr);
        std::tm *tm = std::localtime(&t);
        int seconds = tm->tm_hour * 60 * 60 + tm->tm_min * 60 + tm->tm_sec;
        seconds = SECONDS_ONE_DAY - seconds;
        return (seconds > 0) ? seconds : SECONDS_ONE_DAY;
    }

    int GetYearMonDay() {
        std::time_t t = GetUnixTimestamp();
        return GetYearMonDay(t);
    }

    time_t GetUnixTimestamp() {
        return time(nullptr);
    }

    int GetYear() {
        return GetYear(GetUnixTimestamp());
    }

    int GetMonth() {
        return GetMonth(GetUnixTimestamp());
    };

    int GetDayOfMonth() {
        return GetDayOfMonth(GetUnixTimestamp());
    }

    int GetDayOfWeek() {
        return GetDayOfWeek(GetUnixTimestamp());
    }

    int GetYearMonDay(time_t timestamp) {
        std::tm *tm = std::localtime(&timestamp);
        int number = (tm->tm_year + 1900) * 10000 + (tm->tm_mon + 1) * 100 + tm->tm_mday;
        return number;
    }

    int GetMonth(time_t t) // 0 - 11
    {
        std::tm *tm = std::localtime(&t);
        return tm->tm_mon;
    }

    int GetYear(time_t t)
    {
        std::tm *tm = std::localtime(&t);
        return tm->tm_year + 1900;
    }

    int GetDayOfMonth(time_t t) {  // 1-31
        std::tm *tm = std::localtime(&t);
        return tm->tm_mday;
    }

    int GetDayOfWeek(time_t t) { // 0-6
        std::tm *tm = std::localtime(&t);
        return tm->tm_wday;
    }

    time_t GetUnixTimestampBy(int year , int month , int day){
        return GetUnixTimestampBy(year,month,day,0,0,0);
    }

    time_t GetUnixTimestampBy(int year , int month , int day, int hour, int min , int sec){
        std::tm tm;
        tm.tm_sec = sec;
        tm.tm_min = min;
        tm.tm_hour = hour;
        tm.tm_wday = 0;
        tm.tm_yday = 0;

        tm.tm_year = year - 1900 ;
        tm.tm_mon = month -1 ;
        tm.tm_mday = day;
        return mktime(&tm);
    }

    int DiffDay(time_t d1, time_t d2){
        if ( d1 == d2 ){
            return 0;
        }
        int flag ;
        time_t  big , small ;
        if( d1 > d2 ){
            big = d1;
            small = d2;
            flag = 1;
        } else {
            big = d2;
            small = d1;
            flag = -1;
        }
        int gap = ( big - small )/SECONDS_ONE_DAY;
        time_t big_left = small + ( big - small )%SECONDS_ONE_DAY;
        if(TimerHelper::GetDayOfWeek(big_left)!=TimerHelper::GetDayOfWeek(small)){
            gap ++;
        }
        return gap * flag;
    }


    time_t GetStartOfMonday( time_t stamp )
    {
        std::tm *tm = std::localtime(&stamp);
        if( NULL == tm ) 
        {
            return 0 ;
        }

        int day_pass = tm->tm_wday == 0 ? 6 : tm->tm_wday -1 ;

        return 
            stamp - day_pass*SECONDS_ONE_DAY
            - tm->tm_hour * SECONDS_ONE_HOUR
            - tm->tm_min * SECONDS_ONE_MINUTE
            - tm->tm_sec ;
    }


    time_t GetThisWeekHourStamp(time_t now  , int dweek , int the_hour ) 
    {
        assert( 0<=dweek && dweek < 7 ) ;
        assert( the_hour > 0 && the_hour < 24 ) ;
        time_t monday_start = GetStartOfMonday( now ) ;

        int day_pass =  dweek== 0 ? 6 : dweek -1 ;

        return monday_start + day_pass * SECONDS_ONE_DAY + the_hour * SECONDS_ONE_HOUR ;
    }
}
