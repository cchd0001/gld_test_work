#include <stdio.h>
#include <ctime>
#define DAY_SECONDS (3600*24)

 int GetDayOfWeek(long t) { // 0-6
		std::tm *tm = std::localtime((time_t*)&t);
        return tm->tm_wday;
    }

static int diffday(long d1, long d2){
	if ( d1 == d2 ){
		return 0;
	}
	long  big , small ;
	if( d1 > d2 ){
		big = d1;
		small = d2;
	} else {
		big = d2;
		small = d1;
	}
	int gap = ( big - small )/DAY_SECONDS;
	long big_left = small + ( big - small )%DAY_SECONDS;
	if(GetDayOfWeek(big_left)!=GetDayOfWeek(small)){
		gap ++;
	}
	return gap;
}

int main(){
    long a = 3600 * 16 + 5;
    long b = 3600 * 16 -1; // 东八区 
    long c = 3600 * 42 +3;
    printf("%d %d %d\n",diffday(a,b),diffday(b,c) , diffday(c,a));
    return 0;
}
