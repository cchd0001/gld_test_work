#include "./BlockKeyWords.h"
#include "./KeyWords.h"
#include <sstream>
#include <iostream>
namespace KeyWordsBlock {

    void CheckString(std::string & text){
        ForEach([&](const std::regex & r){
                std::smatch sm; 
                if( std::regex_match (text,sm,r) ) {
                    // Matched 
                    std::ostringstream ost;
                    // Pre-define : all regex should be (.*)(XXX)(.*) format
                    // XXX is the real key word regex 
                   // std::cout<<sm.size()<<std::endl;
                   // for(int i = 0 ; i < sm.size() ; i++ ){
                   //     std::cout<<sm[i]<<std::endl;
                   // }
                    ost<<sm[1] << std::string(sm[2].length(),'*') <<sm[sm.size()-1];
                    text = ost.str();
                }
                });
    }
}
