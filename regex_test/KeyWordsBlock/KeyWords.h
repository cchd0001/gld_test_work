#ifndef __KEYWORDSBLOCK_KEYWORDS_H__
#define __KEYWORDSBLOCK_KEYWORDS_H__

#include <regex>

namespace KeyWordsBlock {

    void Init() ;

    void ForEach(std::function<void(const std::regex & r)>);

    void TestInit();
}

#endif // __KEYWORDSBLOCK_KEYWORDS_H__
