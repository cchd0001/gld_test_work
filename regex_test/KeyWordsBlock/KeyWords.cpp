#include "./KeyWords.h"
#include <map>
#include <mutex>
//#include ""
//

namespace KeyWordsBlock {

    struct RegexMap {
        std::mutex mutex ;
        std::map<std::string , std::regex> regexs;

        void PushRegex(const std::string & str) {
            regexs[str] = std::regex(str);
        }
    };

    static RegexMap & GetRegexMapMgr(){
        static RegexMap r ;
        return r;
    }

    void TestInit() {
        GetRegexMapMgr().PushRegex("\\w[-\\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\\.)+[A-Za-z]{2,14})(.*)");
    }

    void ForEach( std::function<void (const std::regex & r) > f)  {
        std::lock_guard<std::mutex>  l (GetRegexMapMgr().mutex);
        for(const auto & i : GetRegexMapMgr().regexs){
            f(i.second);
        }
    }
}
