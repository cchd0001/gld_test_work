#ifndef __KEYWORDSBLOCK_BLOCKKEYWORDS_H__
#define __KEYWORDSBLOCK_BLOCKKEYWORDS_H__

#include <string>

namespace KeyWordsBlock {

    void CheckString(std::string & text);
}

#endif // __KEYWORDSBLOCK_BLOCKKEYWORDS_H__
