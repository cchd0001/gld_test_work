#include "Rank.h"
#include <algorithm>

namespace Rank {
    

    //////////////////////////////////////////////////////////////////////////
    //
    // RankItem
    //
    /////////////////////////////////////////////////////////////////////////

    static const RankItem biggest{};

    static const RankItem least{};

    bool RankItem::Update( long long s ) {
        if( s != score ) {
            score = s ;
            modify_time = time(0);
            return true ;
        }
        return false ;
    }
    
    bool RankItem::operator < ( const RankItem it ) const{
        // Older one is bigger is score is the same .    
        if (this == &biggest || &it == &least ) {
            return false ;
        } else if ( this == &least || &it == &biggest ) {
            return true ;
        } else {
            return score < it.score || ( score == it.score && modify_time > it.modify_time ) ;
        }
    }

    //////////////////////////////////////////////////////////////////////////
    //
    // Rank
    //
    /////////////////////////////////////////////////////////////////////////

    Rank::Rank( const std::string & key ) : m_hash(key) {}
    
    void Rank::Update( const std::string & key , long long score ) {

        std::lock_guard<std::mutex> l(m_mutex);

        int pos = Find_NoLock(key) ;
        RankItem new_item ;
        if( pos >= 0 && pos < m_rank.size() ) {
            if ( !m_rank[pos].Update(score) ) { // nothing real change 
                return ;
            }
            if( m_rank.size() < 2 ) {
                return ;
            }
            if ( Get_NoLock(pos) < Get_NoLock(pos-1) && Get_NoLock(pos +1) < Get_NoLock(pos) ) {
                // Rank not change .
                return ;
            }
            new_item = Get_NoLock(pos) ;
            Erase_NoLock(pos);
        } else {
            new_item = RankItem {
                key ,
                score ,
                time(0)
            };
        }
        
        Insert_NoLock(new_item);
    }
    

    void Rank::SaveToDB_NoLock( const RankItem & item ) {
        // HSET( m_hash  , item.key , item.ToString() ) ;
    }

    void Rank::LoadFromDB() {
        // auto all_keys = HKEYS( m_hash ) 
        // for( const auto & i : all_keys  ){
        //      str = HGET ( m_hash  , i );
        //      RankItem it ;
        //      it.ParseFromString(str) ;
        //      Insert_NoLock(it);
        // }
    }

    long long Rank::GetScore( const std::string & key ) {
        std::lock_guard<std::mutex> l(m_mutex);
        int index = Rank::Find_NoLock( key ) ;
        if( index < 0 || index >= m_rank.size() ) {
            return -1;
        } else {
            return Get_NoLock(index).score ; 
        }
    }

    // 1 means rank 1 , the score highest .
    // and  2 ,3 ... means rank 2 , 3 ...
    // 0 meas not in rank
    int Rank::GetRank( const std::string & key ) {
        std::lock_guard<std::mutex> l(m_mutex);
        int index = Rank::Find_NoLock( key ) ;
        if( index < 0 || index >= m_rank.size() ) {
            return 0 ;
        }
        return index + 1 ;
    }

    // people in [start ,end] . 0 means highest
    RankList Rank::GetRankList(int start , int end) {
        std::lock_guard<std::mutex> l(m_mutex);
        RankList ret;
        ret.clear();
        if(end < start || end < 0) {
            return ret;
        }
        if( end > m_rank.size() -1 ) {
            end = m_rank.size() - 1 ;
        }
        
        for( int i = start ; i <= end ; i++ ) {
            ret.push_back(m_rank[i].key);
        }
    }

    void Rank::Insert_NoLock( const RankItem & item ){
        auto r_pos = std::lower_bound(m_rank.rbegin() , m_rank.rend() , item);
        auto new_pos = m_rank.begin() + m_rank.size() -( r_pos - m_rank.rbegin() ) ; 
        m_rank.insert(new_pos , item);
        
    }

    int Rank::Find_NoLock( const std::string & key ){
        auto pos = std::find_if( m_rank.begin() , m_rank.end() ,
            [&key] (const  RankItem & ri ) {
                return ri.key == key;
            }
        );
        if ( pos == m_rank.end() ) {
            return -1;
        } else {
            return pos - m_rank.begin();
        }
    }

    void Rank::Erase_NoLock( int index ){
        if ( index < 0 || index >= m_rank.size() ) {
            return ;
        }
        m_rank.erase(m_rank.begin() + index) ;
    }

    const RankItem & Rank::Get_NoLock( int index ) {
        if ( index < 0 ) { 
            return biggest ;  
        } else if( index >= m_rank.size() ) {
            return least ;   
        } else {
            return m_rank[index];
        }
    }
}

#if RANK_SELF_TEST
#include <iostream>
#include <thread>
#include <chrono>
#define TEST_EQUAL( expect , acutal ) \
    if( (expect ) != (acutal) ) { \
        std::cout<<" line "<<__LINE__<<" Expect "<<(expect)<<" but was "<<(acutal)<<std::endl;\
    } else { \
        std::cout<<"line "<<__LINE__<<"pass"<<std::endl;\
    }

int main() {
    Rank::Rank r("");
    
    TEST_EQUAL( -1 , r.Find_NoLock("test1") ) ; 
    TEST_EQUAL( -1 , r.GetScore("test1") ) ;
    
    r.Insert_NoLock( {"test1" , 1 , time(0) } );
    
    TEST_EQUAL( 0 , r.Find_NoLock("test1") ) ; 
    TEST_EQUAL( 1 , r.GetRank("test1") ) ; 
    TEST_EQUAL( 1 , r.GetScore("test1") ) ; 
    
    r.Insert_NoLock( {"test4" , 4 , time(0) } );

    TEST_EQUAL( 1 , r.GetScore("test1") ) ; 
    TEST_EQUAL( 4 , r.GetScore("test4") ) ; 
    
    TEST_EQUAL( 0 , r.Find_NoLock("test4") ) ; 
    TEST_EQUAL( 1 , r.Find_NoLock("test1") ) ; 
    
    r.Insert_NoLock( {"test3" , 3 , time(0) } );
    
    TEST_EQUAL( 1 , r.GetScore("test1") ) ; 
    TEST_EQUAL( 4 , r.GetScore("test4") ) ; 
    TEST_EQUAL( 3 , r.GetScore("test3") ) ; 
    
    TEST_EQUAL( 0 , r.Find_NoLock("test4") ) ; 
    TEST_EQUAL( 1 , r.Find_NoLock("test3") ) ; 
    TEST_EQUAL( 2 , r.Find_NoLock("test1") ) ; 
    
    r.Insert_NoLock( {"test5" , 5 , time(0) } );
    
    TEST_EQUAL( 0 , r.Find_NoLock("test5") ) ; 
    TEST_EQUAL( 1 , r.Find_NoLock("test4") ) ; 
    TEST_EQUAL( 2 , r.Find_NoLock("test3") ) ; 
    TEST_EQUAL( 3 , r.Find_NoLock("test1") ) ; 
    
    r.Insert_NoLock( {"test2" , 2 , time(0) } );
    
    TEST_EQUAL( 0 , r.Find_NoLock("test5") ) ; 
    TEST_EQUAL( 1 , r.Find_NoLock("test4") ) ; 
    TEST_EQUAL( 2 , r.Find_NoLock("test3") ) ; 
    TEST_EQUAL( 3 , r.Find_NoLock("test2") ) ; 
    TEST_EQUAL( 4 , r.Find_NoLock("test1") ) ; 

    r.Update("test1" , 9);
    
    TEST_EQUAL( 0 , r.Find_NoLock("test1") ) ; 
    TEST_EQUAL( 1 , r.Find_NoLock("test5") ) ; 
    TEST_EQUAL( 2 , r.Find_NoLock("test4") ) ; 
    TEST_EQUAL( 3 , r.Find_NoLock("test3") ) ; 
    TEST_EQUAL( 4 , r.Find_NoLock("test2") ) ; 
    
    r.Update("test2" , 7);
    
    TEST_EQUAL( 0 , r.Find_NoLock("test1") ) ; 
    TEST_EQUAL( 1 , r.Find_NoLock("test2") ) ; 
    TEST_EQUAL( 2 , r.Find_NoLock("test5") ) ; 
    TEST_EQUAL( 3 , r.Find_NoLock("test4") ) ; 
    TEST_EQUAL( 4 , r.Find_NoLock("test3") ) ; 
    
    std::this_thread::sleep_for(std::chrono::seconds(1));
    
    r.Update("test3" , 5);
    
    TEST_EQUAL( 0 , r.Find_NoLock("test1") ) ; 
    TEST_EQUAL( 1 , r.Find_NoLock("test2") ) ; 
    TEST_EQUAL( 2 , r.Find_NoLock("test5") ) ; 
    TEST_EQUAL( 3 , r.Find_NoLock("test3") ) ; 
    TEST_EQUAL( 4 , r.Find_NoLock("test4") ) ; 
    
    r.Update("test4" , 3);
    
    TEST_EQUAL( 0 , r.Find_NoLock("test1") ) ; 
    TEST_EQUAL( 1 , r.Find_NoLock("test2") ) ; 
    TEST_EQUAL( 2 , r.Find_NoLock("test5") ) ; 
    TEST_EQUAL( 3 , r.Find_NoLock("test3") ) ; 
    TEST_EQUAL( 4 , r.Find_NoLock("test4") ) ; 
    
    r.Update("test5" , 1);
    
    TEST_EQUAL( 0 , r.Find_NoLock("test1") ) ; 
    TEST_EQUAL( 1 , r.Find_NoLock("test2") ) ; 
    TEST_EQUAL( 2 , r.Find_NoLock("test3") ) ; 
    TEST_EQUAL( 3 , r.Find_NoLock("test4") ) ; 
    TEST_EQUAL( 4 , r.Find_NoLock("test5") ) ; 
    
    r.Update("test5" , 4);
    
    TEST_EQUAL( 0 , r.Find_NoLock("test1") ) ; 
    TEST_EQUAL( 1 , r.Find_NoLock("test2") ) ; 
    TEST_EQUAL( 2 , r.Find_NoLock("test3") ) ; 
    TEST_EQUAL( 3 , r.Find_NoLock("test5") ) ; 
    TEST_EQUAL( 4 , r.Find_NoLock("test4") ) ;
    
    auto ranks1 = r.GetRankList( 0 , 7 ) ;
    TEST_EQUAL( 5 , ranks1.size() );

    auto ranks2 = r.GetRankList( 0 , 3 ) ;
    TEST_EQUAL( 4 , ranks2.size() );

    r.Update("test4" , 1);
    
    TEST_EQUAL( 0 , r.Find_NoLock("test1") ) ; 
    TEST_EQUAL( 1 , r.Find_NoLock("test2") ) ; 
    TEST_EQUAL( 2 , r.Find_NoLock("test3") ) ; 
    TEST_EQUAL( 3 , r.Find_NoLock("test5") ) ; 
    TEST_EQUAL( 4 , r.Find_NoLock("test4") ) ;
    
    r.Update("test5" , 2);
    
    TEST_EQUAL( 0 , r.Find_NoLock("test1") ) ; 
    TEST_EQUAL( 1 , r.Find_NoLock("test2") ) ; 
    TEST_EQUAL( 2 , r.Find_NoLock("test3") ) ; 
    TEST_EQUAL( 3 , r.Find_NoLock("test5") ) ; 
    TEST_EQUAL( 4 , r.Find_NoLock("test4") ) ;
}
#endif // RANK_SELF_TEST
