#ifndef __RANK_H__
#define __RANK_H__

#include <string>
#include <mutex>
#include <vector>


#define RANK_SELF_TEST 1

namespace Rank {

    struct RankItem {

        bool Update( long long s ) ;

        bool operator < ( const RankItem it ) const;

        std::string ToString() const ;

        void ParseFromString( const std::string & str ) ;

        std::string key ;

        long long score;

        long modify_time;
    };

    typedef std::vector<std::string> RankList;

    class Rank {

        public:
            
            Rank(const std::string & key) ;

            void Update( const std::string & key , long long score ) ;

            long long GetScore( const std::string & key ) ;
            // 1 means rank 1 , the score highest .
            // and  2 ,3 ... means rank 2 , 3 ...
            // 0 meas not in rank
            int GetRank( const std::string & key ) ; 

            // people in [start ,end] . 0 means highest
            RankList GetRankList(int start , int end);

            void LoadFromDB() ;

#if RANK_SELF_TEST
        public:
#else
        private:
#endif // RANK_SELF_TEST          

            void Insert_NoLock( const RankItem & item );

            int Find_NoLock( const std::string & key ) ;

            void Erase_NoLock( int index ) ;
         
            const RankItem & Get_NoLock( int index ) ;

            void SaveToDB_NoLock(  const RankItem & item ); 

            std::mutex m_mutex ;

            std::vector< RankItem > m_rank;

            std::string m_hash;
    };
}



#endif // __RANK_H__
