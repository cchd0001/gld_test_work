package main

import (
	"fmt"
)

func main() {
	var buff [5]int32
	fmt.Println(len(buff))
	slice := buff[:]
	fmt.Println(len(slice))
	slice = buff[1:]
	fmt.Println(len(slice))
}
