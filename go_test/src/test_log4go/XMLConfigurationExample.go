package main

import l4g "log4go"

func main() {
	// Load the configuration (isn't this easy?)
	l4g.LoadConfiguration("example.xml")

	l4g.Finest("This will only go to those of you really cool UDP kids!  If you change enabled=true.")
	l4g.Info("About .....")
	l4g.Trace("About that time, eh chaps?")
	l4g.Trace("About that time, eh chaps?")
	l4g.Trace("About that time, eh chaps?")
	l4g.Debug("About that time, eh chaps?")
	l4g.Info("About that time, eh chaps?")
	l4g.Warn("About that time, eh chaps?")
	select {}
}
