package main

import "time"
import l4g "log4go"

func TestLog4Go1() {

	l4g.Error("..RRR.")
	l4g.Warn("...")
	log := l4g.NewLogger()
	defer log.Close()
	log.AddFilter("stdout", l4g.WARNING, l4g.NewConsoleLogWriter())
	log.Info("The time is now: %s", time.Now().Format("15:04:05 MST 2006/01/02"))
}

func main() {
	TestLog4Go1()
}
