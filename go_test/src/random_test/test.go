package main

import (
	c_rand "crypto/rand"
	"fmt"
	"math/big"
	m_rand "math/rand"
)

var end map[int64]int
var size int = 1000

func main() {
	end = make(map[int64]int)
	b := big.NewInt(int64(10))
	for i := 0; i < size; i++ {
		r, _ := c_rand.Int(c_rand.Reader, b)
		m := r.Int64()
		end[m]++
	}

	for i, j := range end {
		fmt.Print(i)
		for m := 0; m < j/(size/100); m++ {
			fmt.Print("*")
		}
		fmt.Println("	", j)
	}

	for i := 0; i < 11; i++ {
		fmt.Println(c_rand.Prime(c_rand.Reader, 64))
	}

	for i := 0; i < 11; i++ {
		fmt.Println(m_rand.ExpFloat64())
	}
}
