package main

import (
	"fmt"
	"reflect"
)

type ITest interface {
	TestString() string
}

func PrintTest(i ITest) {
	fmt.Println(i.TestString())
}

type Test struct {
}

func (t Test) TestString() string {
	return "func TestString"
}

func main() {

	var t Test
	PrintTest(t)

	var i int = 0
	var j interface{}
	j = i
	var m int = j.(int)
	fmt.Println(m)
	fmt.Println(reflect.TypeOf(j))
	fmt.Println(reflect.TypeOf(m))
}
