package main

import (
	"fmt"
)

func main() {
	var i int = 100
	var j *int = &i
	var m int = *j
	fmt.Println(m)
}
