package main

import (
	"conn/tcp"
	"fmt"
	"log"
)

type EchoClient struct {
}

func (this *EchoClient) Connect(conn *tcptool.TcpConn, e error) {
	if nil == conn && nil == e {
		log.Panic("Woo ... nil can't passed in here")
		return
	} else if nil != e {
		log.Panic(e)
	} else {
		fmt.Println("Connection ", conn.GetId())
	}
}

func (this *EchoClient) Disconnect(conn *tcptool.TcpConn, e error) {
	if nil == conn && nil == e {
		log.Panic("Woo ... nil can't passed in here")
		return
	} else if nil != e {
		fmt.Println("DisConnection ", conn.GetId())
		log.Print(e)
	} else {
		fmt.Println("DisConnection ", conn.GetId())
	}
}

func (this *EchoClient) Read(conn *tcptool.TcpConn, buff []byte) int32 {
	fmt.Println(string(buff))
	return int32(len(buff))
}

func (this *EchoClient) Send(conn *tcptool.TcpConn, len int32) {

}

func main() {
	var cb EchoClient
	cli := tcptool.NewTcpClient(&cb)
	cli.Init("127.0.0.1", "12345")

	for {
		var input string
		fmt.Scanf("%s", &input)
		buff := []byte(input)
		cli.Send(buff)
	}
}
