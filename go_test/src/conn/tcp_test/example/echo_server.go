package main

import (
	"conn/tcp"
	"fmt"
	"log"
	//"runtime"
)

type EchoServer struct {
}

func (this *EchoServer) Connect(conn *tcptool.TcpConn, e error) {
	if nil == conn && nil == e {
		log.Panic("Woo ... nil can't passed in here")
		return
	} else if nil != e {
		log.Panic(e)
	} else {
		fmt.Println("Connection ", conn.GetId())
	}
}
func (this *EchoServer) Disconnect(conn *tcptool.TcpConn, e error) {
	if nil == conn && nil == e {
		log.Panic("Woo ... nil can't passed in here")
		return
	} else if nil != e {
		fmt.Println("DisConnection ", conn.GetId())
		log.Print(e)
	} else {
		fmt.Println("DisConnection ", conn.GetId())
	}
}
func (this *EchoServer) Read(conn *tcptool.TcpConn, buff []byte) int32 {
	go conn.Send(buff)
	return int32(len(buff))
}
func (this *EchoServer) Send(conn *tcptool.TcpConn, len int32) {

}

func main() {
	var cb EchoServer
	ser := tcptool.NewTcpServer(&cb)
	ser.Init("127.0.0.1", "12345")
}
