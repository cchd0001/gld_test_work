package tcptool

import (
	//"fmt"
	"errors"
	"log"
	"net"
	"sync"
)

const BUFF_SIZE = 1024
const BUFF_MAX = 640 * 1024

type TcpConn struct {
	m_id     uint32
	m_conn   *net.TCPConn
	m_cb     Conncb
	m_recive []byte
	m_buff   [BUFF_SIZE]byte
	m_rw     sync.RWMutex
}

func NewTcpConn(conn *net.TCPConn, id uint32, cb Conncb) (*TcpConn, error) {
	if nil == conn {
		return nil, errors.New("Conn is nil")
	}
	ret := new(TcpConn)
	ret.m_id = id
	ret.m_conn = conn
	ret.m_recive = make([]byte, 0)
	ret.m_cb = cb
	ret.m_cb.Connect(ret, nil)
	return ret, nil
}

func (this *TcpConn) ResetCb(cb Conncb) {
	this.m_cb = cb
}

func (this *TcpConn) Send(buff []byte) {
	this.m_rw.RLock()
	defer this.m_rw.RUnlock()
	if nil == this.m_conn {
		log.Println("Send data to nil . connection id = ", this.m_id)
		return
	}
	send_buf := make([]byte, len(buff))
	copy(send_buf, buff)
	len_left := len(send_buf)
	total_send_len := 0
	for len_left > 0 {
		send_len, err := this.m_conn.Write(send_buf)
		if nil != err {
			go this.Close(err)
			break
		}
		len_left -= send_len
		total_send_len += send_len
		send_buf = send_buf[send_len:]
	}

	this.m_cb.Send(this, int32(total_send_len))
}

func (this *TcpConn) Read() error {
	this.m_rw.RLock()
	defer this.m_rw.RUnlock()
	if nil == this.m_conn {
		log.Println("Read data from nil . connection id = ", this.m_id)
		return errors.New("Read data from nil ")
	}
	read_buf := this.m_buff[:]
	read_len, err := this.m_conn.Read(read_buf)
	if nil != err {
		go this.Close(err)
		return err
	}
	if len(this.m_recive)+read_len > BUFF_MAX {
		err1 := errors.New("Recieve buff overflow ! Connection shot down ")
		go this.Close(err1)
		return err1
	}
	this.m_recive = append(this.m_recive, read_buf[:read_len]...)
	deal_len := this.m_cb.Read(this, this.m_recive)
	if deal_len > 0 {
		this.m_recive = this.m_recive[deal_len:]
	}
	return nil
}

func (this *TcpConn) Close(err error) {
	this.m_rw.Lock()
	defer this.m_rw.Unlock()
	if nil == this.m_conn {
		//log.Println("Close nil . connection id = ", this.m_id)
		return
	}
	this.m_cb.Disconnect(this, err)
	this.m_conn.Close()
	this.m_conn = nil
}

func (this *TcpConn) GetId() uint32 {
	return this.m_id
}
