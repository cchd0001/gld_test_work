package tcptool

import (
	//"fmt"
	"log"
	"net"
	//"sync"
)

type TcpClient struct {
	m_ip   string
	m_port string
	m_net  string
	m_conn *TcpConn
	m_cb   Conncb
}

func NewTcpClient(cb Conncb) *TcpClient {
	cli := new(TcpClient)
	cli.m_cb = cb
	return cli
}

func (this *TcpClient) Init(ip string, port string) {
	this.m_ip = ip
	this.m_port = port
	this.m_net = "tcp"
	addr, err2 := net.ResolveTCPAddr(this.m_net, this.m_ip+string(":")+this.m_port)
	if nil != err2 {
		log.Panic(err2)
		return
	}
	conn, err := net.DialTCP("tcp", nil, addr)
	if nil != err {
		log.Panic(err)
	}
	this.m_conn, err = NewTcpConn(conn, 1, this.m_cb)
	if nil != err {
		log.Panic(err)
	}
	go this.KeepRead()
}

func (this *TcpClient) KeepRead() {
	var err error
	for nil == err {
		err = this.m_conn.Read()
	}
}

func (this *TcpClient) Send(buff []byte) {
	if nil != this.m_conn {
		this.m_conn.Send(buff)
	}
}

func (this *TcpClient) ShotDown(err error) {
	if nil != this.m_conn {
		this.m_conn.Close(err)
	}
}
