package tcptool

import (
	//"fmt"
	"errors"
	"log"
	"net"
	"sync"
)

type TcpServer struct {
	m_ip    string
	m_port  string
	m_net   string
	m_conns map[uint32]*TcpConn
	m_rw    sync.RWMutex
	m_next  uint32
	m_cb    Conncb
}

func NewTcpServer(cb Conncb) *TcpServer {
	ser := new(TcpServer)
	ser.m_cb = cb
	ser.m_next = 0
	ser.m_conns = make(map[uint32]*TcpConn)
	return ser
}

func (this *TcpServer) Init(ip string, port string) {
	this.m_ip = ip
	this.m_port = port
	this.m_net = "tcp"
	//go this.StartListen()
	this.StartListen()
}

func (this *TcpServer) StartListen() {
	addr, err2 := net.ResolveTCPAddr(this.m_net, this.m_ip+string(":")+this.m_port)
	if nil != err2 {
		log.Panic(err2)
		return
	}
	listener, err1 := net.ListenTCP(this.m_net, addr)
	if nil != err1 {
		log.Panic(err1)
		return
	}
	for {
		conn, err := listener.AcceptTCP()
		if err != nil {
			log.Panic("Accept failed ! ip =  " + this.m_ip + " port = " + this.m_port + " err = " + err.Error())
		}
		this.RegisterConn(conn)
	}
}
func (this *TcpServer) RegisterConn(c *net.TCPConn) {
	this.m_rw.Lock()
	this.m_next++
	id := this.m_next
	this.m_rw.Unlock()
	conn, err := NewTcpConn(c, this.m_next, this.m_cb)
	if nil != err {
		log.Panic(err)
	}
	this.m_rw.Lock()
	this.m_conns[id] = conn
	this.m_rw.Unlock()
	go this.HandleConnection(conn)
}

func (this *TcpServer) HandleConnection(conn *TcpConn) {
	var err2 error
	for nil == err2 {
		err2 = conn.Read()
	}
}

func (this *TcpServer) GetConn(id uint32) (*TcpConn, error) {
	this.m_rw.RLock()
	conn, ok := this.m_conns[id]
	if !ok {
		return nil, errors.New("invalid id")
	}
	this.m_rw.RUnlock()
	return conn, nil
}
