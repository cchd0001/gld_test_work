package tcptool

type Conncb interface {
	Connect(conn *TcpConn, e error)
	Disconnect(conn *TcpConn, e error)
	Read(conn *TcpConn, buff []byte) int32
	Send(conn *TcpConn, len int32)
}
