package main

import (
	"fmt"
)

var (
	cc chan string
	_  = fmt.Printf
)

func Recive() {
	for {
		str, ok := <-cc
		if !ok {
			fmt.Println("Recive End")
			return
		} else {
			fmt.Println(str)
		}
	}
}

func main() {
	cc = make(chan string, 10)
	cc <- "Hello 0"
	go Recive()
	cc <- "Hello 1"
	cc <- "Hello 2"
	cc <- "Hello 3"
	cc <- "Hello 4"
	cc <- "Hello 5"
	cc <- "Hello 6"
	close(cc)
	select {}
}
