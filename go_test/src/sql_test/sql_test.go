package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"sync/atomic"
	"testing"
	"time"
)

var _ = fmt.Print

//Truncate table before benchmark

var j uint32 = 0

func BenchmarkSqlInsert(b *testing.B) {
	db, err := sql.Open("mysql", "root:123456@/test")
	if err != nil {
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	defer db.Close()
	// Prepare statement for inserting data
	stmtIns, err := db.Prepare("INSERT INTO squareNumber VALUES( ?, ? )") // ? = placeholder
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	defer stmtIns.Close() // Close the statement when we leave main() / the program terminates
	// Insert square numbers for 0-24 in the database
	for i := 0; i < b.N; i++ {
		_, err = stmtIns.Exec(j, (j * j)) // Insert tuples (i, i^2)
		j++
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}
	}
}

func BenchmarkSqlInsertParrallel(b *testing.B) {
	b.SetParallelism(8)
	b.RunParallel(func(pb *testing.PB) {
		db, err := sql.Open("mysql", "root:123456@/test")
		if err != nil {
			panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
		}
		defer db.Close()
		// Prepare statement for inserting data
		stmtIns, err := db.Prepare("INSERT INTO squareNumber VALUES( ?, ? )") // ? = placeholder
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}
		defer stmtIns.Close() // Close the statement when we leave main() / the program terminates
		// Insert square numbers for 0-24 in the database
		for pb.Next() {
			jj := atomic.AddUint32(&j, 1)
			_, err = stmtIns.Exec(jj, jj) // Insert tuples (i, i^2)
			if err != nil {
				panic(err.Error()) // proper error handling instead of panic in your app
			}
		}
	})
}

func BenchmarkSqlQuery(b *testing.B) {
	db, err := sql.Open("mysql", "root:123456@/test")
	if err != nil {
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	defer db.Close()

	// Prepare statement for reading data
	stmtOut, err := db.Prepare("SELECT squareNum FROM squareNumber WHERE number = ?")
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	defer stmtOut.Close()

	var squareNum int // we "scan" the result in here

	// Query the square-number of 13
	for i := 0; i < b.N; i++ {
		err = stmtOut.QueryRow(0).Scan(&squareNum) // WHERE number = 13
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}
	}
}

func BenchmarkTest(b *testing.B) {
	for i := 0; i < b.N; i++ {
		time.Sleep(time.Second)
	}
}
