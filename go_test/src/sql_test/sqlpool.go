package sql_tool

import (
	//"common_tool"
	"database/sql"
	"error_tool"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
)

var _ = fmt.Println
var _ = log.Panic

type SqlConf struct {
	Ip       string
	Port     string
	User     string
	Passwd   string
	DbName   string
	PoolSize int
}

func (this *SqlConf) Valid() bool {
	if nil == this || "" == this.Ip || "" == this.Port || "" == this.User || "" == this.Passwd || "" == this.DbName || 1 > this.PoolSize {
		return false
	}
	return true
}

func (this *SqlConf) DataSourceName() string {
	return this.User + string(":") + this.Passwd + string("@") + string("tcp(") + this.Ip + string(":") + this.Port + string(")") + string("/") + this.DbName
}

type SqlConnPool struct {
	m_conf *SqlConf
	m_pool []*sql.DB
	//m_pool *common_tool.FIFOPool
}

func NewSqlConnPool(conf *SqlConf) *SqlConnPool {
	if nil == conf || !conf.Valid() {
		return nil
	}
	ret := &SqlConnPool{
		m_conf: conf,
		//m_pool: common_tool.NewFIFOPool(),
		m_pool: make([]*sql.DB, 0),
	}
	return ret
}

func (this *SqlConnPool) Init() error {
	if this == nil {
		return error_tool.ErrThisNil
	}
	if this.m_conf == nil {
		return errors.New("SqlConnPool : this.m_conf == nil ")
	} else if !this.m_conf.Valid() {
		return errors.New("SqlConnPool : this.m_conf is not valid ")
	}
	for i := 0; i < this.m_conf.PoolSize; i++ {
		db, err := sql.Open("mysql", this.m_conf.DataSourceName())
		if err != nil {
			//	for this.m_pool.Len() > 0 {
			//		a_i := this.m_pool.Get()
			//		if nil == a_i {
			//			continue
			//		}
			//		a_b := a_i.(*sql.DB)
			//		a_b.Close()
			//	}
			//	return err
		}
		//this.m_pool.Put(db)
		this.m_pool = append(this.m_pool, db)
	}
	return nil
}

var next int = 0

func (this *SqlConnPool) getConn() *sql.DB {
	if this == nil {
		log.Panic(error_tool.ErrThisNil)
		return nil
	}
	next++
	return this.m_pool[next%len(this.m_pool)]
	//	a_i := this.m_pool.Get()
	//	if nil == a_i {
	//		return nil
	//	}
	//	a_db := a_i.(*sql.DB)
	//	return a_db
}

func (this *SqlConnPool) putConn(a_db *sql.DB) {
	//	if this == nil {
	//		log.Panic(error_tool.ErrThisNil)
	//	}
	//	if a_db == nil {
	//		return
	//	}
	//	this.m_pool.Put(a_db)
}

func (this *SqlConnPool) Exec(query string, args ...interface{}) (sql.Result, error) {
	if this == nil {
		log.Panic(error_tool.ErrThisNil)
	}
	conn := this.getConn()
	defer this.putConn(conn)
	if nil == conn {
		return nil, errors.New("No valid connected to DB ")
	}
	return conn.Exec(query, args...)
}
