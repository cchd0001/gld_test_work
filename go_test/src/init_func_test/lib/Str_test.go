package str

import (
	"fmt"
	"testing"
)

func init() {
	fmt.Println("---- init func from lib/Str_tets.go")
}

func TestStr(t *testing.T) {
	if Str() != "Hello World" {
		t.Error("error ")
	}
}
