package str

import "fmt"

func init() {
	fmt.Println("---- init func from lib/Str.go")
}

func Str() string {
	return "Hello World"
}
