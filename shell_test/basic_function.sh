


#########################################
# 
# Usage : Count Program_name 
#
# Count progresses that started by $1
# 
# Must not started by 
#                     grep ,
#                     vi ,
#                     bash ,
#                     gdb .
#
#########################################

function Count() {
    if [[ $# -eq 1 ]] ; then 
        local num=`ps -ef | grep $1 | egrep -v 'grep|vi|bash|gdb' | wc -l` 
        return $num
    else 
        echo "Count with param [ $* ] Error "
        return 0
    fi
}

#########################################
# 
# Usage : Kill Program_name 
# 
# Kill a progress that started by $1
# 
# Must not started by 
#                     grep ,
#                     vi ,
#                     bash ,
#                     gdb .
#
#########################################

function Kill() {
    if [[ $# -eq 1 ]] ; then 
        ps -ef | grep $1 | egrep -v 'grep|vi|bash|gdb' | awk '{print $2}' | xargs -i kill -9 {}
    else 
        echo "Kill with param [ $* ] Error "
    fi
}

#########################################
# 
# Usage : Restart Program_fullpath_name 
# 
# Restart $1 if no progress started by it
# 
# Must not started by 
#                     grep ,
#                     vi ,
#                     bash ,
#                     gdb .
#
#########################################

function Restart() {
    if [[ $# -eq 1 ]] ; then 
        local filename=`basename $1`
        local foldername=`dirname $1`
        if [[ ! -e $1 ]] ; then 
            return -1
        fi
        Count $filename
        local num=$?
        if [[ $num -lt 1 ]] ; then 
            echo "Start [ $1 ] by Restart function"
            cd $foldername 
                nohup ./$filename>/dev/null 2>&1 &
            cd - ;
        fi
    else 
        echo "Restart with param [ $* ] Error "
    fi
}
