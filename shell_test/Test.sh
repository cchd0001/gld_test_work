#!/usr/bin/env bash

SCRIPT_ROOT=`pwd`

cd $SCRIPT_ROOT

source basic_function.sh

TestAPP=Center
Count $TestAPP
CenterNum=$?
echo "There are $CenterNum of $TestAPP"

if [[ $CenterNum -gt 0 ]] ; then 
    echo "Kill $TestAPP"
   # Kill  $TestAPP
fi

Restart /home/lguo/PocketMonster/PocketMonsterServer/CenterServer/Center
