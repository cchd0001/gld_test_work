#include <iostream>
#include <sstream>
#include <fstream>
#include <memory>
#include "boost/archive/xml_iarchive.hpp"
#include "boost/archive/xml_oarchive.hpp"
#include "boost/serialization/vector.hpp"
#include "boost/serialization/list.hpp"
#include "boost/serialization/map.hpp"
#include "boost/serialization/array.hpp"
#include "boost/serialization/string.hpp"
#include "boost/serialization/shared_ptr.hpp"
#include <boost/serialization/export.hpp>
#include <vector>
#include <list>
#include <map>
#include <queue>
#include <array>

struct Father
{
    int i ; // 序列化对象的基本类型.
    virtual ~Father(){}
    private:
    friend boost::serialization::access;
    template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
            ar & BOOST_SERIALIZATION_NVP(i) ;
        }
};

struct Son : public Father
{
    int j ;
    virtual ~Son(){}
    private:
    friend boost::serialization::access;

    template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
            // 支持继承.
            ar &  boost::serialization::make_nvp("Father" , boost::serialization::base_object<Father>(*this));
            //ar & BOOST_SERIALIZATION_NVP(boost::serialization::base_object<Father>(*this));
            ar & BOOST_SERIALIZATION_NVP(j) ;
        }
};

BOOST_CLASS_EXPORT_GUID(Father, "Father")
BOOST_CLASS_EXPORT_GUID(Son, "Son")

struct Family
{
    Father f ;
    Son s ;
    private:
    friend boost::serialization::access;

    template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
            // 支持组合.
            ar & BOOST_SERIALIZATION_NVP(f);
            ar & BOOST_SERIALIZATION_NVP(s);
        }
};


static int init  = 0 ;
struct FromOtherLib
{
    FromOtherLib() : i(init++) , j (init++) {}
    int i ;
    int j ;
};


    template<class Archive>
void serialize(Archive & ar, FromOtherLib & lib , const unsigned int version)
{
    // 非侵入式.
    ar & boost::serialization::make_nvp( "j" ,lib.j);
    ar & boost::serialization::make_nvp( "i" ,lib.i);
}


// 处理基本类型.
    template<class Archive>
void serialize(Archive & ar, int & lib , const unsigned int version)
{
    ar & lib ;
}

struct TestSTL
{ // 测试各种容器.
    std::array<FromOtherLib , 5> a;
    std::vector<FromOtherLib>  vector ;
    std::list<FromOtherLib>  list;
    std::map<int ,FromOtherLib>  map;
    //std::queue<FromOtherLib>  queue;

    private:
    friend boost::serialization::access;

    template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
            ar & BOOST_SERIALIZATION_NVP(a);
            ar & BOOST_SERIALIZATION_NVP(vector);
            ar & BOOST_SERIALIZATION_NVP(list);
            ar & BOOST_SERIALIZATION_NVP(map);
           // ar & BOOST_SERIALIZATION_NVP(queue);
        }
};
// 支持指定版本号来实现向后兼容.
BOOST_CLASS_VERSION(TestSTL, 2)

struct TestPtr
{
    TestPtr() : str(nullptr) { 
        for(int i = 0 ; i< 5 ; i++)
        {
            if( i> 2)
            test_po[i] = new Son;
            else 
            test_po[i] = new Father;
        }
    }
    std::shared_ptr<Father>  str;
    //std::shared_ptr<Father> test_po[5];
    Father* test_po[5];
    //std::unique_ptr<Father> f ;
    int *i ;
    private:
    friend boost::serialization::access;
    template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
            ar & BOOST_SERIALIZATION_NVP(str);
            ar & BOOST_SERIALIZATION_NVP(test_po);
            //ar & BOOST_SERIALIZATION_NVP(i);
        }
};

int main() {
    // create and open a character archive for output
    std::ofstream ofs("filename");

    // create class instance
    Family f ;
    f.f.i = 100 ;
    f.s.i = 10 ;
    f.s.j = 20 ;
    FromOtherLib o ;
    o.i = 1 ;
    o.j = 2 ;
    TestSTL stl;
    stl.vector.push_back(FromOtherLib());
    stl.list.push_back(FromOtherLib());
    stl.map.insert(std::make_pair(1,FromOtherLib()));
    //stl.queue.push(FromOtherLib());


    TestPtr p;
   // p.f.reset(new Father());
   // p.f->i = 101 ;
    // save data to archive
    {
        boost::archive::xml_oarchive oa(ofs);
        // write class instance to archive
        oa << BOOST_SERIALIZATION_NVP(f);
        oa << BOOST_SERIALIZATION_NVP(o);
        oa << BOOST_SERIALIZATION_NVP(stl);
        p.str = std::make_shared<Father>() ;
        p.str->i = 101 ;
        p.i = new int(100);
        oa << BOOST_SERIALIZATION_NVP(p); // 保存带有shared_ptr 的结构体， 指针指向的对象会被包存。
        p.str->i = 102 ; 
        *p.i = 103 ;
        oa << BOOST_SERIALIZATION_NVP(p); // 同一个share_ptr 指向的对象不会被保存两次，意味着上一行的修改无效.
        int a = 100 ;
        int *b = & a;
        oa << BOOST_SERIALIZATION_NVP(a);
        //oa << BOOST_SERIALIZATION_NVP(b);
    	// archive and stream closed when destructors are called
    }

    // ... some time later restore the class instance to its orginal state
    Family newg;
    FromOtherLib newo ;
    TestSTL newstl;
    {
        // create and open an archive for input
        std::ifstream ifs("filename");
        boost::archive::xml_iarchive ia(ifs);
        // read class state from archive
        ia >> BOOST_SERIALIZATION_NVP(newg);
        ia >> BOOST_SERIALIZATION_NVP(newo);
        ia >> BOOST_SERIALIZATION_NVP(newstl);
        // archive and stream closed when destructors are called
        std::cout<< newo.i<<std::endl;
        std::cout<< newg.f.i<<std::endl;
    }
    //std::ostringstream ost1;
    //ost1<< newg <<std::endl;
    
    return 0;
}

