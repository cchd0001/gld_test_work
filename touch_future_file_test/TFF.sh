#!/bin/bash


function TouchAFile()
{
    if [[ -f $1 ]] ; then
        local last=`stat --format=%Y $1`
        local now=`date +%s`
        if [[ $last -gt $now ]] ; then
            echo "TouchAFile from future  $1 --- !!! "
            touch $1
        fi
    fi
}

function TouchAItem()
{
    if [[ ! -e $1 ]] ; then
        echo "param \[ $1 \]  is not exsist !"
    fi
    echo "TouchAItem $1 --- !!! "
    if [[ -f $1 ]] ; then 
        TouchAFile $1
    fi
    if [[ -d $1 ]] ; then
        local items=(`ls $1`)
        for item in ${items[*]}
        do
            TouchAItem "$1/$item"
        done
    fi
}


TouchAItem ./
