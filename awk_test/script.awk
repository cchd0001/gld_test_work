#################################################################
## 1. Test variable default init 
#END { printf("string = %s, number = %d\n", aaa, aaa) ;}
#################################################################


##################################################################
## 2. Test use function parameter list to implement local variable
#function  factorial( n ,_ARG_END_ , i , s ) {
##function  factorial( n ) { # If use this line , i will be a global variable
#                  # and error happened in below usage
#    s = 1;
#    for(i =1 ; i<= n ; i ++ ) {
#        s *= i;
#    }
#    return s;
#}
#
#
#END{
#    for(i = 0 ; i < 10 ; i ++ ){
#        printf("factorial( %d ) is %d \n" , i  , factorial(i));
#    }
#}

###########################################################################
# Test array Init
#END {
#    array[1,1,2,4] = 1; array[2] = 3 ;
#    for( a  in array ) {
#        printf(" The array[%s] = %d \n", a ,array[a]); 
#        o = split(a,x,SUBSER);
#        printf("Multikeys %d :", o );
#        for(key in x){
#            printf(" %s ",x[key]);
#        }
#        printf("\n");
#    }
#printf("length of array is %d \n",length(array)) ;
#}

##############################################################################
# Test int function
#END{
#    a = "aaa";
#    printf("a is %d , % s \n",int(a),a);
#}


##############################################################################
{
     print $3 | "sort" ;
     system("echo Yes");
}
