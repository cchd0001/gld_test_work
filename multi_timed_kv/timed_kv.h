

template<class V , class K1 , class K ...>
class ITimedKV {

public:
    // 返回数据有效期限 。 单位是秒。
    virtual int Seconds() = 0 ; 

    // 返回数据到期后的重置值
    virtual V ResetValue( K ... keys ) = 0;

    virtual void SetValue( V value , K ... keys ) = 0 ;

    virtual V GetValue( K ... keys ) = 0 ;

    virtual void IncrValue( V gap , K ... keys ) = 0 ;
    
    virtual void DecrValue( V gap , K ... keys ) = 0 ;
}


