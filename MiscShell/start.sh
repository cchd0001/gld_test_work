#!/bin/bash

if [[ $# < 1 ]] ;  then
    echo "Usage : start.sh [program name]"
    exit
fi

echo "nohup $*>/dev/null 2>&1 &"
nohup $*>/dev/null 2>&1 &
