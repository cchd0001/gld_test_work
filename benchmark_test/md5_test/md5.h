#ifndef __MD5_H__
#define __MD5_H__ 

#include <string>
#include <sstream>

typedef unsigned int MD5_word;
typedef unsigned long long MD5_uint64;

struct MD5Content {
	MD5_word A;
	MD5_word B;
	MD5_word C;
	MD5_word D;

};

union MD5Result {
	struct MD5Content content;
	unsigned char buff[16];
};

struct MD5Content MD5(const unsigned char * buff , MD5_uint64 size );

inline std::string CGetMD5( const unsigned char * buff , MD5_uint64 size ) {
	auto rett = MD5(buff,size);
	MD5Result ret ;
	ret.content = rett ;
	std::ostringstream ost;
	for( int i = 0 ; i < 16 ; i ++ ) {
		ost.width(2);
		ost.fill('0');
		ost<<std::hex<<short(ret.buff[i]);
	}
	return ost.str();
}

#endif //__MD5_H__ 
