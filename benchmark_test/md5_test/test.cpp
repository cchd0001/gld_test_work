#include "md5.h"
#include "md5.hpp"
#include <benchmark/benchmark.h>

static void MD5_test(benchmark::State& state) {
    const unsigned char* src = new unsigned char[state.range_x()]; 
    while (state.KeepRunning()){
		CGetMD5(src,state.range_x());	
	}
    delete[] src;
}
static void MD5_test1(benchmark::State& state) {
    unsigned char* src = new unsigned char[state.range_x()]; 
    while (state.KeepRunning()){
		GetMD5(src,state.range_x());	
	}
    delete[] src;
}
/*
static void MD5_test(benchmark::State& state) {
	std::string ssrc((const char *)src, state.range_x());
    int rett = 0 ;
    unsigned int rettt = 0;
    while (state.KeepRunning()){
        std::string ret = ZBase64::Encode(src,state.range_x());
        std::string ret1 = SBase64_Encode(ssrc,state.range_x());
        std::string ret2 = ZBase64::Decode(ret.c_str(),ret.size(),rett);
        std::string ret3 = SBase64_Decode(ret1 ,rettt);
        assert(ret2 == ssrc );
        assert(ret3 == ssrc );
        assert(ret2 == ret3);
        assert(ret == ret1);
    }
    delete[] src;

}*/

BENCHMARK(MD5_test)->Arg(8)->Arg(64)->Arg(512)->Arg(1<<10)->Arg(8<<10);
BENCHMARK(MD5_test1)->Arg(8)->Arg(64)->Arg(512)->Arg(1<<10)->Arg(8<<10);
BENCHMARK_MAIN();

