#include <benchmark/benchmark.h>
#include <string.h>
#include "base64.h"
#include <iostream>
static void Base64_1(benchmark::State& state) {
    const unsigned char* src = new unsigned char[state.range_x()]; 
    int rett;
    while (state.KeepRunning()){
        auto ret = ZBase64::Encode(src,state.range_x());
        ZBase64::Decode(ret.c_str(),ret.size(),rett);
    }
    delete[] src;
}
//BENCHMARK(Base64_1)->Arg(8)->Arg(64)->Arg(512)->Arg(1<<10)->Arg(8<<10);

static void Base64_2(benchmark::State& state) {
    const unsigned char* src = new unsigned char[state.range_x()]; 
    int rett;
    while (state.KeepRunning()){
        unsigned int rett;
        void * ret = CBase64_Encode(src,state.range_x(),&rett);
        unsigned int rett1;
        CBase64_Decode(ret,rett,&rett1);
    }
    delete[] src;
}
//BENCHMARK(Base64_2)->Arg(8)->Arg(64)->Arg(512)->Arg(1<<10)->Arg(8<<10);

static void Base64_3(benchmark::State& state) {
    std::string ssrc(state.range_x(),'a');
    while (state.KeepRunning()){
        unsigned int ret;
        auto str = SBase64_Encode(ssrc,state.range_x());
        SBase64_Decode(str,ret);
        assert(ret == state.range_x());
    }
}
//BENCHMARK(Base64_3)->Arg(8)->Arg(64)->Arg(512)->Arg(1<<10)->Arg(8<<10);

static void Base64_4(benchmark::State& state) {
    const unsigned char* src = new unsigned char[state.range_x()]; 
    std::string ssrc((const char *)src, state.range_x());
    int rett = 0 ;
    unsigned int rettt = 0;
    while (state.KeepRunning()){
        std::string ret = ZBase64::Encode(src,state.range_x());
        std::string ret1 = SBase64_Encode(ssrc,state.range_x());
        std::string ret2 = ZBase64::Decode(ret.c_str(),ret.size(),rett);
        std::string ret3 = SBase64_Decode(ret1 ,rettt);
        assert(ret2 == ssrc );
        assert(ret3 == ssrc );
        assert(ret2 == ret3);
        assert(ret == ret1);
        //assert(rett == rettt ) ;
    }
    delete[] src;

}
BENCHMARK(Base64_4)->Arg(8)->Arg(64)->Arg(512)->Arg(1<<10)->Arg(8<<10);

BENCHMARK_MAIN();
