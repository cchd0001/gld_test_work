#define BENCHMARK(n) \
    BENCHMARK_PRIVATE_DECLARE(n) =                               \
        (::benchmark::internal::RegisterBenchmarkInternal(       \
            new ::benchmark::internal::FunctionBenchmark(#n, n)))

/////////////////////////////////////////////////////////////////////
#define BENCHMARK_PRIVATE_DECLARE(n)       \
  static ::benchmark::internal::Benchmark* \
  BENCHMARK_PRIVATE_NAME(n) BENCHMARK_UNUSED

/////////////////////////////////////////////////////////////////////

  static ::benchmark::internal::Benchmark*  BENCHMARK_PRIVATE_NAME(n) BENCHMARK_UNUSED = 
        (::benchmark::internal::RegisterBenchmarkInternal(new ::benchmark::internal::FunctionBenchmark(#n, n)))

#define BENCHMARK_PRIVATE_NAME(n) \
    BENCHMARK_PRIVATE_CONCAT(_benchmark_, BENCHMARK_PRIVATE_UNIQUE_ID, n)
#define BENCHMARK_PRIVATE_CONCAT(a, b, c) BENCHMARK_PRIVATE_CONCAT2(a, b, c)
#define BENCHMARK_PRIVATE_CONCAT2(a, b, c) a##b##c
#if defined(__COUNTER__) && (__COUNTER__ + 1 == __COUNTER__ + 0)
#define BENCHMARK_PRIVATE_UNIQUE_ID __COUNTER__
#else
#define BENCHMARK_PRIVATE_UNIQUE_ID __LINE__
#endif    
/////////////////////////////////////////////////////////////////////////////////

static ::benchmark::internal::Benchmark*  _benchmark_##__LINE__##n BENCHMARK_UNUSED = 
        (::benchmark::internal::RegisterBenchmarkInternal(new ::benchmark::internal::FunctionBenchmark(#n, n)))



