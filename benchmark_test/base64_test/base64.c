#include <assert.h>
#include <stdlib.h>

static const char encode_map[64] = {
    'A','B','C','D','E','F','G',
    'H','I','J','K','L','M','N',
    'O','P','Q','R','S','T','U',
    'V','W','X','Y','Z',
    'a','b','c','d','e','f','g',
    'h','i','j','k','l','m','n',
    'o','p','q','r','s','t','u',
    'v','w','x','y','z',
    '0','1','2','3','4','5','6','7','8','9',
    '+','/'};

static const char decode_map[] = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        62, // '+'
        0, 0, 0,
        63, // '/'
        52, 53, 54, 55, 56, 57, 58, 59, 60, 61, // '0'-'9'
        0, 0, 0, 0, 0, 0, 0,
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
        13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, // 'A'-'Z'
        0, 0, 0, 0, 0, 0,
        26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
        39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, // 'a'-'z'
    };

void * CBase64_Encode(const void  * buf,unsigned int len, unsigned int * ret_len)
{
    *ret_len= ( len / 3 ) *4 + ((len % 3 !=0) ? 4 : 0);
    char * ret_buf = (char*)malloc(*ret_len);
    assert(ret_buf);
    char * curr_ret = ret_buf;
    char * curr = (char*)buf;
    unsigned int len_left = len;
    while(len_left >2){
        *curr_ret++ = encode_map[(*curr & 0xfc) >>2];  // H 6
        *curr_ret++ = encode_map[((*curr & 0x3)<<4) + ((*(curr+1) & 0xf0) >> 4)]; // L2 H 4
        *curr_ret++ = encode_map[(((*(curr+1))&0xf)<<2) + (((*(curr+2) & 0xc0))>>6) ];  // L4 H2
        *curr_ret++ = encode_map[(*(curr+2)) & 0x3f];// L6
        len_left -= 3;
        curr += 3;
    }
    if(len_left ==1 ){
        *curr_ret++ = encode_map[(*curr & 0xfc) >>2];  // H 6
        *curr_ret++ = encode_map[((*curr & 0x3)<<4)]; // L2 + 0
        *curr_ret++ = '=';
        *curr_ret++ = '=';
        len_left -= 1;
    }else if(len_left == 2){
        *curr_ret++ = encode_map[(*curr & 0xfc) >>2];  // H 6
        *curr_ret++ = encode_map[((*curr & 0x3)<<4) + ((*(curr+1) & 0xf0) >> 4)]; // L2 H 4
        *curr_ret++ = encode_map[(*(curr+1)&0xf)<<2];  // L4 H2
        *curr_ret++ = '=';
        len_left -= 2;
    }
    assert(len_left == 0);
    return (void*)ret_buf;       
}
void * CBase64_Decode(const void * buf,unsigned int len, unsigned int * ret_len){
    unsigned int base_len = len / 4 * 3 ;
    unsigned int len_left = len;
    char * ret_bufs = (char *)calloc(sizeof(char),base_len);
    char * ret_buf = ret_bufs;
    char * b64_buf = (char *)buf;
    while(len_left > 3){
        *ret_buf += (decode_map[*b64_buf] & 0x3f )<<2;  //  6-0  -> H6
        *ret_buf += (decode_map[*(b64_buf+1)] & 0x30 )>>4; // L 6-4   -> L2 
        if(*(b64_buf+2) == '=' ){
            base_len -= 2;
            *(ret_buf+1) = 0;
            break;
        } else {
            *(ret_buf+1) += (decode_map[*(b64_buf+1)] & 0x0f )<<4; // 4-0 -> H4
            *(ret_buf+1) += (decode_map[*(b64_buf+2)] & 0x3c )>>2; // 6-2 -> L4
            if(*(b64_buf +3) != '='){
                *(ret_buf+2) += (decode_map[*(b64_buf+2)] & 0x03 )<<6; //2-0 -> H2
                *(ret_buf+2) += (decode_map[*(b64_buf+3)] & 0x3f );    //6-0 -> L6 
            } else {
                base_len -= 1;
                *(ret_buf+2) = 0;
                len_left = 0;
                break;
            }
        }
        len_left -= 4;
        ret_buf += 3;
        b64_buf += 4;
    }
    *ret_len = base_len;
    return ret_bufs;
}
