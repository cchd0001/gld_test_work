﻿#ifndef __ZBASE64_H__
#define __ZBASE64_H__

#include <string>
class ZBase64
{
public:
	static std::string Encode(const unsigned char* Data,int DataByte);
	static std::string Decode(const char* Data,int DataByte,int& OutByte);
};

void * CBase64_Decode(const void * buf,unsigned int len, unsigned int * ret_len);
void * CBase64_Encode(const void  * buf,unsigned int len, unsigned int * ret_len);

std::string inline SBase64_Decode(const std::string & str ,unsigned int & ret_size){
    void * pret = CBase64_Decode(static_cast<const void *>(str.c_str()) , str.size() , &ret_size);
    std::string ret(static_cast<char *>(pret) , ret_size);
    free(pret);
    return ret;
}

std::string inline SBase64_Encode(const std::string & str ,unsigned int size){
    unsigned int ret_size;
    void * pret = CBase64_Encode(static_cast<const void *>(str.c_str()) , size, &ret_size);
    std::string ret(static_cast<char *>(pret) , ret_size);
    free(pret);
    return ret;
}

#endif
