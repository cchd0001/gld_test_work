#include "Storage.h"
#include <benchmark/benchmark.h>
#include <sstream>
#include <atomic>
#include <chrono>
#include <thread>

class BenchmarkClient : public Storage {
public:
    BenchmarkClient() : Storage ( "" , "127.0.0.1" , "3306" 
    ,"root","123456","test",8) {}

    void Insert(int i) {
        std::ostringstream ost;
        ost<<"INSERT INTO squareNumber VALUES( "<<i<<" , "<<i<<" ) ;";
        ExecSql(ost.str());
    }
} the_client;

std::atomic<int> i{0} ;

struct Init {
    Init() {
       the_client.Init(); 
    }
};

static Init init;

static void Sql(benchmark::State& state) {
    while (state.KeepRunning()){
        int j = i++;
        the_client.Insert(j);
    }
}

BENCHMARK(Sql);

static void SqlThreads(benchmark::State& state) {
    while (state.KeepRunning()){
        int j = i++;
        the_client.Insert(j);
    }
}

BENCHMARK(SqlThreads)->Threads(4);

static void Test(benchmark::State& state) {
    while (state.KeepRunning()){
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}

BENCHMARK(Test);
BENCHMARK(Test)->Threads(5);
BENCHMARK_MAIN();
