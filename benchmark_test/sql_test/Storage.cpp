﻿#include "Storage.h"
#include "ThreadPool.h"

#include "Log.h"

//是否使用连接池，连接池需要多测试.
//当前项目主要是用redis，不太需要.
#define USE_POOL 1

Storage::Storage(const std::string& game_name
        , const std::string& ip
        , const std::string& port
        , const std::string& user
        , const std::string& pass
        , const std::string& db_name
        , int thread_count)
: game_name_(game_name)
, url_("tcp://" + ip + ':' + port)
, user_(user)
, pass_(pass)
, name_(db_name)
, thread_count_(thread_count)
, thread_pool_(nullptr)
, conn_(nullptr)
, driver_(sql::mysql::get_driver_instance())
{
    if (thread_count_ > 8) thread_count_ = 8;
    if (thread_count_ < 1) thread_count_ = 1;
#if !USE_POOL
	//不使用连接池，一个连接，只用一个线程排队处理吧.
	thread_count_ = 1;
#endif
	thread_pool_ = new GameBase::ThreadPool(thread_count_);
}

Storage::~Storage()
{
	Stop();
}

void Storage::ExecSql(std::string sql)
{
	PROFILE(__FUNCTION__);
#if USE_POOL
	auto conn = GetConnection();
	try
	{
		PROFILE("execute(sql)");
		SqlStmtPtr stmt(conn->createStatement());
		stmt->execute(sql);
	}
	catch (const sql::SQLException& e)
	{
		LOGW("SQLException: code=" << e.getErrorCode() << ", msg=" << e.what() << ", sql=" << sql);
	}
	PutConnection(conn);
#else
	if (!conn_) return;
	try
	{
		std::lock_guard<std::mutex> l(conn_mutex_);
		SqlStmtPtr stmt(conn_->createStatement());
		stmt->execute(sql);
	}
	catch (const sql::SQLException& e)
	{
		LOGW("SQLException: code=" << e.getErrorCode() << ", msg=" << e.what() << ", sql=" << sql);
	}
#endif
}

bool Storage::Init()
{
	if (!driver_) {
		LOGE("get_driver_instance return nullptr!");
		return false;
	}

#if USE_POOL
	DestroyConnectionPool();
	CreateConnectionPool();
#else
	DestroyConnection();
	if (!CreateConnection()) return false;
#endif

    return true;
}

void Storage::Stop()
{
	if (thread_pool_)
	{
		thread_pool_->join();
		delete thread_pool_;
	}
#if USE_POOL
	DestroyConnectionPool();
#else
	DestroyConnection();
#endif
	driver_ = nullptr;
}

void Storage::PushSql(const std::string& sql)
{
    LOGT("PushSql: " << sql);
	thread_pool_->enqueue(&Storage::ExecSql, this, sql);
}

sql::ResultSet * Storage::SqlNow(const std::string& sql)
{
#if USE_POOL
	auto conn = GetConnection();
	if (!conn) return nullptr;
	sql::ResultSet* result = nullptr;
	try
	{
		SqlStmtPtr stmt(conn->createStatement());
		result = stmt->executeQuery(sql);
	}
	catch (const sql::SQLException& e)
	{
		LOGW("SQLException: code=" << e.getErrorCode() << ", msg=" << e.what() << ", sql=" << sql);
	}
	PutConnection(conn);
	return result;
#else
	if (!conn_) return nullptr;
	try
	{
		std::lock_guard<std::mutex> l(conn_mutex_);
		SqlStmtPtr stmt(conn_->createStatement());
		return stmt->executeQuery(sql);
	}
	catch (const sql::SQLException& e)
	{
		LOGW("SQLException: code=" << e.getErrorCode() << ", msg=" << e.what() << ", sql=" << sql);
	}
	return nullptr;
#endif
}

sql::Connection* Storage::GetConnection()
{
	PROFILE(__FUNCTION__);
	std::unique_lock<std::mutex> lock(conn_pool_mutex_);
	while (conn_pool_.empty())
	{
		conn_pool_condition_.wait(lock);
	}
	auto conn = conn_pool_.front();
	conn_pool_.pop_front();
	return conn;
}

void Storage::PutConnection(sql::Connection* conn)
{
	PROFILE(__FUNCTION__);
	std::unique_lock<std::mutex> lock(conn_pool_mutex_);
	conn_pool_.push_back(conn);
	conn_pool_condition_.notify_one();
}

sql::Connection* Storage::CreateConnection_nolock()
{
	sql::ConnectOptionsMap conn_prop;
	conn_prop["hostName"] = url_;
	conn_prop["userName"] = user_;
	conn_prop["password"] = pass_;
	conn_prop["OPT_CONNECT_TIMEOUT"] = 10;
	conn_prop["OPT_RECONNECT"] = true;
	auto conn = driver_->connect(conn_prop);
	if (conn)
	{
		conn->setSchema(name_);
	}
	return conn;
}

void Storage::DestroyConnection_nolock(sql::Connection* conn)
{
	if (conn)
	{
		conn->close();
		delete conn;
	}
}

void Storage::CreateConnectionPool()
{
	std::lock_guard<std::mutex> lock(conn_pool_mutex_);
	while (conn_pool_.size() < thread_count_ + 2)
	{
		auto conn = CreateConnection_nolock();
		if (conn)
		{
			conn_pool_.push_back(conn);
			LOGI("connect to db " << url_ << "[" << name_ << "], pool size=" << conn_pool_.size());
		}
		else
		{
			LOGE("connect to db " << url_ << "[" << name_ << "] failed!");
		}
	}
}
void Storage::DestroyConnectionPool()
{
	std::lock_guard<std::mutex> lock(conn_pool_mutex_);
	for (auto conn : conn_pool_)
	{
		DestroyConnection_nolock(conn);
	}
	conn_pool_.clear();
}

bool Storage::CreateConnection()
{
	std::lock_guard<std::mutex> l(conn_mutex_);
	conn_ = CreateConnection_nolock();
	if (!conn_)
	{
		LOGE("connect to db " << url_ << "[" << name_ << "] failed!");
		return false;
	}
	return true;
}
void Storage::DestroyConnection()
{
	std::lock_guard<std::mutex> l(conn_mutex_);
	DestroyConnection_nolock(conn_);
	conn_ = nullptr;
}
