#ifndef _STORAGE_H
#define _STORAGE_H

#include <mutex>
#include <condition_variable>
#include <string>
#include <list>
#include <memory>
#include <mysql_driver.h>
#include <mysql_connection.h>
#include <cppconn/statement.h>
#include <cppconn/exception.h>
#include "Log.h"

namespace GameBase
{
	class ThreadPool;
}


class Storage {
public:
	typedef std::unique_ptr<sql::Statement> SqlStmtPtr;

    Storage(const std::string& game_name
          , const std::string& ip
          , const std::string& port
          , const std::string& user
          , const std::string& pass
          , const std::string& db_name
          , int thread_count);
	virtual ~Storage();

public:
    bool							Init();
	void							Stop();//为处理正常退出，等线程池处理完，销毁线程池、sql连接池.
    void							PushSql(const std::string& sql);
    sql::ResultSet *				SqlNow(const std::string& sql);
protected:
	void							ExecSql(std::string sql);
	sql::Connection*				CreateConnection_nolock();
	void							DestroyConnection_nolock(sql::Connection*);
	sql::Connection*				GetConnection();
	void							PutConnection(sql::Connection*);
	void							CreateConnectionPool();
	void							DestroyConnectionPool();
	bool							CreateConnection();
	void							DestroyConnection();
protected:
	std::string						game_name_;
	std::string						url_, user_, pass_, name_;
    int32							thread_count_;
	GameBase::ThreadPool*			thread_pool_;
	std::mutex						conn_pool_mutex_;
	std::condition_variable			conn_pool_condition_;
	std::list<sql::Connection*>		conn_pool_;
	std::mutex						conn_mutex_;
	sql::Connection*				conn_;
	sql::Driver*					driver_;
}; // Storage

#endif // _STORAGE_H
