﻿//
//  ThreadPool.cpp
//  CocosDemo
//
//  Created by Xiaobin Li on 10/27/14.
//
//

#include "ThreadPool.h"


namespace GameBase {
	// the constructor just launches some amount of workers
	ThreadPool::ThreadPool(size_t threads)
		:   stop(false)
	{
		for(size_t i = 0;i<threads;++i)
			workers.emplace_back(
			[this]
		{
			LOGT("worker begin.");
			for(;;)
			{
				if (this->tasks.empty())
				{
					LOGT("tasks finished");
					this->finish.notify_all();
				}
				LOGT("worker wait task ...");
				std::unique_lock<std::mutex> lock(this->queue_mutex);
				while(!this->stop && this->tasks.empty())
				{
					this->condition.wait(lock);
				}
				LOGT("worker receive a task.");
				if(this->stop && this->tasks.empty())
				{
					return;
				}
				std::function<void()> task(this->tasks.front());
				this->tasks.pop();
				lock.unlock();
				LOGT("worker run task ...");
				task();
				LOGT("worker run task OK.");
			}
			LOGT("worker end.");
		}
		);
	}

	// the destructor joins all threads
	ThreadPool::~ThreadPool()
	{
		LOGT("~ThreadPool()");
		cancel();
	}

	void ThreadPool::cancel()
	{
		// 1.stop
		{
			std::unique_lock<std::mutex> lock(queue_mutex);
			if (stop) return;
			stop = true;
		}
		// 2.detach thread
		condition.notify_all();
		for(size_t i = 0;i<workers.size();++i)
		{
			try {
				workers[i].detach();
			}
			catch (const std::system_error& e)
			{
				LOGE("cancel catch system_error: " << e.what());
			}
		}
	}

	void ThreadPool::join()
	{
		LOGT("join begin.");
		//already stop, wait worker return
		if (stop)
		{
			LOGT("threadpool has stopped, wait all workers return ...");
			condition.notify_all();
			for(size_t i = 0;i<workers.size();++i)
			{
				if(workers[i].joinable()) workers[i].join();
			}
			LOGT("join OK.");
			return;
		}

		// 1.wait all tasks finish
		if (!tasks.empty())
		{
			LOGT("wait all tasks finished ...");
			std::unique_lock <std::mutex> lock(finish_mutex);
			this->finish.wait(lock);
		}

		// 2.stop threadpool
		{
			LOGT("all tasks finished, stop threadpool ...");
			std::unique_lock<std::mutex> lock(queue_mutex);
			stop = true;
		}

		// 3.wait worker return
		LOGT("wait all workers return ...");
		condition.notify_all();
		for(size_t i = 0;i<workers.size();++i)
		{
			if(workers[i].joinable()) workers[i].join();
		}
		LOGT("join OK.");
	}
}
