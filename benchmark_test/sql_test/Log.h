#ifndef __LOG_H__
#define __LOG_H__
#include <iostream>

#define LOGT(...) std::cout<<__VA_ARGS__ <<std::endl
#define LOGD(...) std::cout<<__VA_ARGS__ <<std::endl
#define LOGE(...) std::cout<<__VA_ARGS__ <<std::endl
#define LOGI(...) std::cout<<__VA_ARGS__ <<std::endl
#define LOGW(...) std::cout<<__VA_ARGS__ <<std::endl

#define PROFILE(x) void(x)

typedef unsigned int uint32;
typedef int int32;

#endif //__LOG_H__
