﻿//
//  ThreadPool.h
//  CocosDemo
//
//  Created by Xiaobin Li on 10/27/14.
//
//

#ifndef __CocosDemo__ThreadPool__
#define __CocosDemo__ThreadPool__
#include <vector>
#include <queue>
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <future>
#include <functional>
#include <stdexcept>
#include "Log.h"

namespace GameBase {
    /**
     * @class ThreadPool
     * @brief c++11线程池
     * @note 某些编译器未实现c++11变参模板, 所以enqueue需要手动bind
     *  pool.enqueue(std::bind(&C::f, this, ...));
     */
    class ThreadPool {
    public:
		ThreadPool(size_t);

		// xxx(lxb): vs2012 不支持变参模板 wtf
        template<class F, class... Args>
        auto enqueue(F&& f, Args&&... args) -> std::future<typename std::result_of<F(Args...)>::type>;

//		template<class F>
//		auto enqueue(F&& f) -> std::future<typename std::result_of<F()>::type> {
//			typedef typename std::result_of<F()>::type return_type;
//
//			// don't allow enqueueing after stopping the pool
//			if(stop) throw std::runtime_error("enqueue on stopped ThreadPool");
//
//			auto task = std::make_shared< std::packaged_task<return_type()> >(std::bind(std::forward<F>(f)));
//
//			std::future<return_type> res = task->get_future();
//			{
//				std::unique_lock<std::mutex> lock(queue_mutex);
//				tasks.push([task](){ (*task)(); });
//			}
//			condition.notify_one();
//			return res;
//		}

        ~ThreadPool();
        void cancel();
        void join();
        size_t taskCount() {
            std::unique_lock<std::mutex> lock(queue_mutex);
            return tasks.size();
        }
    private:
        // need to keep track of threads so we can join them
        std::vector< std::thread > workers;
        // the task queue
        std::queue< std::function<void()> > tasks;
        
        // synchronization
        std::mutex queue_mutex;
        std::condition_variable condition;
        std::mutex finish_mutex;
        std::condition_variable finish;
        bool stop;
    };
    
    // add new work item to the pool
    template<class F, class... Args>
    auto ThreadPool::enqueue(F&& f, Args&&... args) -> std::future<typename std::result_of<F(Args...)>::type> {
        typedef typename std::result_of<F(Args...)>::type return_type;
        
        // don't allow enqueueing after stopping the pool
        if(stop) throw std::runtime_error("enqueue on stopped ThreadPool");
        
        auto task = std::make_shared< std::packaged_task<return_type()> >(std::bind(std::forward<F>(f), std::forward<Args>(args)...));
        
        std::future<return_type> res = task->get_future();
        {
            std::unique_lock<std::mutex> lock(queue_mutex);
            tasks.push([task](){ (*task)(); });
        }
        condition.notify_one();
        return res;
    }
}
#endif /* defined(__CocosDemo__ThreadPool__) */
