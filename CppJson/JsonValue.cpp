#include "JsonValue.h"
#include <sstream>
namespace CppJson {

    std::string JsonObject::ToString() const{
        std::ostringstream ost;
        ost<<'{' ;
        auto size = _children.size() ; 
        size_t index = 1 ;
        for(auto it = _children.begin() ; it != _children.end() ; it ++ , index ++ ){
            if(it->second){
                ost<<'"'<<it->first<<"\":"<<it->second->ToString();
                if ( index != size ) {
                    ost<<',';
                }
            }
        }
        ost<<'}';
        return ost.str();
    }
    std::string JsonArray::ToString() const{
        std::ostringstream ost;
        ost<<'[' ;
        auto size = _children.size() ; 
        size_t index = 1 ;
        for(auto it = _children.begin() ; it != _children.end() ; it ++ , index ++ ){
            ost<<it->ToString();
            if ( index != size ) {
                ost<<',';
            }
        }
        ost<<']';
        return ost.str();
    }
}
