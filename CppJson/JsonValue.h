#ifndef __CPPJSON_JSONVALUE_H__
#define __CPPJSON_JSONVALUE_H__

#include <string>
#include <map>
#include <memory>
#include <vector>

namespace CppJson{

    enum ValueType{
        Unknow = 0 ,
        String = 1 ,
        Boolean = 2 ,
        Array = 3 ,
        Object = 4 ,
        Null = 5 ,
        Number = 6 ,
        TypeMax = 7 
    };

    struct JsonValue {

        virtual ValueType GetType() const = 0 ;

        virtual std::string ToString() const = 0 ;
    };

    struct JsonNumber : public JsonValue {

        JsonNumber() :  JsonNumber(0) { }

        JsonNumber(long n)  {  _number = n ; }

        virtual ValueType GetType() const final {return ValueType::Number ;}

        virtual std::string ToString() const final { return std::to_string( _number ) ;} 

        virtual ~JsonNumber() {}

        long _number;

    };

    struct JsonString : public JsonValue {

        JsonString() : JsonString("") {}

        JsonString(const std::string & str) { _string = str ; }

        virtual ValueType GetType() const final {return ValueType::String ; }

        virtual std::string ToString() const { return  "\""+_string+"\"" ;} 

        virtual ~JsonString() {}

        std::string _string;
    };

    struct JsonBoolean : public JsonValue  {

        JsonBoolean() : JsonBoolean(false) {} 

        JsonBoolean( bool f ) { _boolean = f ;} 

        virtual ValueType GetType() const final {return ValueType::Boolean; }

        virtual std::string ToString() const { return  _boolean ? "true" : "false" ;} 

        virtual ~JsonBoolean() {}

        bool _boolean;
    };
    struct JsonNull: public JsonValue  {

        virtual ValueType GetType() const final {return ValueType::Null; }

        virtual std::string ToString() const { return  "null" ;} 

        virtual ~JsonNull() {}
    };

    struct JsonObject : public JsonValue {

        virtual ValueType GetType() const final  { return ValueType::Object ; }

        virtual std::string ToString() const final  ;


        std::map<std::string , std::shared_ptr<JsonValue> > _children;
    };

    struct JsonArray : public JsonValue {

        virtual ValueType GetType() const final  { return ValueType::Array ; }

        virtual std::string ToString() const final  ;

        std::vector<JsonObject> _children;
    };
}

#endif // __CPPJSON_JSONVALUE_H__
