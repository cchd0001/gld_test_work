#ifndef __CPPJSON_JSONOBJECT_H__
#define __CPPJSON_JSONOBJECT_H__

#include <string>
#include "JsonValue.h"
namespace CppJson{


    struct JsonObject : public JsonValue {

        virtual ValueType GetType() const final  { return ValueType::Object ; }

        virtual std::string ToString() const final  ;
    };

}

#endif // __CPPJSON_JSONOBJECT_H__
