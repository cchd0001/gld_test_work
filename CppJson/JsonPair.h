#ifndef __CPPJSON_JSONPAIR_H__
#define __CPPJSON_JSONPAIR_H__

#include <string>
#include "JsonValue.h"

namespace CppJson {

    typedef std::pair<std::string , JsonValue>  JsonPair ;

}
#endif // __CPPJSON_JSONPAIR_H__
