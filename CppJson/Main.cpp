#include "JsonValue.h"
#include <iostream>

int main(){
    CppJson::JsonArray array;
   auto o1 = std::make_shared<CppJson::JsonObject>() ;
   auto o2 = std::make_shared<CppJson::JsonObject>() ;
   auto o3 = std::make_shared<CppJson::JsonObject>();

    o1->_children["head1"] = std::make_shared<CppJson::JsonNumber>(1) ;
    o2->_children["head1"] = std::make_shared<CppJson::JsonNumber>(1) ;
    o3->_children["head1"] = std::make_shared<CppJson::JsonNumber>(1) ;
    o1->_children["head2"] = std::make_shared<CppJson::JsonString>("1.1") ;
    o2->_children["head2"] = std::make_shared<CppJson::JsonString>("1.2") ;
    o3->_children["head2"] = std::make_shared<CppJson::JsonString>("1.3") ;
    o3->_children["o2"] = o2;
    o2->_children["o1"] = o1;


    array._children.push_back(*o1);
    array._children.push_back(*o2);
    array._children.push_back(*o3);
    array._children.push_back(*o3);
    std::cout<<array.ToString()<<std::endl;
    return 0;
}
