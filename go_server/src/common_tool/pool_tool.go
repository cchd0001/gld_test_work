package common_tool

import (
	"error_tool"
	"log"
	"sync"
)

// Support Put and Get items via multi-goroutine.
// Items follow FIFO rule.
type FIFOPool struct {
	m_pool []interface{}
	m_lock sync.Mutex
	m_cond *sync.Cond
}

func NewFIFOPool() *FIFOPool {
	ret := &FIFOPool{
		m_pool: make([]interface{}, 0),
	}
	ret.m_cond = sync.NewCond(&ret.m_lock)
	return ret
}

// If nothing is pool , it will block utill someone put something into it.
// Follow FIFO.
// May return nil if someone Put nil .
func (this *FIFOPool) Get() interface{} {
	if nil == this {
		log.Panic(error_tool.ErrThisNil)
		return nil
	}
	this.m_lock.Lock()
	defer this.m_lock.Unlock()
	var ret interface{}
	for {
		if len(this.m_pool) == 0 {
			this.m_cond.Wait()
			continue
		}
		ret = this.m_pool[0]
		this.m_pool = this.m_pool[1:]
		break
	}
	return ret
}

func (this *FIFOPool) Put(i interface{}) {
	if nil == this {
		log.Panic(error_tool.ErrThisNil)
		return
	}
	this.m_lock.Lock()
	defer this.m_lock.Unlock()
	this.m_pool = append(this.m_pool, i)
	this.m_cond.Signal()
}

func (this *FIFOPool) Len() int {
	if nil == this {
		log.Panic(error_tool.ErrThisNil)
		return 0
	}
	this.m_lock.Lock()
	defer this.m_lock.Unlock()
	return len(this.m_pool)
}
