package common_tool

import (
	"fmt"
	"reflect"
	"sync/atomic"
	"testing"
)

var _ = atomic.AddUint32
var _ = fmt.Println

type Test struct {
}

func TestPoolPutGeT(t *testing.T) {
	pool := NewFIFOPool()
	var test_data []interface{} = []interface{}{
		int(1),
		string("Hello"),
		nil,
		[]byte("World"),
		float32(0.123),
		new(Test),
	}

	for _, i := range test_data {
		pool.Put(i)
	}
	if pool.Len() != len(test_data) {
		t.Error("Put error")
	}
	for _, i := range test_data {
		v1 := pool.Get()
		if !reflect.DeepEqual(i, v1) {
			t.Error("Get error")
		}
	}
}

func put(pool *FIFOPool, test_data []interface{}) {
	for _, i := range test_data {
		go pool.Put(i)
	}
}

var zero_100 uint32 = 0
var fatel uint32 = 0
var cc chan int

func init() {
	fmt.Println("--- Init test file --- ")
	cc = make(chan int)
}

func get(pool *FIFOPool, test_data []interface{}, t *testing.T) {
	for _, i := range test_data {
		v1 := pool.Get()
		if nil == v1 {
			t.Log("Get nil")
			atomic.AddUint32(&fatel, 1)
		}
		if reflect.DeepEqual(i, v1) {
			t.Log("Miss match . accept because parrallel .")
		}
	}
	atomic.AddUint32(&zero_100, 1)
	if zero_100 == 100 {
		cc <- 1
	}
}
func TestPoolPutGeTParrallel(t *testing.T) {
	pool := NewFIFOPool()
	var test_data []interface{} = []interface{}{
		int(1),
		string("Hello"),
		nil,
		[]byte("World"),
		float32(0.123),
		new(Test),
	}
	for j := 0; j < 100; j++ {
		go put(pool, test_data)
	}
	for j := 0; j < 100; j++ {
		go get(pool, test_data, t)
	}
	_ = <-cc
	fmt.Println("get nil for ", fatel)
}
