package error_tool

import "errors"

var ErrThisNil = errors.New("this == nil")
