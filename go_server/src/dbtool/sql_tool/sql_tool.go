package sql_tool

import (
	"database/sql"
	"error_tool"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
)

var _ = fmt.Println
var _ = log.Panic

type SqlConf struct {
	Ip     string
	Port   string
	User   string
	Passwd string
	DbName string
}

func (this *SqlConf) Valid() bool {
	if nil == this || "" == this.Ip || "" == this.Port || "" == this.User || "" == this.Passwd || "" == this.DbName {
		return false
	}
	return true
}

func (this *SqlConf) DataSourceName() string {
	return this.User + string(":") + this.Passwd + string("@") + string("tcp(") + this.Ip + string(":") + this.Port + string(")") + string("/") + this.DbName
}

type SqlTool struct {
	m_conf *SqlConf
	m_db   *sql.DB
}

func NewSqlTool(conf *SqlConf) *SqlTool {
	if nil == conf || !conf.Valid() {
		return nil
	}
	ret := &SqlTool{
		m_conf: conf,
	}
	return ret
}

func (this *SqlTool) Init() error {
	if this == nil {
		return error_tool.ErrThisNil
	}
	if this.m_conf == nil {
		return errors.New("SqlTool : this.m_conf == nil ")
	} else if !this.m_conf.Valid() {
		return errors.New("SqlTool : this.m_conf is not valid ")
	}
	db, err := sql.Open("mysql", this.m_conf.DataSourceName())
	if err != nil {
		db.Close()
		return err
	}
	this.m_db = db

	return nil
}

func (this *SqlTool) PushSql(query string, args ...interface{}) {
	go func() {
		if db := this.GetSqlDB(); db != nil {
			db.Exec(query, args...)
		}
	}()
}

func (this *SqlTool) ExecNow(query string, args ...interface{}) (err error) {
	if db := this.GetSqlDB(); db != nil {
		_, err = db.Exec(query, args...)
	} else {
		return errors.New("SqlTool : m_db is nil")
	}
	return err
}

func (this *SqlTool) GetSqlDB() *sql.DB {
	if this == nil {
		log.Panic(error_tool.ErrThisNil)
	}
	return this.m_db
}
