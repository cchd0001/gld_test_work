package sql_tool

import (
	"fmt"
	"log"
	"sync/atomic"
	"testing"
)

var sqlpool *SqlTool

func init() {
	var conf SqlConf
	conf.Ip = "127.0.0.1"
	conf.Port = "3306"
	conf.User = "root"
	conf.Passwd = "123456"
	conf.DbName = "test"
	sqlpool = NewSqlTool(&conf)
	err1 := sqlpool.Init()
	if nil != err1 {
		log.Panic(err1)
	}
	err := sqlpool.ExecNow("truncate table squareNumber;")
	if nil != err {
		log.Panic(err)
	}
}

func TestSqlExec(t *testing.T) {
	err := sqlpool.ExecNow(string("insert into squareNumber values(1,1);"))
	if nil != err {
		t.Error(err)
	}
}

var j uint32 = 1

// 1 goroutine

func BenchmarkSqlExec(b *testing.B) {
	for i := 0; i < b.N; i++ {
		m := atomic.AddUint32(&j, 1)
		command := fmt.Sprint("insert into squareNumber values(", m, ",", m, ");")
		err := sqlpool.ExecNow(command)
		if nil != err {
			b.Error(err)
		}
	}
}

// for lots of goroutine
func BenchmarkSqlExecParallel(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			m := atomic.AddUint32(&j, 1)
			command := fmt.Sprint("insert into squareNumber values(", m, ",", m, ");")
			err := sqlpool.ExecNow(command)
			if nil != err {
				b.Error(err)
			}
		}

	})
}

// lots of goroutine
func BenchmarkSqlExecParallelThread(b *testing.B) {
	b.SetParallelism(32)
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			m := atomic.AddUint32(&j, 1)
			command := fmt.Sprint("insert into squareNumber values(", m, ",", m, ");")
			err := sqlpool.ExecNow(command)
			if nil != err {
				b.Error(err)
			}
		}
	})
}
