#!/bin/bash

SCRIPT_PATH=`pwd`
TCPEXAMPLE_PATH=$SCRIPT_PATH/../conn/tcp/example

cd $TCPEXAMPLE_PATH/client
echo "go build echo_client.go"
go build echo_client.go
echo "go build pb_echo_client.go"
go build pb_echo_client.go
echo "go build pb_power_test_client.go"
go build pb_power_test_client.go
echo "go build power_test_client.go"
go build power_test_client.go
cd -


cd $TCPEXAMPLE_PATH/server
echo "go build echo_server.go"
go build echo_server.go
echo "go build pb_echo_server.go"
go build pb_echo_server.go
cd -
