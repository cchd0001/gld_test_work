package pb_tool

import (
	"conn/packet"
	"conn/protobuf_file/pb_go"
	"conn/tcp"
	"errors"
	proto "github.com/golang/protobuf/proto"
)

func PbSend(packet *Packet.Packet, sender tcp_tool.ISend) error {
	data, err1 := proto.Marshal(packet)
	if nil == data || err1 != nil {
		return errors.New(" Marshal failed for Packet.Packet ")
	}
	err2 := packet_conn_tool.PacketSend(data, sender)
	if err2 != nil {
		return err2
	}
	return nil
}
