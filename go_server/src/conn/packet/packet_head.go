package packet_conn_tool

import (
	"encoding/binary"
	"errors"
)

const (
	PACKET_MAX = 32 * 1024
	HEAD_BYTE  = 4
)

//
// @Return
//    Marked bytes size .Type int32 . packet_len + HEAD_BYTE .
//    Read data slice . Type []byte
//    Error if error happend.

func CheckPacketHead(buff []byte) (int32, []byte, error) {
	var buff_len, packet_len int32
	buff_len = int32(len(buff))
	if 0 == buff_len {
		return 0, nil, errors.New("Empty buffer!")
	}
	// Buff too small . Let's wait for more data.
	if HEAD_BYTE > buff_len {
		return 0, nil, nil
	}
	u32_len := binary.LittleEndian.Uint32(buff)
	// The size data is bigger than expect.
	if u32_len > PACKET_MAX {
		return 0, nil, errors.New("PACKET_MAX overflow!")
	}
	// Maybe this is a rubbish data.
	if u32_len <= HEAD_BYTE {
		return 0, nil, errors.New("Invalid packet_len , not bigger than HEAD_BYTE")
	}
	packet_len = int32(u32_len)
	// Valid packet
	// Not the whole packet received .  Let's wait for more data.
	if buff_len < packet_len {
		return 0, nil, nil
	}
	// Already has all data .
	return packet_len, buff[HEAD_BYTE:packet_len], nil
}

func GenPacket(buff []byte) ([]byte, error) {
	buff_len := int32(len(buff))
	if buff_len < 1 {
		return nil, errors.New("Empty buffer")
	}
	buff_len += HEAD_BYTE
	if buff_len > PACKET_MAX {
		return nil, errors.New("PACKET_MAX overflow!")
	}
	real_buff := make([]byte, buff_len)
	binary.LittleEndian.PutUint32(real_buff, uint32(buff_len))
	copy(real_buff[HEAD_BYTE:], buff)
	return real_buff, nil
}
