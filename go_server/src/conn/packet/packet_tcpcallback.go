package packet_conn_tool

import (
	"conn/tcp"
	"error_tool"
	"log"
)

type PacketReader struct {
	m_handler HandlePacket
}

func (this *PacketReader) SetHandle(h HandlePacket) {
	if nil == this {
		log.Panic(error_tool.ErrThisNil)
	}
	this.m_handler = h
}

func (this *PacketReader) OnRead(conn *tcp_tool.TcpConn, buff []byte) int32 {
	if this == nil {
		log.Panic(error_tool.ErrThisNil)
	}
	if this.m_handler == nil {
		log.Panic("PacketTcpCallback : m_handler== nil")
	}
	ret_len, data, err := CheckPacketHead(buff)
	if err != nil {
		conn.Close(err)
		return 0
	}
	if nil != data {
		go this.m_handler.Handle(conn, data)
	}
	return ret_len
}
