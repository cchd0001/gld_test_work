package packet_conn_tool

import (
	"conn/tcp"
	"errors"
)

type HandlePacket interface {
	Handle(conn *tcp_tool.TcpConn, buff []byte)
}

func PacketSend(buff []byte, sender tcp_tool.ISend) error {
	real_send, err := GenPacket(buff)
	if err != nil {
		return err
	} else if err == nil && real_send == nil {
		return errors.New("GenPacket returns all nil !")
	}
	sender.Send(real_send)
	return nil
}
