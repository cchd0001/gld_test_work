package packet_conn_tool

import (
	"bytes"
	"encoding/binary"
	"testing"
)

func TestGenCheckPacket(t *testing.T) {
	var nil_buf []byte
	// Test nil as parameter
	p1, err := GenPacket(nil_buf)
	if nil == err || p1 != nil {
		t.Error("No error for nil")
	}
	l1, b1, e1 := CheckPacketHead(nil_buf)
	if 0 != l1 || nil != b1 || e1 == nil {
		t.Error("CheckPacketHead error for nil")
	}
	// Test empty slice as parameter
	empty_buff := make([]byte, 0)
	p2, err1 := GenPacket(empty_buff)
	if nil == err1 || p2 != nil {
		t.Error("No error for empty buff")
	}
	l2, b2, e2 := CheckPacketHead(empty_buff)
	if 0 != l2 || nil != b2 || e2 == nil {
		t.Error("CheckPacketHead error for empty")
	}
	// Test valid slice as parameter
	for i := 1; i < PACKET_MAX; i = i << 2 {
		buff := make([]byte, i)
		packet, err2 := GenPacket(buff)
		if nil != err2 || len(packet) != i+HEAD_BYTE || binary.LittleEndian.Uint32(packet) != uint32(i+HEAD_BYTE) {
			t.Error("GenPacket for ", i, " error")
		}
		packet = append(packet, make([]byte, i)...)
		len, buff1, err4 := CheckPacketHead(packet)
		if err4 != nil || len-HEAD_BYTE != int32(i) {
			t.Error("CheckPacketHead error  i = ", i, " err = ", err4, " len = ", len)
		}
		if !bytes.Equal(buff, buff1) {
			t.Error("CheckPacketHead data error i = ", i)
		}
	}

	// Test huge slice as parameter
	big_buff := make([]byte, PACKET_MAX)
	p2, err3 := GenPacket(big_buff)
	if nil == err3 || p2 != nil {
		t.Error("No error for empty buff")
	}
}

func BenchmarkGenPacket(b *testing.B) {
	var i int = PACKET_MAX - 5
	buff := make([]byte, i)
	for j := 0; j < b.N; j++ {
		packet, err2 := GenPacket(buff)
		if nil != err2 || len(packet) != i+HEAD_BYTE || binary.LittleEndian.Uint32(packet) != uint32(i+HEAD_BYTE) {
			b.Error("GenPacket for ", i, " error")
		}
	}
}

func BenchmarkGenPacketParallel(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		var i int = PACKET_MAX - 5
		buff := make([]byte, i)
		for pb.Next() {
			packet, err2 := GenPacket(buff)
			if nil != err2 || len(packet) != i+HEAD_BYTE || binary.LittleEndian.Uint32(packet) != uint32(i+HEAD_BYTE) {
				b.Error("GenPacket for ", i, " error")
			}
		}
	})
}
