package tcp_tool

import (
	"error_tool"
	"errors"
	"log"
	"net"
)

const BUFF_SIZE = 1024
const BUFF_MAX = 640 * 1024

type CommandId int

const (
	SHUTDOWN CommandId = iota
	IGNORE_TIMEOUT
	RESET_CB
)

type Command struct {
	Id   CommandId
	Data interface{}
}

type Status int

const (
	Valid         Status = iota //Can recive and send
	IgnoreTimeout               //Ignore timeout
	Shutdown                    //Can't recive and send , but will send all buff
	Close                       //Close. no operator support
)

type TcpConn struct {
	m_id      uint32
	m_conn    *net.TCPConn
	m_cb      Conncb
	m_recive  []byte
	m_buff    [BUFF_SIZE]byte
	m_status  Status
	m_command chan Command
	m_send    chan []byte
}

func NewTcpConn(conn *net.TCPConn, id uint32, cb Conncb) (*TcpConn, error) {
	if nil == conn {
		return nil, errors.New("Conn is nil")
	}
	if nil == cb {
		return nil, errors.New("Callback interface is null")
	}
	ret := &TcpConn{
		m_id:      id,
		m_conn:    conn,
		m_recive:  make([]byte, 0),
		m_cb:      cb,
		m_status:  Valid,
		m_command: make(chan Command),
		m_send:    make(chan []byte, 5),
	}
	go ret.m_cb.OnConnect(ret, nil)
	go func() {
		for {
			select {
			case c := <-ret.m_command:
				{
					switch c.Id {
					case SHUTDOWN:
						if ret.m_status < IgnoreTimeout {
							ret.m_status = Shutdown
							close(ret.m_send)
						}
					case IGNORE_TIMEOUT:
						if ret.m_status < IgnoreTimeout {
							ret.m_status = IgnoreTimeout
						}
					case RESET_CB:
						switch c.Data.(type) {
						case Conncb:
							{
								ret.m_cb = c.Data.(Conncb)
							}
						default:
						}
					}
				}
			case send_buf, ok := <-ret.m_send:
				if ok {
					len_left := len(send_buf)
					total_send_len := 0
					for len_left > 0 {
						send_len, err := ret.m_conn.Write(send_buf)
						if nil != err {
							go ret.Close(err)
							break
						}
						len_left -= send_len
						total_send_len += send_len
						send_buf = send_buf[send_len:]
					}

					ret.m_cb.OnSend(ret, int32(total_send_len))
				} else {
					ret.Close(nil)
				}
			}
		}
	}()
	return ret, nil
}

func (this *TcpConn) ResetCb(cb Conncb) {
	if this == nil {
		log.Panic(error_tool.ErrThisNil)
		return
	}
	this.m_command <- Command{
		Id:   RESET_CB,
		Data: cb,
	}
}

func (this *TcpConn) Send(send_buf []byte) {
	if this == nil {
		log.Panic(error_tool.ErrThisNil)
		return
	}
	if this.m_status > IgnoreTimeout {
		return
	}
	if nil == this.m_conn {
		log.Println("Send data to nil . connection id = ", this.m_id)
		return
	}
	this.m_send <- send_buf

}

func (this *TcpConn) Read() error {
	if this == nil {
		return error_tool.ErrThisNil
	}
	if nil == this.m_conn {
		log.Println("Read data from nil . connection id = ", this.m_id)
		return errors.New("Read data from nil ")
	}
	if this.m_status > IgnoreTimeout {
		return errors.New("Connection close !")
	}
	read_buf := this.m_buff[:]
	read_len, err := this.m_conn.Read(read_buf)
	if nil != err {
		go this.Close(err)
		return err
	}
	if len(this.m_recive)+read_len > BUFF_MAX {
		err1 := errors.New("Recieve buff overflow ! Connection shot down ")
		go this.Close(err1)
		return err1
	}
	this.m_recive = append(this.m_recive, read_buf[:read_len]...)
	deal_len := this.m_cb.OnRead(this, this.m_recive)
	if deal_len > 0 {
		this.m_recive = this.m_recive[deal_len:]
	}

	return nil
}

func (this *TcpConn) Close(err error) {
	if this == nil {
		log.Panic(error_tool.ErrThisNil)
		return
	}
	if nil == this.m_conn {
		//log.Println("Close nil . connection id = ", this.m_id)
		return
	}
	this.m_status = Close
	this.m_cb.OnDisconnect(this, err)
	this.m_conn.Close()
	this.m_conn = nil
}

func (this *TcpConn) GetId() uint32 {
	if this == nil {
		log.Panic(error_tool.ErrThisNil)
		return 0
	}
	return this.m_id
}
