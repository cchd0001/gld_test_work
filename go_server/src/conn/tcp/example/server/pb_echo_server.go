package main

//import "error_tool"

import (
	"conn/packet"
	pb_tool "conn/protobuf_file"
	"conn/protobuf_file/pb_go"
	tcp_tool "conn/tcp"
	proto "github.com/golang/protobuf/proto"
	"log"
)

type EchoHandler struct {
	tcp_tool.PrintOnDisconnect
	tcp_tool.PrintOnConnect
	tcp_tool.IgnoreOnSend
	packet_conn_tool.PacketReader
}

func (this *EchoHandler) Handle(conn *tcp_tool.TcpConn, buff []byte) {
	var packet Packet.Packet
	err := proto.Unmarshal(buff, &packet)
	if nil != err {
		conn.Close(err)
	}
	err2 := pb_tool.PbSend(&packet, conn)
	if err2 != nil {
		log.Print(err2)
		return
	}
}

func main() {
	var handler EchoHandler
	handler.SetHandle(&handler)
	ser := tcp_tool.NewTcpServer(&handler)
	ser.Init("127.0.0.1", "12345")
	select {}
}
