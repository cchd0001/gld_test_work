package main

//import "error_tool"

import (
	"conn/tcp"
)

type EchoServer struct {
	tcp_tool.PrintOnDisconnect
	tcp_tool.PrintOnConnect
	tcp_tool.IgnoreOnSend
}

func (this *EchoServer) OnRead(conn *tcp_tool.TcpConn, buff []byte) int32 {
	go conn.Send(buff)
	return int32(len(buff))
}

func main() {
	var cb EchoServer
	ser := tcp_tool.NewTcpServer(&cb)
	ser.Init("127.0.0.1", "12345")
	select {}
}
