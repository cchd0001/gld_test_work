package main

//import "error_tool"

import (
	"conn/packet"
	"conn/protobuf_file"
	"conn/protobuf_file/pb_go"
	"conn/tcp"
	"fmt"
	proto "github.com/golang/protobuf/proto"
	"log"
	"sync/atomic"
	"time"
)

type ReportHandler struct {
	tcp_tool.IgnoreOnSend
	packet_conn_tool.PacketReader
	total_send int32
	total_recv int32
	curr_send  int32
	curr_recv  int32
	total_conn int32
	total_disc int32
	time       time.Time
}

func NewReportClient() *ReportHandler {
	ret := &ReportHandler{
		total_send: 0,
		total_recv: 0,
		curr_send:  0,
		curr_recv:  0,
		total_conn: 0,
		total_disc: 0,
		time:       time.Now(),
	}
	ret.SetHandle(ret)
	return ret
}

func (this *ReportHandler) Handle(conn *tcp_tool.TcpConn, buff []byte) {
	var packet Packet.Packet
	err := proto.Unmarshal(buff, &packet)
	if nil != err {
		conn.Close(err)
		return
	}
	atomic.AddInt32(&this.total_recv, 1)
	atomic.AddInt32(&this.curr_recv, 1)
	this.Send(conn)
}

func (this *ReportHandler) Send(sender tcp_tool.ISend) {
	var hw string = "hello world"
	var hello Packet.Packet
	var com uint32 = 1
	hello.Command = &com
	hello.Serialized = []byte(hw)
	pb_tool.PbSend(&hello, sender)
	atomic.AddInt32(&this.total_send, 1)
	atomic.AddInt32(&this.curr_send, 1)
}
func (this *ReportHandler) OnDisconnect(conn *tcp_tool.TcpConn, e error) {
	atomic.AddInt32(&this.total_disc, 1)
	if nil == conn && nil == e {
		log.Panic("Woo ... nil can't passed in here")
		return
	} else if nil != e {
		fmt.Println("DisConnection ", conn.GetId())
		log.Print(e)
	} else {
		fmt.Println("DisConnection ", conn.GetId())
	}
}

func (this *ReportHandler) OnConnect(conn *tcp_tool.TcpConn, e error) {
	if nil == conn && nil == e {
		log.Panic("Woo ... nil can't passed in here")
		return
	} else if nil != e {
		log.Panic(e)
	} else {
		fmt.Println("Connection ", conn.GetId())
		atomic.AddInt32(&this.total_conn, 1)
		fmt.Println("total ", this.total_conn)
	}
}

func (this *ReportHandler) Report() {
	for {
		time.Sleep(time.Second)
		fmt.Println(" Total Disc ", this.total_disc)
		fmt.Println(" Total Conn ", this.total_conn)
		fmt.Println(" Total Send ", this.total_send)
		fmt.Println(" Total Recv ", this.total_recv)
		fmt.Println(" Curr  Send ", this.curr_send)
		fmt.Println(" Curr  Recv ", this.curr_send)
		fmt.Println(" Time ", time.Now().Sub(this.time))
		atomic.SwapInt32(&this.curr_send, 0)
		atomic.SwapInt32(&this.curr_recv, 0)
	}
}

func main() {
	handler := NewReportClient()
	for i := 0; i < 100; i++ {

		cli := tcp_tool.NewTcpClient(handler)
		cli.Init("127.0.0.1", "12345")
		go handler.Send(cli)
	}
	handler.Report()
}
