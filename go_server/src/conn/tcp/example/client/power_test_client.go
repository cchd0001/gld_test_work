package main

//import "error_tool"

import (
	"conn/tcp"
	"fmt"
	"sync/atomic"
	"time"
)

type EchoClient struct {
	tcp_tool.PrintOnConnect
	tcp_tool.PrintOnDisconnect
	total_send int32
	total_recv int32
	curr_send  int32
	curr_recv  int32
	total_conn int32
	total_disc int32
	time       time.Time
}

func NewEchoClient() *EchoClient {
	ret := &EchoClient{
		total_send: 0,
		total_recv: 0,
		curr_send:  0,
		curr_recv:  0,
		total_conn: 0,
		total_disc: 0,
		time:       time.Now(),
	}
	return ret
}

func (this *EchoClient) OnRead(conn *tcp_tool.TcpConn, buff []byte) int32 {
	len := int32(len(buff))

	atomic.AddInt32(&this.total_recv, len)
	atomic.AddInt32(&this.curr_recv, len)
	buff1 := make([]byte, 10)
	conn.Send(buff1)
	return len
}

func (this *EchoClient) OnSend(conn *tcp_tool.TcpConn, len int32) {
	atomic.AddInt32(&this.total_send, len)
	atomic.AddInt32(&this.curr_send, len)
}

func (this *EchoClient) Report() {
	for {
		time.Sleep(time.Second)
		fmt.Println(" Total Conn ", this.total_conn)
		fmt.Println(" Total Disc ", this.total_disc)
		fmt.Println(" Total Send ", this.total_send/10)
		fmt.Println(" Total Recv ", this.total_recv/10)
		fmt.Println(" Curr  Send ", this.curr_send/10)
		fmt.Println(" Curr  Recv ", this.curr_send/10)
		fmt.Println(" Time ", time.Now().Sub(this.time))
		atomic.SwapInt32(&this.curr_send, 0)
		atomic.SwapInt32(&this.curr_recv, 0)
	}
}

func main() {
	cb := NewEchoClient()
	buff := make([]byte, 10)
	for i := 0; i < 100; i++ {
		cli := tcp_tool.NewTcpClient(cb)
		cli.Init("127.0.0.1", "12345")
		go cli.Send(buff)
	}
	cb.Report()
}
