package main

//import "error_tool"

import (
	"conn/tcp"
	"fmt"
)

type EchoClient struct {
	tcp_tool.IgnoreOnConnect
	tcp_tool.IgnoreOnDisconnect
	tcp_tool.IgnoreOnSend
}

func (this *EchoClient) OnRead(conn *tcp_tool.TcpConn, buff []byte) int32 {
	fmt.Println(string(buff))
	return int32(len(buff))
}

func main() {
	var cb EchoClient
	cli := tcp_tool.NewTcpClient(&cb)
	cli.Init("127.0.0.1", "12345")

	for {
		var input string
		fmt.Scanf("%s", &input)
		buff := []byte(input)
		cli.Send(buff)
	}
}
