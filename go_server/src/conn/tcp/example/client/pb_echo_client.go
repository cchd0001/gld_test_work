package main

//import "error_tool"

import (
	"conn/packet"
	"conn/protobuf_file"
	"conn/protobuf_file/pb_go"
	"conn/tcp"
	"fmt"
	proto "github.com/golang/protobuf/proto"
	//"log"
	"time"
)

type PrintHandler struct {
}
type PrintCallback struct {
	tcp_tool.PrintOnDisconnect
	tcp_tool.PrintOnConnect
	tcp_tool.IgnoreOnSend
	packet_conn_tool.PacketReader
}

func (this PrintHandler) Handle(conn *tcp_tool.TcpConn, buff []byte) {
	var packet Packet.Packet
	err := proto.Unmarshal(buff, &packet)
	if nil != err {
		conn.Close(err)
	}
	fmt.Println(string(packet.GetSerialized()))
}

func main() {
	var handler PrintHandler
	var cb PrintCallback
	cb.SetHandle(&handler)
	cli := tcp_tool.NewTcpClient(&cb)
	cli.Init("127.0.0.1", "12345")
	var hw string = "hello world"
	var hello Packet.Packet
	hello.Command = proto.Uint32(1)
	hello.Serialized = []byte(hw)
	for {
		time.Sleep(time.Second)
		pb_tool.PbSend(&hello, cli)
	}
}
