package tcp_tool

import (
	"fmt"
	"log"
)

type ISend interface {
	Send([]byte)
}

type IOnConnect interface {
	OnConnect(conn *TcpConn, e error)
}

type IOnDisconnect interface {
	OnDisconnect(conn *TcpConn, e error)
}

type IOnRead interface {
	OnRead(conn *TcpConn, buff []byte) int32
}

type IOnSend interface {
	OnSend(conn *TcpConn, len int32)
}

type Conncb interface {
	IOnConnect
	IOnDisconnect
	IOnSend
	IOnRead
}

// Print callback
type PrintOnDisconnect struct {
}

func (this *PrintOnDisconnect) OnDisconnect(conn *TcpConn, e error) {
	if nil == conn && nil == e {
		log.Panic("Woo ... nil can't passed in here")
		return
	} else if nil != e {
		fmt.Println("DisConnection ", conn.GetId())
		log.Print(e)
	} else {
		fmt.Println("DisConnection ", conn.GetId())
	}
}

type PrintOnConnect struct {
}

func (this *PrintOnConnect) OnConnect(conn *TcpConn, e error) {

	if nil == conn && nil == e {
		log.Panic("Woo ... nil can't passed in here")
		return
	} else if nil != e {
		log.Panic(e)
	} else {
		fmt.Println("Connection ", conn.GetId())
	}
}

// Keep silent callback

type IgnoreOnSend struct {
}

func (this *IgnoreOnSend) OnSend(conn *TcpConn, len int32) {
}

type IgnoreOnDisconnect struct {
}

func (this *IgnoreOnDisconnect) OnDisconnect(conn *TcpConn, e error) {
}

type IgnoreOnConnect struct {
}

func (this *IgnoreOnConnect) OnConnect(conn *TcpConn, e error) {
}
