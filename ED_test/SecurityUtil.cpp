﻿//
//  SecurityUtil.cpp
//  TheKing
//
//  Created by Xiaobin Li on 2/11/15.
//
//

#include "SecurityUtil.h"
#include <stdlib.h>
#include <zlib.h>
#include "md5.h"
#include "sha2.h"


static const int32 MAX_ENCRYPT_KEY_LEN = 1024;
static uint32 _inflateMemoryWithHint(unsigned char *in, uint32 inLength, unsigned char **out, uint32 outLengthHint);

#define S11 7
#define S12 12
#define S13 17
#define S14 22
#define S21 5
#define S22 9
#define S23 14
#define S24 20
#define S31 4
#define S32 11
#define S33 16
#define S34 23
#define S41 6
#define S42 10
#define S43 15
#define S44 21
class MD5
{
public:
	typedef unsigned int size_type; // must be 32bit
	
	void init()
	{
		finalized=false;
		
		count[0] = 0;
		count[1] = 0;
		
		// load magic initialization constants.
		state[0] = 0x67452301;
		state[1] = 0xefcdab89;
		state[2] = 0x98badcfe;
		state[3] = 0x10325476;
	}
	
	void update(const unsigned char *input, size_type length)
	{
		// compute number of bytes mod 64
		size_type index = count[0] / 8 % blocksize;
		
		// Update number of bits
		if ((count[0] += (length << 3)) < (length << 3))
			count[1]++;
		count[1] += (length >> 29);
		
		// number of bytes we need to fill in buffer
		size_type firstpart = 64 - index;
		
		size_type i;
		
		// transform as many times as possible.
		if (length >= firstpart)
		{
			// fill buffer first, transform
			memcpy(&buffer[index], input, firstpart);
			transform(buffer);
			
			// transform chunks of blocksize (64 bytes)
			for (i = firstpart; i + blocksize <= length; i += blocksize)
				transform(&input[i]);
			
			index = 0;
		}
		else
			i = 0;
		
		// buffer remaining input
		memcpy(&buffer[index], &input[i], length-i);
	}
	void update(const char *buf, size_type length)
	{
		update((const unsigned char*)buf, length);
	}
	
	MD5& finalize()
	{
		static unsigned char padding[64] = {
			0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
		};
		
		if (!finalized) {
			// Save number of bits
			unsigned char bits[8];
			encode(bits, count, 8);
			
			// pad out to 56 mod 64.
			size_type index = count[0] / 8 % 64;
			size_type padLen = (index < 56) ? (56 - index) : (120 - index);
			update(padding, padLen);
			
			// Append length (before padding)
			update(bits, 8);
			
			// Store state in digest
			encode(digest, state, 16);
			
			// Zeroize sensitive information.
			memset(buffer, 0, sizeof buffer);
			memset(count, 0, sizeof count);
			
			finalized=true;
		}
		
		return *this;
	}
	
	std::string hexdigest() const
	{
		if (!finalized)
			return "";
		
		char buf[33];
		for (int i=0; i<16; i++)
			sprintf(buf+i*2, "%02x", digest[i]);
		buf[32]=0;
		
		return std::string(buf);
	}
	
private:
	typedef unsigned char uint1; //  8bit
	typedef unsigned int uint4;  // 32bit
	enum {blocksize = 64}; // VC6 won't eat a const static int here
	
	void transform(const uint1 block[blocksize])
	{
		uint4 a = state[0], b = state[1], c = state[2], d = state[3], x[16];
		decode (x, block, blocksize);
		
		/* Round 1 */
		FF (a, b, c, d, x[ 0], S11, 0xd76aa478); /* 1 */
		FF (d, a, b, c, x[ 1], S12, 0xe8c7b756); /* 2 */
		FF (c, d, a, b, x[ 2], S13, 0x242070db); /* 3 */
		FF (b, c, d, a, x[ 3], S14, 0xc1bdceee); /* 4 */
		FF (a, b, c, d, x[ 4], S11, 0xf57c0faf); /* 5 */
		FF (d, a, b, c, x[ 5], S12, 0x4787c62a); /* 6 */
		FF (c, d, a, b, x[ 6], S13, 0xa8304613); /* 7 */
		FF (b, c, d, a, x[ 7], S14, 0xfd469501); /* 8 */
		FF (a, b, c, d, x[ 8], S11, 0x698098d8); /* 9 */
		FF (d, a, b, c, x[ 9], S12, 0x8b44f7af); /* 10 */
		FF (c, d, a, b, x[10], S13, 0xffff5bb1); /* 11 */
		FF (b, c, d, a, x[11], S14, 0x895cd7be); /* 12 */
		FF (a, b, c, d, x[12], S11, 0x6b901122); /* 13 */
		FF (d, a, b, c, x[13], S12, 0xfd987193); /* 14 */
		FF (c, d, a, b, x[14], S13, 0xa679438e); /* 15 */
		FF (b, c, d, a, x[15], S14, 0x49b40821); /* 16 */
		
		/* Round 2 */
		GG (a, b, c, d, x[ 1], S21, 0xf61e2562); /* 17 */
		GG (d, a, b, c, x[ 6], S22, 0xc040b340); /* 18 */
		GG (c, d, a, b, x[11], S23, 0x265e5a51); /* 19 */
		GG (b, c, d, a, x[ 0], S24, 0xe9b6c7aa); /* 20 */
		GG (a, b, c, d, x[ 5], S21, 0xd62f105d); /* 21 */
		GG (d, a, b, c, x[10], S22,  0x2441453); /* 22 */
		GG (c, d, a, b, x[15], S23, 0xd8a1e681); /* 23 */
		GG (b, c, d, a, x[ 4], S24, 0xe7d3fbc8); /* 24 */
		GG (a, b, c, d, x[ 9], S21, 0x21e1cde6); /* 25 */
		GG (d, a, b, c, x[14], S22, 0xc33707d6); /* 26 */
		GG (c, d, a, b, x[ 3], S23, 0xf4d50d87); /* 27 */
		GG (b, c, d, a, x[ 8], S24, 0x455a14ed); /* 28 */
		GG (a, b, c, d, x[13], S21, 0xa9e3e905); /* 29 */
		GG (d, a, b, c, x[ 2], S22, 0xfcefa3f8); /* 30 */
		GG (c, d, a, b, x[ 7], S23, 0x676f02d9); /* 31 */
		GG (b, c, d, a, x[12], S24, 0x8d2a4c8a); /* 32 */
		
		/* Round 3 */
		HH (a, b, c, d, x[ 5], S31, 0xfffa3942); /* 33 */
		HH (d, a, b, c, x[ 8], S32, 0x8771f681); /* 34 */
		HH (c, d, a, b, x[11], S33, 0x6d9d6122); /* 35 */
		HH (b, c, d, a, x[14], S34, 0xfde5380c); /* 36 */
		HH (a, b, c, d, x[ 1], S31, 0xa4beea44); /* 37 */
		HH (d, a, b, c, x[ 4], S32, 0x4bdecfa9); /* 38 */
		HH (c, d, a, b, x[ 7], S33, 0xf6bb4b60); /* 39 */
		HH (b, c, d, a, x[10], S34, 0xbebfbc70); /* 40 */
		HH (a, b, c, d, x[13], S31, 0x289b7ec6); /* 41 */
		HH (d, a, b, c, x[ 0], S32, 0xeaa127fa); /* 42 */
		HH (c, d, a, b, x[ 3], S33, 0xd4ef3085); /* 43 */
		HH (b, c, d, a, x[ 6], S34,  0x4881d05); /* 44 */
		HH (a, b, c, d, x[ 9], S31, 0xd9d4d039); /* 45 */
		HH (d, a, b, c, x[12], S32, 0xe6db99e5); /* 46 */
		HH (c, d, a, b, x[15], S33, 0x1fa27cf8); /* 47 */
		HH (b, c, d, a, x[ 2], S34, 0xc4ac5665); /* 48 */
		
		/* Round 4 */
		II (a, b, c, d, x[ 0], S41, 0xf4292244); /* 49 */
		II (d, a, b, c, x[ 7], S42, 0x432aff97); /* 50 */
		II (c, d, a, b, x[14], S43, 0xab9423a7); /* 51 */
		II (b, c, d, a, x[ 5], S44, 0xfc93a039); /* 52 */
		II (a, b, c, d, x[12], S41, 0x655b59c3); /* 53 */
		II (d, a, b, c, x[ 3], S42, 0x8f0ccc92); /* 54 */
		II (c, d, a, b, x[10], S43, 0xffeff47d); /* 55 */
		II (b, c, d, a, x[ 1], S44, 0x85845dd1); /* 56 */
		II (a, b, c, d, x[ 8], S41, 0x6fa87e4f); /* 57 */
		II (d, a, b, c, x[15], S42, 0xfe2ce6e0); /* 58 */
		II (c, d, a, b, x[ 6], S43, 0xa3014314); /* 59 */
		II (b, c, d, a, x[13], S44, 0x4e0811a1); /* 60 */
		II (a, b, c, d, x[ 4], S41, 0xf7537e82); /* 61 */
		II (d, a, b, c, x[11], S42, 0xbd3af235); /* 62 */
		II (c, d, a, b, x[ 2], S43, 0x2ad7d2bb); /* 63 */
		II (b, c, d, a, x[ 9], S44, 0xeb86d391); /* 64 */
		
		state[0] += a;
		state[1] += b;
		state[2] += c;
		state[3] += d;
		
		// Zeroize sensitive information.
		memset(x, 0, sizeof x);
	}
	static void decode(uint4 output[], const uint1 input[], size_type len)
	{
		for (unsigned int i = 0, j = 0; j < len; i++, j += 4)
			output[i] = ((uint4)input[j]) | (((uint4)input[j+1]) << 8) |
			(((uint4)input[j+2]) << 16) | (((uint4)input[j+3]) << 24);
	}
	static void encode(uint1 output[], const uint4 input[], size_type len)
	{
		for (size_type i = 0, j = 0; j < len; i++, j += 4) {
			output[j] = input[i] & 0xff;
			output[j+1] = (input[i] >> 8) & 0xff;
			output[j+2] = (input[i] >> 16) & 0xff;
			output[j+3] = (input[i] >> 24) & 0xff;
		}
	}
	
	bool finalized;
	uint1 buffer[blocksize]; // bytes that didn't fit in last 64 byte chunk
	uint4 count[2];   // 64bit counter for number of bits (lo, hi)
	uint4 state[4];   // digest so far
	uint1 digest[16]; // the result
	
	// low level logic operations
	static inline uint4 F(uint4 x, uint4 y, uint4 z)
	{
		return (x&y) | (~x&z);
	}
	static inline uint4 G(uint4 x, uint4 y, uint4 z)
	{
		return (x&z) | (y&~z);
	}
	static inline uint4 H(uint4 x, uint4 y, uint4 z)
	{
		return x^y^z;
	}
	static inline uint4 I(uint4 x, uint4 y, uint4 z)
	{
		return y ^ (x | ~z);
	}
	static inline uint4 rotate_left(uint4 x, int n)
	{
		return (x << n) | (x >> (32-n));
	}
	static inline void FF(uint4 &a, uint4 b, uint4 c, uint4 d, uint4 x, uint4 s, uint4 ac)
	{
		a = rotate_left(a+ F(b,c,d) + x + ac, s) + b;
	}
	static inline void GG(uint4 &a, uint4 b, uint4 c, uint4 d, uint4 x, uint4 s, uint4 ac)
	{
		a = rotate_left(a + G(b,c,d) + x + ac, s) + b;
	}
	static inline void HH(uint4 &a, uint4 b, uint4 c, uint4 d, uint4 x, uint4 s, uint4 ac)
	{
		a = rotate_left(a + H(b,c,d) + x + ac, s) + b;
	}
	static inline void II(uint4 &a, uint4 b, uint4 c, uint4 d, uint4 x, uint4 s, uint4 ac)
	{
		a = rotate_left(a + I(b,c,d) + x + ac, s) + b;
	}
};

namespace GameBase {
    
    bool xorEncrypt(uint8* dataBuf, uint32 dataLen, const uint8* keyBuf, int32 keyLen/* = -1*/) {
        if (!dataBuf || !dataLen || !keyBuf) return false;
        int32 len = keyLen;
        if (-1 == len) len = (int32)strlen((char*)keyBuf);
        if (len > MAX_ENCRYPT_KEY_LEN) return false;
        
		for (uint32 i = 0; i<dataLen; ++i) dataBuf[i] ^= keyBuf[i%keyLen];
        
        return true;
    }
    
    bool zip(const uint8* src, uint32 srcLen, uint8 **dst, uint32* dstLen) {
        
        if (!src || !srcLen || !dst || !dstLen) return false;
        
        *dst = nullptr;
        *dstLen = 0;
        
        bool result = false;
        
        do {
            uLong length = compressBound(srcLen);
            uint8* buffer = (uint8*)malloc(length);
            if (!buffer) break;
            memset(buffer, 0x00, length);
            
            if (compress2(buffer, &length, src, srcLen, 1) == Z_OK) {
                *dst = buffer;
                *dstLen = (uint32)length;
                result = true;
            }
            else {
                free(buffer);
                break;
            }
        } while (0);
        
        return result;
        
    }
    
    bool unzip(const uint8* src, uint32 srcLen, uint8 **dst, uint32* dstLen) {
		// 256k for hint
		*dstLen = _inflateMemoryWithHint((unsigned char*)src, srcLen, dst, 256 * 1024);
        return *dstLen>0;
    }
	
	std::string md5(const std::string& str) {
        return md5((uint8*)str.c_str(), (uint32)str.length());
    }
	std::string md5(const uint8* data, uint32 len) {
		MD5 ctx;
		ctx.init();
		ctx.update((char*)data, len);
		ctx.finalize();
		return ctx.hexdigest();
    }
	
    std::string sha2(const std::string& str) {
        return sha2((uint8*)str.c_str(), (uint32)str.length());
    }
    std::string sha2(const uint8* data, uint32 len) {
        unsigned char digest[SHA256::DIGEST_SIZE];
        memset(digest, 0x00, SHA256::DIGEST_SIZE);
        
        SHA256 ctx;
        ctx.init();
        ctx.update((unsigned char*)data, len);
        ctx.finalize(digest);
        
        char buf[2*SHA256::DIGEST_SIZE+1];
        buf[2*SHA256::DIGEST_SIZE] = 0;
        for (int i = 0; i < SHA256::DIGEST_SIZE; i++) sprintf(buf+i*2, "%02x", digest[i]);
        
        return std::string(buf);
    }
}


#define BUFFER_INC_FACTOR (2)
static uint32 _inflateMemoryWithHint(unsigned char *in, uint32 inLength, unsigned char **out, uint32 *outLength, uint32 outLenghtHint)
{
	/* ret value */
	int err = Z_OK;
	
	uint32 bufferSize = outLenghtHint;
	*out = (unsigned char*)malloc(bufferSize);
	
	z_stream d_stream; /* decompression stream */
	d_stream.zalloc = (alloc_func)0;
	d_stream.zfree = (free_func)0;
	d_stream.opaque = (voidpf)0;
	
	d_stream.next_in  = in;
	d_stream.avail_in = static_cast<unsigned int>(inLength);
	d_stream.next_out = *out;
	d_stream.avail_out = static_cast<unsigned int>(bufferSize);
	
	/* window size to hold 256k */
	if( (err = inflateInit2(&d_stream, 15 + 32)) != Z_OK )
		return err;
	
	for (;;)
	{
		err = inflate(&d_stream, Z_NO_FLUSH);
		
		if (err == Z_STREAM_END)
		{
			break;
		}
		
		switch (err)
		{
			case Z_NEED_DICT:
				err = Z_DATA_ERROR;
			case Z_DATA_ERROR:
			case Z_MEM_ERROR:
				inflateEnd(&d_stream);
				return err;
		}
		
		// not enough memory ?
		if (err != Z_STREAM_END)
		{
			*out = (unsigned char*)realloc(*out, bufferSize * BUFFER_INC_FACTOR);
			
			/* not enough memory, ouch */
			if (! *out )
			{
				inflateEnd(&d_stream);
				return Z_MEM_ERROR;
			}
			
			d_stream.next_out = *out + bufferSize;
			d_stream.avail_out = static_cast<unsigned int>(bufferSize);
			bufferSize *= BUFFER_INC_FACTOR;
		}
	}
	
	*outLength = bufferSize - d_stream.avail_out;
	err = inflateEnd(&d_stream);
	return err;
}

static uint32 _inflateMemoryWithHint(unsigned char *in, uint32 inLength, unsigned char **out, uint32 outLengthHint)
{
	uint32 outLength = 0;
	int err = _inflateMemoryWithHint(in, inLength, out, &outLength, outLengthHint);
	
	if (err != Z_OK || *out == nullptr) {
		if (err == Z_MEM_ERROR)
		{
		}
		else
		{
			if (err == Z_VERSION_ERROR)
			{
			}
			else
			{
				if (err == Z_DATA_ERROR)
				{
				}
				else
				{
				}
			}
		}
		
		if(*out) {
			free(*out);
			*out = nullptr;
		}
		outLength = 0;
	}
	
	return outLength;
}

#if 0
#include "unittest.h"

#define AS(a) (sizeof(a)/sizeof(a[0]))

static uint8 test_data[] = {
    0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F,
    0x72, 0x6C, 0x64, 0x21, 0x20, 0xE4, 0xBD, 0xA0,
    0xE5, 0xA5, 0xBD, 0xEF, 0xBC, 0x81
};

static uint8 test_key[] = {
    0xE4, 0xBD, 0xA0, 0xE7, 0x8C, 0x9C
};

TEST_CASE(XOR) {
    const char *rawData = "Hello World! 你好！";
    LOGI("raw: %s" << rawData);
    char *data = strdup(rawData);
    uint32 len = static_cast<uint32>(strlen(data));
    std::string key("你猜");
    GameBase::xorEncrypt((uint8*)data, len, (uint8*)key.c_str(), static_cast<uint32>(key.length()));
    LOGI("xor: " << data);
    GameBase::xorEncrypt((uint8*)data, len, (uint8*)key.c_str(), static_cast<uint32>(key.length()));
    LOGI("xor: " << data);
    
    TEST_ASSERT(strcmp(rawData, data)==0);
    
    len = AS(test_data);
    uint8 *test_data_copy = new uint8[len + 1];
    test_data_copy[len] = '\0';
    memcpy(test_data_copy, test_data, len);
    GameBase::xorEncrypt(test_data_copy, len, test_key, AS(test_key));
    GameBase::xorEncrypt(test_data_copy, len, test_key, AS(test_key));
    LOGI("test_data_copy: " << test_data_copy);
    TEST_ASSERT(memcmp(test_data, test_data_copy, len)==0);
    
    delete[] test_data_copy;
    free(data);
}

TEST_CASE(ZIP) {
    const char *rawData = "Hello World! 你好！";
    LOGI("raw: " << rawData);
    char *data = strdup(rawData);
    uint32 len = static_cast<uint32>(strlen(data));
    
    uint8 *compressed = nullptr;
    uint32 compressedLen = 0;
    TEST_ASSERT(GameBase::zip((uint8*)data, len, &compressed, &compressedLen));
    printf("zip header: %02x%02x%02x%02x%02x%02x%02x%02x\n",
                   compressed[0], compressed[1], compressed[2], compressed[3],
                   compressed[4], compressed[5], compressed[6], compressed[7]);
    
    uint8 *uncompressed = nullptr;
    uint32 uncompressedLen = 0;
    TEST_ASSERT(GameBase::unzip(compressed, compressedLen, &uncompressed, &uncompressedLen));
    char *szData = new char[uncompressedLen+1];
    szData[uncompressedLen] = '\0';
    memcpy(szData, uncompressed, uncompressedLen);
    LOGI("after zip and unzip: " << szData);
    
    TEST_ASSERT(strcmp(rawData, szData)==0);
    
    free(compressed);
    free(uncompressed);
    delete[] szData;
    free(data);
    
    TEST_ASSERT(GameBase::zip(test_data, AS(test_data), &compressed, &compressedLen));
    printf("zip header: %02x%02x%02x%02x%02x%02x%02x%02x\n",
                   compressed[0], compressed[1], compressed[2], compressed[3],
                   compressed[4], compressed[5], compressed[6], compressed[7]);
    TEST_ASSERT(GameBase::unzip(compressed, compressedLen, &uncompressed, &uncompressedLen));
    TEST_ASSERT(AS(test_data) == uncompressedLen);
    TEST_ASSERT(memcmp(uncompressed, test_data, AS(test_data)) == 0);
    free(compressed);
    free(uncompressed);
    {
        const char* test = "1234567890";
        TEST_ASSERT(GameBase::zip((uint8*)test, static_cast<uint32>(strlen(test)), &compressed, &compressedLen));
        printf("zip header: %02x%02x%02x%02x%02x%02x%02x%02x\n",
                       compressed[0], compressed[1], compressed[2], compressed[3],
                       compressed[4], compressed[5], compressed[6], compressed[7]);
        TEST_ASSERT(GameBase::unzip(compressed, compressedLen, &uncompressed, &uncompressedLen));
        TEST_ASSERT(strlen(test) == uncompressedLen);
        free(compressed);
        free(uncompressed);
    }
    {
        const char* test = "abcdefghi";
        TEST_ASSERT(GameBase::zip((uint8*)test, (uint32)strlen(test), &compressed, &compressedLen));
        printf("zip header: %02x%02x%02x%02x%02x%02x%02x%02x\n",
                       compressed[0], compressed[1], compressed[2], compressed[3],
                       compressed[4], compressed[5], compressed[6], compressed[7]);
        TEST_ASSERT(GameBase::unzip(compressed, compressedLen, &uncompressed, &uncompressedLen));
        TEST_ASSERT(strlen(test) == uncompressedLen);
        free(compressed);
        free(uncompressed);
    }
    {
        const char* test = "zzzzzzzzzzzzzzzzz";
        TEST_ASSERT(GameBase::zip((uint8*)test, static_cast<uint32>(strlen(test)), &compressed, &compressedLen));
        printf("zip header: %02x%02x%02x%02x%02x%02x%02x%02x\n",
                       compressed[0], compressed[1], compressed[2], compressed[3],
                       compressed[4], compressed[5], compressed[6], compressed[7]);
        TEST_ASSERT(GameBase::unzip(compressed, compressedLen, &uncompressed, &uncompressedLen));
        TEST_ASSERT(strlen(test) == uncompressedLen);
        free(compressed);
        free(uncompressed);
    }
    {
        const char* test = "ccccccccccccccccccccccccc";
        TEST_ASSERT(GameBase::zip((uint8*)test, (uint32)strlen(test), &compressed, &compressedLen));
        printf("zip header: %02x%02x%02x%02x%02x%02x%02x%02x\n",
                       compressed[0], compressed[1], compressed[2], compressed[3],
                       compressed[4], compressed[5], compressed[6], compressed[7]);
        TEST_ASSERT(GameBase::unzip(compressed, compressedLen, &uncompressed, &uncompressedLen));
        TEST_ASSERT(strlen(test) == uncompressedLen);
        free(compressed);
        free(uncompressed);
    }
    {
        const char* test = "日日日日日日日日日日日日日日日日日日日日日日日日日日日日日日日日日日日日日日日";
        TEST_ASSERT(GameBase::zip((uint8*)test, (uint32)strlen(test), &compressed, &compressedLen));
        printf("zip header: %02x%02x%02x%02x%02x%02x%02x%02x\n",
                       compressed[0], compressed[1], compressed[2], compressed[3],
                       compressed[4], compressed[5], compressed[6], compressed[7]);
        TEST_ASSERT(GameBase::unzip(compressed, compressedLen, &uncompressed, &uncompressedLen));
        TEST_ASSERT(strlen(test) == uncompressedLen);
        free(compressed);
        free(uncompressed);
    }
}

TEST_CASE(SIGNATURE) {
    const char *rawData = "Hello World!";
    std::string md5 = GameBase::md5((uint8*)rawData, (uint32)strlen(rawData));
    LOGI("md5(\"Hello World!\")=" << md5);
    TEST_ASSERT(md5 == "ed076287532e86365e841e92bfc50d8c");
    
    std::string sha2 = GameBase::sha2(rawData);
    LOGI("sha2(\"Hello World!\")=" << sha2);
    TEST_ASSERT(sha2 == "7f83b1657ff1fc53b92dc18148a1d65dfc2d4b1fa3d677284addd200126d9069");
    
    std::string test_md5 = GameBase::md5(test_data, AS(test_data));
    LOGI("md5(test_data)=%s" << test_md5);
    TEST_ASSERT(test_md5 == "38165febac12accd9cd9876cad45f1de");
    
    std::string test_sha2 = GameBase::sha2(test_data, AS(test_data));
    LOGI("sha2(test_data)=%s" << test_sha2);
    TEST_ASSERT(test_sha2 == "dcad188403ba3a4931288076f8398283abed9a90d1955364b3b5beeb551f0062");
}

#endif
