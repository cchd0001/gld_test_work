#include "UuidErypt.h"

#include <ctime>

#include "SecurityUtil.h"
#include "base64.h"

const std::string _key_y("MGh^Itv7NNNdQ*t@&KSPcGeEW$%zLyH0");
const std::string _key_z("$haSA9y0!@Y05EYzgnjs3aUdwCjo#@X^");

const std::string salt("#yqgEEK%mxi3AsSS&IBIbDpE9*xRf*4p");

static const int MAX_LOGIN_TIME = 5 * 60;


static std::string  AddSalt(const std::string & key){
    return salt+key ; // +"|"+std::to_string(time(nullptr));
}

static std::string  RemoveSalt( const std::string & key){
    return key.substr(salt.length());
    /*
	auto uuid_base = key.substr(salt.length());
	auto vs = SplitString(uuid_base, '|');
	if (vs.size() != 2)
	{
		return "";
	}
	auto timestamp = atoll(vs[1].c_str());
	if (abs(time(nullptr) - timestamp) > MAX_LOGIN_TIME)
	{
		return "";
	}
    return vs[0];*/
}

const std::string Prefix("YOYO");

static std::string AddPrefix(const std::string & key) {
    return Prefix+key;
}

static bool CheckPrefix(const std::string & key) {
    if( key.size() > Prefix.size() && key.substr(0,Prefix.size()) == Prefix) {
        return true ;
    }
    return false ;
}

static std::string RemovePrefix(const std::string & key ) {
    return key.substr(Prefix.size());
}

static std::string CalcString(const std::string & source , const std::string & key ){
    std::string result(source);
    for( int i= 0 ; i < int(result.size()) ; i++ ){
        result[i] = result[i] ^ key[i%key.size()];
    }
    return result;
}

std::string EncyptUuid(const std::string & uuid) {
    std::string add_salt = AddSalt(uuid);
    std::string misc_1 = CalcString(add_salt , _key_y);
    char * ret = nullptr;
    uint32 len = 0;
    if( GameBase::zip( (uint8 *)misc_1.c_str() , misc_1.size() , (uint8 **)&ret , &len ) && ret && len > 0 ) {
        std::string after_zip(ret, len);
        after_zip[0] = std::rand() % 256 ;
        free(ret);
        std::string need_base_64 = CalcString(after_zip , _key_z) ;
        std::string after_base64 = ZBase64::Encode( (const unsigned char*)need_base_64.c_str() , len ) ;
        return AddPrefix(after_base64);
    } else {
        return "";
    }
}

std::string DecyptUuid(const std::string & uuid) {
    if( !  CheckPrefix(uuid) ) {
        return "";
    } 
    std::string need_key_z = RemovePrefix(uuid) ;
    int ret_len;
    std::string after_base64 = ZBase64::Decode((const char*)need_key_z.c_str() , need_key_z.size() ,ret_len);
    std::string need_unzip = CalcString(after_base64, _key_z) ;
    need_unzip[0] = 0x78 ;
    char * ret = nullptr;
    uint32 len = 0;
    if(GameBase::unzip( (uint8 *)need_unzip.c_str() , need_unzip.size() ,(uint8 **) &ret , &len )  && ret && len > 0 ) {
        std::string need_key_y(ret , len);
        free(ret);
        return RemoveSalt(CalcString(need_key_y , _key_y));
    }else {
        return "";
    }
}
