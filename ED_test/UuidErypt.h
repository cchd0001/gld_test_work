#ifndef __UUIDERYPT_H__
#define __UUIDERYPT_H__

#include <string>

std::string EncyptUuid(const std::string & uuid) ;

std::string DecyptUuid(const std::string & uuid) ;

#endif // __UUIDERYPT_H__
