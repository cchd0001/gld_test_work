﻿//
//  SecurityUtil.h
//  TheKing
//
//  Created by Xiaobin Li on 2/11/15.
//
//

#ifndef __TheKing__SecurityUtil__
#define __TheKing__SecurityUtil__
#include <string>
#include <vector>

typedef unsigned int uint32 ;
typedef int int32 ;
typedef unsigned char uint8 ;
//#include "../Common/Types.h"

namespace GameBase {

    // dataBuf[0, dataLen-1] ^ keyBuf[0, keyLen-1]
    bool xorEncrypt(uint8* dataBuf, uint32 dataLen, const uint8* keyBuf, int32 keyLen = -1);

	// todo(lxb): zip压缩后前两个字节是{0x78, 0xda}?
	// 调用者free(dst)
	bool zip(const uint8* src, uint32 srcLen, uint8 **dst, uint32* dstLen);
	// 调用者free(dst)
	bool unzip(const uint8* src, uint32 srcLen, uint8 **dst, uint32* dstLen);


	std::string md5(const std::string& str);
	std::string md5(const uint8* data, uint32 len);

	std::string sha2(const std::string& str);
	std::string sha2(const uint8* data, uint32 len);
}

#endif /* defined(__TheKing__SecurityUtil__) */
