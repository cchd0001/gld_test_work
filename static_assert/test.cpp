#include <utility>
#include <iostream>
struct Y{};
namespace helper
{
  template <typename T ,typename M = int>
  struct is_member_of_sample : std::false_type
  {
  };

  template <typename T>
  struct is_member_of_sample<T,
      decltype(
          adl_is_member_of_sample(
              std::declval<T>()
              )
          ) > : std::true_type
  {
  };
}

template <typename T>
int adl_is_member_of_sample(T && ){
    std::cout<<"11\n";
}


namespace sample
{
}


//-- Test it

namespace sample
{
  struct X{};
}


static_assert(not helper::is_member_of_sample<sample::X>::value, "");
static_assert(helper::is_member_of_sample<Y>::value, "");
static_assert(true , "Yes");
static_assert(not false , "Yes1");

int main(){
    Y y;
    adl_is_member_of_sample(y);
    sample::X x;
    adl_is_member_of_sample(x);
    return 0;
}
