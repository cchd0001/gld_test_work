#include <chrono>
#include <iostream>

#define LOGD(x) \
    std::cout<<x<<std::endl;

struct ProfileTimer
{
    ProfileTimer(const char* name) : _name(name), _startTime(std::chrono::high_resolution_clock::now())
    {
        LOGD( "enter " << _name);
    }
    ~ProfileTimer()
    {
        auto _endTime = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(_endTime-_startTime).count();
        LOGD("leave " << _name << " " << duration << "us.");
    }
    const char *_name;
    std::chrono::high_resolution_clock::time_point _startTime;
};


int main(){
    std::string name = "a";
    int j  = 0;
    ProfileTimer(name.c_str());
    for( int i = 0 ; i < 10000 ; i ++ ) {
        long a = time(0);
        j++;
    }
    j;
    return 0;
}
