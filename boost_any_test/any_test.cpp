#include "boost/any.hpp"
#include <iostream>
#include <cassert>

struct Test_Any
{
    Test_Any() 
    {
        i = 0 ;
        std::cout<<"Construct ... i = "<<i<<std::endl;
    }
    Test_Any( const Test_Any & b)
    {
        i = b.i +1 ;
        std::cout<<"Copy Construct ... i = "<<i<<std::endl;
    }
    ~Test_Any()
    {
        std::cout<<"~Construct ... i= "<<i<<std::endl;
    }

    int i ;
};


struct A{};

int main()
{
    Test_Any t ; // i = 0 
    boost::any test(t) ; // i = 1 
    //test = 1 ;
    void * ret = new A() ;
    assert( typeid(ret) == typeid(new A())) ;

    if( test.type() == typeid(Test_Any) )
    {
        auto got = boost::any_cast<Test_Any>(test); // i = 2
        std::cout<< got.i<< std::endl;
        test = got; // i = 3
        auto got_ptr = boost::any_cast<Test_Any>(&test); // i = 3 
        std::cout<< got_ptr->i<< std::endl;
        got_ptr->i = 10 ;
    }
    return 0 ;
}
