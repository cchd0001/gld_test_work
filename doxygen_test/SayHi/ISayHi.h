#ifndef __SAYHI_ISAYHI_H__
#define __SAYHI_ISAYHI_H__
#include <string>

/*! \addtogroup TestInterface
 *
 *  Contain interface class SayHi .
 */


class ISayHi {
    public:
        virtual void SayHi( const std::string & name ) = 0 ; 

        virtual ~ISayHi() {}
};

//! A Function 
/*!
 * return the word " hi "
 */

std::string Hi() ;

#endif //__SAYHI_ISAYHI_H__
