#include <iostream>
#include <string>

/*! \addtogroup test
 *  
 *  Test doxygen . Only has 1 module named test
 */


//! test class 1
/*!
 * A more ... about class 
 */
class Test{
    int i ;
};



//! test class 2
/*!
 *  a children of class 1
 */
class Test2 : public Test{
    int i ;
};

/*!
 *  Function SayHi
 */
int SayHi(const std::string & name) {
    std::cout<<"Hi "<<name<<std::endl;
}

std::string name = "World " ; //!< The name is World

/*!\brief Main
 *
 *  Function main
 */
int main(){
    SayHi(name);
    return 0;
}

/*! @! asdad */
