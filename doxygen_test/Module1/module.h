#ifndef __MODULE1_MODULE_H__
#define __MODULE1_MODULE_H__

#include "../SayHi/ISayHi.h"

 /*! \addtogroup Module2
 *
 *  Test Module2 .
 *  Provide interface SayHi which say hi before name.
 */

 //! class SayHi_before
/*!
 *  SayHi which say hi before name.
 */
class SayHi_before : public ISayHi {
    public:
        //! A member function
        //! say hi before name.
        virtual void SayHi( const std::string & name ) final;

        virtual ~SayHi_before() {}
};
#endif //__MODULE1_MODULE_H__
