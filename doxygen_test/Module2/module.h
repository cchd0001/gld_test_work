#ifndef __MODULE2_MODULE_H__
#define __MODULE2_MODULE_H__

#include "../SayHi/ISayHi.h"

/*! \addtogroup Module1
 *
 *  Test Module1 .
 *  Provide interface SayHi which say hi after name.
 */


//! class SayHi_after
/*!
 *  SayHi which say hi after name.
 */
 
class SayHi_after : public ISayHi {
    public:
        
        virtual void SayHi( const std::string & name ) final; //< say hi after name.

        virtual ~SayHi_after() {}

    private:

        void NotShow(); //< not show this
};
#endif //__MODULE2_MODULE_H__
