#include <iostream>
#include "Hash.h"

struct Test{
	int i ;
	double t;
};

int main() {
	int a;
	std::cout<<Hash(100)<<std::endl;
	std::cout<<Hash(100.01f)<<std::endl;
	std::cout<<Hash(&a)<<std::endl;
	std::cout<<Hash(std::string("...."))<<std::endl;
	//std::cout<<Hash(Test{1,3.0f})<<std::endl;
	return 0; 
}
