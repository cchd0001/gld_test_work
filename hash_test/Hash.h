#include <functional>

template<class T>
size_t Hash(const T t){
	return std::hash<T>()(t);
}
