#include <string>
#include <stdio.h>
#include <stdlib.h>

int main()
{
    std::string a("123  456  789");
    printf("a = %p \n",a.c_str());
    std::string b = a.substr(3,5);
    printf("b = %p \n",b.c_str());
    printf("sub = %d \n",atoi(a.substr(3).c_str()));
    printf("b == %d\n",atoi(b.c_str()));
    return 0;
}
