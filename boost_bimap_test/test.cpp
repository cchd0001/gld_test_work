#include <string>
#include <iostream>

#include <boost/bimap.hpp>
#include <boost/bimap/list_of.hpp>
#include <boost/bimap/set_of.hpp>
#include <boost/bimap/unordered_set_of.hpp>
/*
template< class MapType >
void print_map(const MapType & map,
               const std::string & separator,
               std::ostream & os )
{
    typedef typename MapType::const_iterator const_iterator;

    for( const_iterator i = map.begin(), iend = map.end(); i != iend; ++i )
    {
        os << i->first << separator << i->second << std::endl;
    }
}*/


template<class T>
void print_bimap_lbase(const T & results) {
    for( auto i = results.begin() ; i != results.end() ; i++){
        std::cout<< i->get_left_pair().first<<" <--> " << i->get_left_pair().second << std::endl;
    }
}

template<class T>
void print_bimap_rbase(const T & results) {
    for( auto i = results.begin() ; i != results.end() ; i++){
        std::cout<< i->get_right_pair().first<<" <--> " << i->get_right_pair().second << std::endl;
    }
}


struct name {} ;
struct rank_{} ;

typedef boost::bimap< boost::bimaps::unordered_set_of< boost::bimaps::tagged<std::string , name> >
                    , boost::bimaps::list_of< boost::bimaps::tagged< int , rank_ > >
                    , boost::bimaps::list_of_relation
                    , boost::bimaps::with_info<std::string>
                    > bimap;

typedef bimap::value_type value;
typedef bimap::left_value_type  lvalue;
typedef bimap::right_value_type  rvalue;
int main() {
    bimap rank ;
    /////////////////////////////
    // 增 .
    /////////////////////////////
    // 整体.
    rank.push_back( value( "Test1" , 1 , "like test 1") );
    rank.push_back( value( "Test2" , 2 , "like test 2") );
    // 从左边的容器.
    auto & left = rank.left;
    left.insert( lvalue( "Test3" , 3 ) );
    left["Test4"] = 4 ;
    // 从右边的容器.
    auto & right = rank.right;
    right.push_back( rvalue(5 , "Test5") ) ;
    //输出
    print_bimap_lbase(rank) ;
    print_bimap_rbase(rank) ;

    /////////////////////////////
    // 查 + 改 +  删.
    /////////////////////////////

    // 整体.
    auto equal_rank1 = [] (const value & v ) {
        return 1 == v.get_left_pair().second;
    };
    // use iterator
    bimap::iterator it = std::find_if( rank.begin() , rank.end() , equal_rank1 ) ; 
    std::cout<<"Top 1 is "<<it->get<name>()<< it->get_left_pair().info << std::endl;
    // use tag
    std::cout<<"Test1 is "<<rank.by<name>().at("Test1") <<std::endl ;
    //rank.by<name>() = 7 ;
    //it->get<name>() = "test_m1" ;
    print_bimap_lbase(rank) ;
    rank.erase(it);
    print_bimap_rbase(rank) ;
    
    // 从左边的容器.
    std::cout<<" Test2 is "<<left.at("Test2")<<std::endl;
    left["Test2"] = 12 ;
    print_bimap_lbase(rank) ;
    left.erase("Test2") ;
    print_bimap_rbase(rank) ;
    // 从右边的容器.
    auto get_test3 = [] (const rvalue & r) {
        return 3 == r.first ;
    };
    auto itr = std::find_if( right.begin() , right.end() , get_test3 );
    std::cout<<" top 3 is "<< itr->first <<std::endl;
    //itr->operator= ("Test_m3" );
    right.erase(itr);
    print_bimap_lbase(rank) ;
    print_bimap_rbase(rank) ;

    return 0 ;
}

/////! 增查 : 
//int main()
//{
//    // Soccer World cup
//
//    //typedef boost::bimap< std::string, int > results_bimap;
//    typedef  boost::bimaps::bimap< boost::bimaps::unordered_set_of< std::string>, boost::bimaps::list_of<int> , boost::bimaps::list_of_relation> results_bimap;
//    typedef results_bimap::value_type position;
//    //  增Add 
//    results_bimap results;
//    //results.insert( position("Argentina"    ,5) );
//    //results.insert( position("TTT"    ,2) );
//    //results.insert( position("Spain"        ,1) );
//    //results.insert( position("Germany"      ,3) );
//    //results.insert( position("France"       ,4) );
//
//    results.push_back( position("Argentina"    ,5) );
//    results.push_back( position("TTT"    ,2) );
//    results.push_back( position("Spain"        ,1) );
//    results.push_back( position("Germany"      ,3) );
//    results.push_back( position("France"       ,4) );
//    results.left["SSS"] = 6 ;
//    // results.right[7] = "OOO" ;
//    // 查 
//    std::cout << "The number of countries is " << results.size()
//        << std::endl;
//
//    std::cout << "The winner TTT is " << results.left.at("TTT")
//        << std::endl
//        << std::endl;
//
//    std::cout << "Countries names ordered by their final position:"
//        << std::endl;
//
//    results.left["SSS"] = 8 ;
//    // results.right works like a std::map< int, std::string >
//
//    print_map( results.right, ") ", std::cout );
//
//    std::cout << std::endl
//        << "Countries names ordered alphabetically along with"
//        "their final position:"
//        << std::endl;
//
//    // results.left works like a std::map< std::string, int >
//
//    print_map( results.left, " ends in position ", std::cout );
//    
//    for( auto i = results.begin() ; i != results.end() ; i++){
//        std::cout<< i->get_left_pair().first<<" <-L-> " << i->get_left_pair().second << std::endl;
//    }
//    return 0;
//}
