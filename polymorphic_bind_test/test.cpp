#include <functional>
#include <iostream>

struct Test {
    virtual void Print() = 0;
    std::function<void()> GenPrint() {
        return std::bind(&Test::Print , this);
    }
};

struct Test1 : public Test {
    virtual void Print() override {
        std::cout<<__FUNCTION__<<" from 1"<<std::endl;   
    };
};

struct Test2 : public Test {
    virtual void Print() override {
        std::cout<<__FUNCTION__<<" from 2"<<std::endl;   
    }
};


int main() {
    Test1 t1;
    auto p1 = t1.GenPrint();
    p1();

    Test2 t2;
    auto p2 = t2.GenPrint();
    p2();
    return 0;
}
