#include <iostream>
#include <thread>
#include <mutex>

//attach PID
//info thread
//thread n

int main()
{
    auto dead_lock = []()
    {
            std::mutex l ;
            std::lock_guard<std::mutex> ll(l);
            std::lock_guard<std::mutex> lll(l);
    };

    auto thread = std::thread(dead_lock);
    thread.join() ;
    return 0 ;
}
