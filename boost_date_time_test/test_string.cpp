#include <boost/date_time/posix_time/posix_time.hpp>
#include <iostream>

int main() {
    boost::posix_time::time_duration td(0,0,0);
    std::stringstream ss("00:23:11.123131");
    ss >> td;
    std::cout << td <<std::endl; // "14:23:11.345678
    std::cout << td.fractional_seconds() <<std::endl;

    auto now = boost::posix_time::second_clock::local_time();
    std::cout<<now<<std::endl;
    std::cout << boost::posix_time::time_duration(now - boost::posix_time::from_time_t(0)).fractional_seconds() <<std::endl;
    std::cout<<now + td<<std::endl;
    std::stringstream ss1("-abc-");
    ss1 >> td; // dosn't change td
    std::cout<<td<<std::endl;
    return 0 ;
}
