#include <iostream>

struct Test{
    Test(){
        std::cout<<"From Construct "<<this<<std::endl;
    }

    void Print(){
        std::cout<<"From Construct "<<this<<std::endl;
    }
};


int main() {
    for(int i = 0 ; i < 10 ; i ++){
        Test m;
        m.Print();
    }
    return 0;
}
