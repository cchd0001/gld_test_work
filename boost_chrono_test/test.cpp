
#include <boost/chrono/chrono.hpp>
#include <boost/chrono/clock_string.hpp>
#include <boost/chrono/process_cpu_clocks.hpp>
#include <iostream>

int main() {
    auto timep = boost::chrono::system_clock::now();
    auto cpu = boost::chrono::process_real_cpu_clock::now();
    return 0;
}
