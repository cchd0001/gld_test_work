#!/bin/bash
# Only 1 user can use restart

flock -xn ./rebuild.sh -c ./rebuild.sh

if [[ $? == 1 ]] ; then 
echo ==========================================================
echo = There are others locked the rebuild script.
echo = Wait until others build finish before start your build
echo ==========================================================
fi   
