#include "test.h"

int main()
{
    auto p1 = [](){
        std::this_thread::sleep_for(std::chrono::seconds(1));
        std::cout<< "thread #1 : "<<GetTestSingletonMgr().i<<std::endl;
    };

    auto p2 = [](){
        std::this_thread::sleep_for(std::chrono::seconds(1));
        std::cout<< "thread #2 : "<<GetTestSingletonMgr().i<<std::endl;
    };

    auto a1 = std::thread(p1) ;
    auto a2 = std::thread(p2) ;

    a1.join();
    a2.join();
}
