#pragma once

#include <iostream>
#include <chrono>
#include <thread>

struct TestSingleton
{
    TestSingleton() 
    {
        for(i = 0 ; i < 10; i++ )
        {
            std::cout<<__FUNCTION__<< " i = " << i <<std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }

    int i ;
};

inline  TestSingleton & GetTestSingletonMgr()
{
    static TestSingleton r ;
    return r ;
}
