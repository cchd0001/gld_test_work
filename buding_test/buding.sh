#!/bin/bash

###################################################################
#
# @SYNOPSIS
#                   CalcTime [timestamp] .
# @DESCRIPTION 
#                   Print the timestamps of each middle night
#                       that from 2016.04.27 to [timestamp] .
# @EXAMPLE 
#                   CalcTime `date +%s` .
#   
###################################################################
function CalcTime() {
    local now=$1 
    local start=1461686400 #2016.04.27 00:00:00
    while [[ ! $start -gt $now ]] ; 
    do
        echo "$start "
        ((start+=86400))   # + 3600*24
    done
}

###################################################################
#
# @SYNOPSIS
#                   ExecCountSql [sql count command] .
# @DESCRIPTION 
#                   Exec [sql count command] 
#                       and just print the count number.
#
# @EXAMPLE 
#                   ExecCountSql "select count(*) from t_account;"
#
###################################################################
function ExecCountSql() {
    local cmd="$1"
    local count=`./exesql.sh "$cmd" | grep -v count`
    echo  $count
}


function PullData(){
    local now=`date +%s`
    local all_stamp=(`CalcTime  $now`)
    local size=${#all_stamp[*]}
    local cmd_r="select count(*) from t_account where channel=\"324\" and register_time > ${all_stamp[(($size-1))]};"
    ExecCountSql "$cmd_r"
    echo " " 
    local index=$size
    while [[ $index -gt 1 ]] ; 
    do
        local cmd="select count(*) from t_account where channel=\"324\" and register_time < ${all_stamp[(($index-1))]} and register_time>  ${all_stamp[(($index-2))]} and last_logoff > ${all_stamp[(($size-1))]};"
        echo " " 
        ((index-=1))
    done
}
PullData 

