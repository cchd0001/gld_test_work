#include "units_windows.hpp"
#include "SDL/SDL.h"
#include "units_error.hpp"

namespace Units
{
    const UnitsWindows::WFLAGS UnitsWindows::SCREEN_INVISIBLE = 0x00000001;
    const UnitsWindows::WFLAGS UnitsWindows::SCREEN_DEFAULT   = 0x00000002;
    const UnitsWindows::WFLAGS UnitsWindows::SCREEN_FULL      = 0x00000004;

    const UnitsWindows::WFLAGS UnitsWindows::SDL_FLAG_TIMER   = 0x00010000;
    const UnitsWindows::WFLAGS UnitsWindows::SDL_FLAG_CDROM   = 0x00020000;
    const UnitsWindows::WFLAGS UnitsWindows::SDL_FLAG_JOYSTICK= 0x00040000;
    const UnitsWindows::WFLAGS UnitsWindows::SDL_FLAG_ALL     = 0x00080000;
    const UnitsWindows::WFLAGS UnitsWindows::COLOR_FLAGS_16   = 0x10000000;
    const UnitsWindows::WFLAGS UnitsWindows::MOUSE_FLAGS_DIS  = 0x20000000;

    bool UnitsWindows::SetFlags(const UnitsWindows::WFLAGS flag)
    {
        if(is_flag_conflict(flag))
            return false;
        m_Flags |= flag ;
        return true;
    }
    
    bool UnitsWindows::is_flag_conflict(const UnitsWindows::WFLAGS flag)
    {
        return bool(m_Flags & SCREEN_INVISIBLE);
    }
    
    bool UnitsWindows::Show()
    {
        int r, g, b, d, bpp;
        if (m_Flags & COLOR_FLAGS_16)
        {
            //16bit color
            r =  b = 5;
            g = 6;
            d = 16;
            bpp = 16;
        }
        else
        {
            //24 (true color) bit color
            r = b = g = 8;
            d = 16;
            bpp = 24;
        }

        if(m_WindowsInstance == 0) // First Show
        {
            WFLAGS sdl_flags = 0x00000000;
            if(m_Flags & SDL_FLAG_ALL)
                sdl_flags = SDL_INIT_EVERYTHING;
            else 
            {
                sdl_flags = SDL_INIT_VIDEO | SDL_INIT_AUDIO;
                if(m_Flags & SDL_FLAG_TIMER )
                    sdl_flags |= SDL_INIT_TIMER;
                if(m_Flags & SDL_FLAG_CDROM )
                    sdl_flags |= SDL_INIT_CDROM ;
                if( m_Flags & SDL_FLAG_JOYSTICK )
                    sdl_flags |= SDL_INIT_JOYSTICK;
            }
            if(SDL_Init( sdl_flags) < 0)
                UnitsError::QuitWithoutErrno("SDL_Init error : %s\n",SDL_GetError());
            SDL_GL_SetAttribute( SDL_GL_RED_SIZE, r );
            SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, g );
            SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, b );
            SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, d );
            SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
        }
        else
        {
            SDL_FreeSurface( (SDL_Surface *)m_WindowsInstance);
            m_WindowsInstance = 0;
        }
        WFLAGS video_flags = SDL_OPENGL | SDL_RESIZABLE;
        if (m_Flags & SCREEN_FULL)
            video_flags |= SDL_FULLSCREEN;
        if ((m_WindowsInstance = SDL_SetVideoMode(m_Width, m_Height, bpp, video_flags)) == NULL)
            UnitsError::QuitWithoutErrno("error SDL_SetVideoMode() : %s \n", SDL_GetError());
        if ( m_Flags & MOUSE_FLAGS_DIS)
            SDL_ShowCursor(0);
        else
            SDL_ShowCursor(1);
        return true;
    }

    UnitsWindows::~UnitsWindows()
    {
        SDL_FreeSurface( (SDL_Surface *)m_WindowsInstance);
        m_WindowsInstance = 0;
        SDL_Quit();
    }

}
