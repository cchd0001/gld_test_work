#ifndef __src_units_windows_hpp__
#define __src_units_windows_hpp__
#ifndef __cplusplus 
#error "Please use C++ compiler."
#endif

namespace Units
{
    class UnitsWindows
    {
        public:
            typedef unsigned int WFLAGS;

            /******     Windows Attribute  ***********/
            static const WFLAGS SCREEN_DEFAULT; 
            static const WFLAGS SCREEN_FULL ;
            static const WFLAGS SCREEN_INVISIBLE;
            //static const WFLAGS SCREEN_

            /******     SDL Subsystem Attribute  ***********/
            //Default SDL only use video and audio
            static const WFLAGS SDL_FLAG_TIMER;
            static const WFLAGS SDL_FLAG_CDROM;
            static const WFLAGS SDL_FLAG_JOYSTICK;
            static const WFLAGS SDL_FLAG_ALL;

            /******     Misc Attribute  ***********/
            static const WFLAGS COLOR_FLAGS_16 ;
            static const WFLAGS MOUSE_FLAGS_DIS;

      public:
            UnitsWindows(const unsigned int width = 600 , const unsigned int height = 800, const WFLAGS flags = SCREEN_DEFAULT)
                : m_Height(height) , m_Width(width), m_Flags(flags) ,m_WindowsInstance(0){}
            virtual ~UnitsWindows();
      public:
            bool SetFlags(const WFLAGS flags);
            bool Resize(const unsigned int height , const unsigned int width) 
            {  
                if((m_Flags & SCREEN_FULL) || (m_Flags & SCREEN_INVISIBLE )) 
                    return false;
                m_Height = height;
                m_Width = width;
                Show();
                return true;
            }
            WFLAGS GetGlags() const {return m_Flags ;}
            unsigned int GetHeight() const {return m_Height ;}
            unsigned int GetWidth() const {return m_Width ;}
            virtual bool Show();
            
        protected:
            unsigned int  m_Height;
            unsigned int  m_Width;
            WFLAGS        m_Flags;
            void*         m_WindowsInstance ;
            bool is_flag_conflict(const WFLAGS flag);
    };
}

#endif //__src_units_window_hpp__
