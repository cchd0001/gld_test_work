#include "units_windows.hpp"
#include "units_error.hpp"
#include "units_signals.hpp"
#include "unistd.h"
#include "units_glmatrix.hpp"

#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_audio.h>
#include <SDL/SDL_mixer.h>

using Units::UnitsWindows ;
using Units::UnitsError   ;
using Units::UnitsSignal  ;
using Units::UnitsGLMatrix;
using Units::Coord2D;

int intr = 1;

void intr_f(int , siginfo_t* , void *)
{  
    intr ++;
}
static void core_clean_surface(SDL_Surface * surface)
{
  unsigned int alpha_mask;
  unsigned int i;
  unsigned int *data;

  data = (unsigned int *)(surface->pixels);
  alpha_mask = surface->format->Amask;
  for (i = 0; i < surface->w * surface->h; i++)
  {
    if (!(data[i] & alpha_mask) && (data[i] & (~alpha_mask)))
      data[i] = 0;              //clean by filling with 0
    else
        data[i] |= 0xff000000;
  }
}

void DrawItems(GLuint tex_id,float x , float y , float w, float h, float z )
{
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tex_id);
    glColor3f(0.0f,0.0f,0.0f);
    glBegin(GL_QUADS);
    glTexCoord2f(0,0);glVertex3f(x ,  y-h , z);
    glTexCoord2f(0,1);glVertex3f(x,   y,    z);
    glTexCoord2f(1,1);glVertex3f(x+h, y,    z);
    glTexCoord2f(1,0);glVertex3f(x+h, y-h , z);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}
void BindSurface2Texture(GLuint *i , SDL_Surface * surface)
{
    glGenTextures(1, i);
    glBindTexture(GL_TEXTURE_2D, *i);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surface->w,
            surface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE,
            surface->pixels);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    /*  说明映射方式*/
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
}
int main()
{
    float wF = 4.0f / 800.0f  , hF = 4.0f / 800.0f ;
    UnitsSignal sig;
    sig.setupSigSegv();
    sig.reg_sigaction(SIGUSR1 , intr_f );
    UnitsWindows * window = new UnitsWindows(800, 800   );
    window->Show();
    UnitsGLMatrix matrix(Coord2D<int>(0,0),Coord2D<int>(800,800),-2.0f,2.0f,-2.0f,2.0f,10.0f,20.0f,UnitsGLMatrix::MODEVIEW);
    matrix.Resize(Coord2D<int>(0,0),Coord2D<int>(800,800));
        glShadeModel(GL_SMOOTH);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_DEPTH_TEST);
    //glEnable(GL_TEXTURE_2D);                
    glEnable(GL_CULL_FACE);
    TTF_Init();
    glClearColor(0.0f,0.0f,0.0f,0.0f);

    gluLookAt(0.0f,0.0,0.0,
            0.0,0.0,-1.0,
            0.0,1.0,0.0);

    SDL_Surface *sdl_img;
    sdl_img=IMG_Load_RW(SDL_RWFromFile("attact.png", "rb"), 1);
    if(!sdl_img) {
        printf("IMG_Load_RW: %s\n", IMG_GetError());
        return 0; 
    }
    GLuint tex_id[3];
    glGenTextures(1, &tex_id[0]);
    glBindTexture(GL_TEXTURE_2D, tex_id[0]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sdl_img->w,
            sdl_img->h, 0, GL_RGBA, GL_UNSIGNED_BYTE,
            sdl_img->pixels);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    /*  说明映射方式*/
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    SDL_FreeSurface(sdl_img);
    sdl_img=IMG_Load_RW(SDL_RWFromFile("attract.png", "rb"), 1);
    if(!sdl_img) {
        printf("IMG_Load_RW: %s\n", IMG_GetError());
        return 0; 
    }
    glGenTextures(1, &tex_id[1]);
    glBindTexture(GL_TEXTURE_2D, tex_id[1]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sdl_img->w,
            sdl_img->h, 0, GL_RGBA, GL_UNSIGNED_BYTE,
            sdl_img->pixels);
    SDL_FreeSurface(sdl_img);
        /*  控制滤波 */
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    /*  说明映射方式*/
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    /*  启动纹理映射 */

    glColor3f(0.0f,0.0f,0.0f);
    SDL_Color forecol =  {255,0,0};
    TTF_Font * font = TTF_OpenFont("helb_mid.ttf", 40);
    if(!font)
    {
        printf("TTF_OpenFont: %s\n", IMG_GetError());
        return 0; 
    }
    TTF_SetFontStyle(font, TTF_STYLE_NORMAL);
    sdl_img = TTF_RenderUTF8_Blended(font,"H",forecol);
    SDL_SetAlpha(sdl_img, 0, 0);
    core_clean_surface(sdl_img);

    SDL_SetColorKey(sdl_img, SDL_SRCCOLORKEY | SDL_RLEACCEL,   //dont copy black
                        SDL_MapRGB(sdl_img->format, 0, 0, 0));

    BindSurface2Texture(&tex_id[2],sdl_img);
    //TTF_Font * font2 = TTF_OpenFont("helb_mid.ttf", 10);
    //glDisable(GL_DEPTH_TEST);
    //

    Mix_OpenAudio( MIX_DEFAULT_FREQUENCY ,MIX_DEFAULT_FORMAT ,2 ,128);
    // load sample.wav in to sample
    Mix_Chunk *sample;
    sample=Mix_LoadWAV("sample.wav");
    if(!sample) {
        printf("Mix_LoadWAV: %s\n", Mix_GetError());
        // handle error
    }
    if(Mix_PlayChannel(-1, sample, -1)==-1) {
        printf("Mix_PlayChannel: %s\n",Mix_GetError());
        // may be critical error, or maybe just no channels were free.
        // you could allocated another channel in that case...
    }  
    while(1)
    {
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
       
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, tex_id[0]);
        glColor4f(1.0f,1.0f,1.0f,1.0f);			// 全亮度， 50% Alpha 混合
	    glBlendFunc(GL_SRC_ALPHA,GL_SRC_ALPHA);	
        glBegin(GL_QUADS);
            glTexCoord2f(0,0);glVertex3f(-1.0,-1.5,-12.5);
            glTexCoord2f(0,1);glVertex3f(-1.0,1.5,-12.5);
            glTexCoord2f(1,1);glVertex3f(1.5,1.5,  -15.5);
            glTexCoord2f(1,0);glVertex3f(1.5,-1.5 , -15.5);
        glEnd();
        glDisable(GL_TEXTURE_2D);
         glEnable(GL_BLEND);
                glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, tex_id[1]);
        glColor4f(1.0f,1.0f,1.0f,1.0f);			// 全亮度， 50% Alpha 混合
	    glBlendFunc(GL_SRC_ALPHA,GL_ONE);	
        glBegin(GL_QUADS);
            glTexCoord2f(0,0);glVertex3f(-1.5,-1.5,-10.5);
            glTexCoord2f(0,1);glVertex3f(-1.5,1.5,-10.5);
            glTexCoord2f(1,1);glVertex3f(0.0,1.5,  -10.5);
            glTexCoord2f(1,0);glVertex3f(0.0,-1.5 , -10.5);
        glEnd();
        glDisable(GL_TEXTURE_2D);
        glDisable(GL_BLEND);

         glEnable(GL_BLEND);
	    glBlendFunc(GL_ONE,GL_SRC_ALPHA);	
        DrawItems(tex_id[2], -1.0f, 0.1f, sdl_img->w * wF  ,sdl_img->h *hF,  -10 );
        glDisable(GL_BLEND);
        SDL_GL_SwapBuffers();

        if(intr ==1)
            sleep(0.5);
        else 
        {
            if(intr == 2)
            {
                window->Resize(500,500);
                intr ++;
            }
            sleep(0.5);
            if(intr > 3)
                break;
        }
    }
    SDL_FreeSurface(sdl_img);
    delete window;
    SDL_CloseAudio();
    SDL_Quit();
    return 0;
}
