#include "units_signals.hpp"

void f_3(int i)
{
   int * pZero = 0;
   *pZero = i;
}

void f_2(int i)
{
    f_3(i);
}
void  f_1(int i)
{
   f_2(i);
}
int main()
{
    Units::UnitsSignal::setupSigSegv();
    f_1(1);
    return 0;
}
