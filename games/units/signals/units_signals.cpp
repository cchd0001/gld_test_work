#include "units_signals.hpp"
#include "units_strings.hpp"
extern "C"
{
#define BUFFSIZE 1024
#define NUM_BACKTRACE_FNS 10
#include <execinfo.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
}
namespace Units{
    static int reg_sigaction(int sig_num); 
    static void log_stacktrace(void * message, const char *format, ...);
    static void signal_segv(int sig_num, siginfo_t *info, void * message);
    void UnitsSignal::setupSigSegv()
    {
        reg_sigaction(SIGSEGV , signal_segv);
        reg_sigaction(SIGILL,signal_segv);
        reg_sigaction(SIGBUS,signal_segv);
        reg_sigaction(SIGFPE,signal_segv);
        reg_sigaction(SIGTERM,signal_segv);
        reg_sigaction(SIGINT,signal_segv);
        reg_sigaction(SIGABRT,signal_segv);
        reg_sigaction(SIGUSR1,signal_segv);
    }
    static void signal_segv( int sig_num, siginfo_t *info, void * message )
    {
        switch(sig_num)
        {
            case SIGILL:
                log_stacktrace(message, "SIGILL(%lu) Illegal Instruction!\n", sig_num);
                break;
            case SIGBUS:
                log_stacktrace(message, "SIGBUS(%lu) Bus Error!\n", sig_num);
                break;
            case SIGSEGV:
                log_stacktrace(message, "SIGSEGV(%lu) Segmentation Fault!\n", sig_num);
                break;
            case SIGFPE:
                log_stacktrace(message, "SIGFPE(%lu) Floating Point Exception!\n", sig_num);
                break;
            case SIGTERM:
                log_stacktrace(message, "SIGTERM(%lu) Program Terminated!\n", sig_num);
                break;
            case SIGINT:
                log_stacktrace(message, "SIGINT(%lu) Program Interrupted!\n", sig_num);
                break;
            case SIGABRT:
                log_stacktrace(message, "SIGABRT(%lu) Program Aborted!\n", sig_num);
                break;
            case SIGUSR1:
                log_stacktrace(message, "SIGUSR1(%lu) Watchdog Abort!\n", sig_num);
                break;
        }
    }
    static void log_stacktrace(void * message, const char *format, ...)
    {
        va_list va;
        va_start(va,format);
        char buff[BUFFSIZE];
        UnitsString::vsnprint( buff, 0, BUFFSIZE, format, va );
        va_end(va);
        fprintf(stderr,buff);

        void *traceArray[NUM_BACKTRACE_FNS];
        size_t size = backtrace(traceArray, NUM_BACKTRACE_FNS);
        /*
         * First make sure backtrace makes it
         * to standard error - visual console
         * suffers from video locks and
         * therefore thread safety issues
         * if execution is redirected to error.
         */
        backtrace_symbols_fd(traceArray, size,fileno(stderr));
        fflush(stderr);
        exit(1);
    }

    int UnitsSignal::reg_sigaction(int sig_num , void (*f)(int , siginfo_t *, void * ))
    {
        struct sigaction action;
        memset(&action, 0, sizeof(action));
        action.sa_sigaction = f;
        action.sa_flags = SA_SIGINFO;
        if(sigaction(sig_num, &action, NULL) < 0) 
        {
            perror("sigaction");
            return 0;
        }
        return 1;
    }
}

