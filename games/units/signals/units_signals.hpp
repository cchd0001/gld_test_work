#ifndef __src_units_signals_hpp__
#define __src_units_signals_hpp__
#ifndef __cplusplus 
#error "Please use C++ compiler."
#endif

#include <signal.h>
namespace Units
{
    class UnitsSignal
    {
        public:
            //static void installTrace();
            void setupSigSegv();
            int reg_sigaction(int , void (*f)(int , siginfo_t *, void * ));
        public:
            UnitsSignal(){}
            ~UnitsSignal(){}
    };
}

#endif // __src_units_signals_hpp__
