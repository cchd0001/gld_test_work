#ifndef __src_units_error_hpp__
#define __src_units_error_hpp__

namespace Units // Classes and interfaces that any kind of project can use .
{
    class UnitsError
    {
    public:
        static void QuitWithErrno(const char *, ...);
        static void QuitWithoutErrno(const char *, ...);
        static void WarningWithErrno(const char *, ...);
        static void WarningWithoutErrno(const char *, ...);
      
    private:
        ~UnitsError(){}
        UnitsError(UnitsError & ){}
        UnitsError() {}
        UnitsError & operator= (const UnitsError& ); //Useless 
    };


    inline void CHECK_TRUE( bool args , const char * fmt = "" , ...) 
    {
#ifdef  CONFIG_COMPILE_DEBUG
        if(!args)
        {
           va_list others;
           va_start(others,fmt);
           UnitsError::QuitWithErrno(fmt,others)
           va_end();
        }
#endif
        return;
    }

    inline void CHECK_FALSE(bool args , const char * fmt = "" , ...)
    {
#ifdef  CONFIG_COMPILE_DEBUG
        if(args)
        {
           va_list others;
           va_start(others,fmt);
           UnitsError::QuitWithErrno(fmt,others)
           va_end();
        }
#endif
        return;
    }

}
#endif //__src_units_error_hpp__
