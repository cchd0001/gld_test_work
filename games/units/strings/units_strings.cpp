#include "units_strings.hpp"
extern "C" {
#include <stdio.h>
#include <stdarg.h>
}
namespace Units{
    size_t UnitsString::vsnprint( char *buff, size_t offset, size_t buff_size, const char *format, va_list ap )
    {
#ifdef CONFIG_COMPILE_DEBUG
        assert( buff != 0 );
        assert( format != 0 );
#endif
        va_list args;
        va_copy(args, ap);
        size_t avaliable_size =  buff_size - offset - 1;
        size_t ret_num = vsnprintf(&buff[offset], avaliable_size, format, args);
        if (ret_num >= avaliable_size) //if buffer memory overflow , discard the overflow part             
        {
            buff[buff_size - 1] = '\0'; 
            ret_num = avaliable_size;
        }
        va_end(args);
        return ret_num;
    }
    size_t UnitsString::snprint( char *buff, size_t offset, size_t buff_size, const char * format, ... )
    {
#ifdef CONFIG_COMPILE_DEBUG
        assert( buff != 0 );
        assert( format != 0 );
#endif        
        va_list args;
        va_start(args, format);
        size_t ret_num = vsnprint(buff, offset, offset, format, args);
        va_end(args);
        return ret_num; 
    }

}
