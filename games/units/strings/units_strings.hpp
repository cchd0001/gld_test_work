#ifndef __units_strings_hpp__
#define __units_strings_hpp__
#ifndef __cplusplus 
#error "Please use C++ compiler."
#endif
extern "C" {
#include <stdlib.h>
#include <stdarg.h>
}
namespace Units{

class UnitsString
{
public:
    static size_t vsnprint (char *buff, size_t offset, size_t buff_size, const char *format,  va_list ap) ;
    static size_t snprint (char *buff, size_t offset, size_t buff_size, const char * format, ... ) ;
};

}

#endif //__strings_interface_hpp__
