#include "units_languages.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;
using namespace Units;
int main()
{
    UnitsLanguages::Instance().AddLanguagePackage(LanguagePackage("P1","locale",".utf8",1)); 
    UnitsLanguages::Instance().AddLanguagePackage(LanguagePackage("P2","locale")); 
    UnitsLanguages::Instance().AddLanguagePackage(LanguagePackage("P3","locale"));

    UnitsLanguages::Instance().ChangeLanguageLocale("zh_CN");
    
    cout<<__("Hello Me")<<endl;
    cout<<__("Hello You")<<endl;
    cout<<__("Hello Your")<<endl;
    return 0;
}
