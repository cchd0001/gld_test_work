#ifndef __src_utils_languages_hpp__
#define __src_utils_languages_hpp__
#ifndef __cplusplus 
#error "Please use C++ compiler."
#endif

#include <string>
#include <vector>

namespace Units{

   
    class LanguagePackage
    {
        protected:
            std::string  m_packageName;
            std::string  m_packagePath;
            unsigned int m_priority;
            std::string  m_coding;
            static const std::string defaultCoding;
        public:
            enum
            {   // For priority, normally we use PRIORITY_NORMAL.
                // Use other 2 only when translation confilict between different name domain.
                PRIORITY_NORMAL = 0,
                PRIORITY_1,
                PRIORITY_2,
            };
            LanguagePackage(std::string const &packageName, std::string const &packagePath,
                    std::string const &coding = defaultCoding, unsigned int priority = PRIORITY_NORMAL )
                :m_packageName(packageName) , m_packagePath(packagePath), m_priority(priority),m_coding(coding){}

            LanguagePackage(LanguagePackage const & another)
                :m_packageName(another.m_packageName) , m_packagePath(another.m_packagePath), m_priority(another.m_priority),
                 m_coding(another.m_coding){}
            
            LanguagePackage & operator = (LanguagePackage const & another)
            {
                if(this != &another)
                {
                    m_packagePath = another.m_packagePath;
                    m_packageName = another.m_packageName;
                    m_priority = another.m_priority;
                    m_coding      = another.m_coding;
                }
                return *this;
            }
            virtual ~LanguagePackage(){}
        public:
            bool SetPriority(unsigned int prio , bool forceSet)  
            {
                if(forceSet || prio >= m_priority)
                {
                    m_priority = prio ;
                    return true;
                }
                return false;
            }
            unsigned int  GetPriority()const 
            {
                return m_priority;
            } 
            bool operator == (LanguagePackage const & another) const
            {
                if(m_packageName != another.m_packageName)
                    return false;
                if(m_packagePath != another.m_packagePath)
                    return false;
                if(m_coding != another.m_coding)
                    return false;
                return true;
            }
            bool operator < (LanguagePackage const & another) const 
            {
                return m_priority < another.m_priority;
            }

            bool SetLocale(std::string const & locale);            
            std::string GetText(std::string const & eng);
    };

    class UnitsLanguages   
    {
        // Singleton :
        public:
           static UnitsLanguages & Instance(){static UnitsLanguages _instance; return _instance; }
           static UnitsLanguages * InstancePtr(){return &Instance();}
        private:
           UnitsLanguages(){};
           UnitsLanguages(UnitsLanguages const & );
           UnitsLanguages & operator=(UnitsLanguages const & );
           static std::vector<LanguagePackage> m_packageList;
        public:
           bool AddLanguagePackage(LanguagePackage const &);
           bool RemoveLanguagePackage(LanguagePackage const &);
           bool ChangeLanguageLocale(std::string const & locale);
           //std::string GetStringLocale(const std::string & packageName , std::string &str );
           //std::string GetStringLocale(const std::string &str );   
           std::string Translate2Locale(std::string const & english);
    };

    inline std::string __(const std::string & str ){return UnitsLanguages::Instance().Translate2Locale(str);}
}

#endif //__src_utils_languages_hpp__
