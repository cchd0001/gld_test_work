#include "units_languages.hpp"
extern "C"
{
#include <libintl.h>
#include <locale.h>
#include <stdlib.h>
}

#include <vector>
#include <algorithm>
namespace Units
{
    const std::string LanguagePackage::defaultCoding(".utf8");

    bool LanguagePackage::SetLocale(std::string const & locale)
    {
        std::string localeEnv(locale);
        localeEnv.append(m_coding); 
        setenv("LANG", localeEnv.c_str(), 1);
        setlocale(LC_ALL, "");
        bindtextdomain(m_packageName.c_str(), m_packagePath.c_str());
        textdomain(m_packageName.c_str());
        
        return true;
    }

    std::string LanguagePackage::GetText(std::string const & eng)
    {
        std::string buff;
        buff = dgettext(m_packageName.c_str(),eng.c_str());
        return buff;
    }

    std::vector<LanguagePackage> UnitsLanguages::m_packageList;
    typedef std::vector<LanguagePackage>::iterator Iterator;
    bool UnitsLanguages::AddLanguagePackage(LanguagePackage const & package)
    {
        Iterator it = find(m_packageList.begin(), m_packageList.end(),package);
        if(it == m_packageList.end())
            m_packageList.push_back(package);
        else if((*it) < package)
            (*it).SetPriority(package.GetPriority(),false);
        else
            return false;
        sort(m_packageList.begin(), m_packageList.end());
        return true;
    }

    bool UnitsLanguages::RemoveLanguagePackage(LanguagePackage const & package)
    {
        Iterator it = find(m_packageList.begin(), m_packageList.end(),package);     
        if(it != m_packageList.end() )
        {
             m_packageList.erase(it);
             return true;
        }
        return false;
    }
     
   // bool RemoveLanguagePackageByName(const std::string packageName)
   // {
   //     
   // }
   
    bool UnitsLanguages::ChangeLanguageLocale(std::string const & locale)
    {
        Iterator it ;
        for(it = m_packageList.begin() ; it != m_packageList.end() ; it++ )
            (*it).SetLocale(locale);
        return true;                
    }

    std::string UnitsLanguages::Translate2Locale(std::string const & english)
    {
        std::string original(english);
        std::vector<LanguagePackage>::reverse_iterator it ;
        for(it = m_packageList.rbegin(); it != m_packageList.rend(); it ++)
        {
             std::string retString;
             retString = (*it).GetText(original);
             if(retString != original)
                 return retString;
        }
        return original;
    }

}
