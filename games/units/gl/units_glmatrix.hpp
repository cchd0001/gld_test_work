#ifndef __units_gl_units_glmatrix_hpp__
#define __units_gl_units_glmatrix_hpp__
#ifndef __cplusplus 
#error "Please use C++ compiler."
#endif

namespace Units{
    template<typename T>
    struct Coord2D
    {
        T x;
        T y;
        Coord2D(const T x,const T y):x(x),y(y){}
    };


    template<typename T>
    struct Coord3D{
        T X;
        T y;
        T z;
    };
    class UnitsGLMatrix
    {
        public:
            enum
            {
                PROJECTION = 0x01,
                MODEVIEW=0x02,
                BOTH = 0x03     
            };
            

        public :
            UnitsGLMatrix(Coord2D<int> left_bottom , Coord2D<int> right_top , float x1,float x2,float x3,float x4,float x5,float x6, int type)
            :m_LB(left_bottom), m_RT(right_top), m_Type(type) ,
            left(x1),
            right(x2),
            bottom(x3),
            top(x4),
            near(x5),
            far(x6)
        {}
           virtual ~UnitsGLMatrix(){}
           void Resize(Coord2D<int> left_bottom , Coord2D<int> right_top);
        private:
            
           Coord2D<int> m_LB;
           Coord2D<int> m_RT;
           int m_Type;
           float left;
           float right;
           float bottom;
           float top;
           float near;
           float far;
           void set_gl();
    };
}


#endif //__units/gl/units_glmatrix_hpp__
