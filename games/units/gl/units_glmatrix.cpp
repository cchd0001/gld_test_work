#include "units_glmatrix.hpp"
#include <GL/gl.h>
namespace Units
{
    void UnitsGLMatrix::Resize(Coord2D<int> left_top , Coord2D<int> right_bottom )
    {
        m_LB = left_top; m_RT = right_bottom;                       
        set_gl();
    }

    void UnitsGLMatrix::set_gl()
    {
        glViewport(m_LB.x , m_LB.y , m_RT.x, m_RT.y);
        if(m_Type & PROJECTION)
        {
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glOrtho(left, right,top ,bottom, near, far);
        }
        if(m_Type & MODEVIEW)
        {
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();
            glFrustum(left, right,top ,bottom, near, far);
        }
    }
}
