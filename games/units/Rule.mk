#
# The Units Folder , Contains all basic class .
#
UNITS_PATH := $(PLATFORM_SRC_PATH)/units

include $(UNITS_PATH)/pattern/Rule.mk
include $(UNITS_PATH)/signals/Rule.mk
include $(UNITS_PATH)/strings/Rule.mk
include $(UNITS_PATH)/multilingual/Rule.mk
include $(UNITS_PATH)/error/Rule.mk
include $(UNITS_PATH)/windows/Rule.mk
include $(UNITS_PATH)/gl/Rule.mk
