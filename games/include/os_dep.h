#ifndef __os_dep_h__
#define __os_dep_h__
#ifdef __cplusplus
extern "C" {
#endif
#ifdef _WIN32
#include <windows.h>
#else
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <getopt.h>
#include <ctype.h>
#include <dlfcn.h>
#include <time.h>
#include <stdarg.h>
#include <printf.h>
#endif


#ifdef __cplusplus
}
#endif
#endif //__os_dep_h__
