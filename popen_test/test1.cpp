#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <zlib.h>
int main()
{

#define SIZE 4000000000L 
#define USE_ZLIB 1 

    char * used = (char *)malloc(sizeof(char) *SIZE  );
    for( long i = 0 ; i< SIZE ; i++ )
    {
        used[i] = '1';
    }
    std::cout<<" 1!"<<std::endl;
    sleep(1);
    std::cout<<" 2!"<<std::endl;
    if ( USE_ZLIB )
    {
        gzFile fp = gzopen("test.gz","r");
        if( fp == NULL )
        {
            std::cout<<"zlib err !"<<std::endl;
        }
        else
        {
            std::cout<<"zlib corr!"<<std::endl;
            char buff[1024];
            while( gzgets(fp,buff,1024) != NULL )
            {
                std::cout<<buff;
            }
        }
    }
    else
    {
        FILE * fp = popen("gzip -dc test.gz","r");
        sleep(1);
        std::cout<<" 3!"<<std::endl;
        if( fp == NULL )
        {
            std::cout<<" err !"<<std::endl;
        }
        else
        {
            std::cout<<" corr!"<<std::endl;
            char buff[1024];
            while( fgets(buff,1024,fp) != NULL )
            {
                std::cout<<buff;
            }
        }
    }
    return 0 ;

}

