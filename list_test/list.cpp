#include <list>
#include <vector>
#include <iostream>
#include <algorithm>

std::list<int> m_plans;

bool add(int node){
    auto itr_big = std::upper_bound(m_plans.begin(), m_plans.end(),node);
    if(itr_big == m_plans.begin()) {
        if(*itr_big == node){
            m_plans.erase(itr_big);
        }
        m_plans.insert(m_plans.begin(),node);
        
    }else{
        auto itr = m_plans.begin();
        do{
            auto prev = itr;
            ++itr;
            if( (*prev) == node ){

                m_plans.erase(prev);
            }
        }while(itr!= itr_big);
        m_plans.insert(itr_big,node);
    }
    std::cout<<"The list have : ";
    for(auto itr = m_plans.begin() ; itr != m_plans.end() ; ++itr){
        std::cout<<(*itr)<<" ";
    }
    std::cout<<std::endl;
}


int main(){
    m_plans.clear();
    std::vector<int> buf = { 1, 1,8, 4, 3,4,4,7,7,9,6,5,2 ,9,9};
    for(int i =0 ; i< buf.size() ; i++){
        add(buf[i]);
    }
    std::cout<<"The list have : ";
    for(auto itr = m_plans.begin() ; itr != m_plans.end() ; ++itr){
        std::cout<<(*itr)<<" ";
    }
    std::cout<<std::endl;
    return 0;
}
