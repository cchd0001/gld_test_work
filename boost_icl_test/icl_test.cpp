#include <boost/icl/interval.hpp>
#include <boost/icl/interval_map.hpp>
#include <boost/icl/interval_set.hpp>
#include <boost/icl/split_interval_set.hpp>
#include <boost/icl/separate_interval_set.hpp>
#include <iostream>
#include <string>

typedef int seconds;

typedef std::string guests;
int main() {
    boost::icl::interval<double>::closed(1.0f, 3.0f) ;
    boost::icl::interval_map< int , std::set<std::string> > prizes ;
    auto a = boost::icl::interval<int>::closed(1,4);
    auto b = boost::icl::interval<int>::closed(2,3);
    std::cout<< a.lower()  <<std::endl;
    std::cout<< a.upper() <<std::endl;
    boost::icl::interval_map<int, int> overlapCounter;
    auto night_and_day = boost::icl::interval<int>::closed(1,4); 
    overlapCounter += make_pair(night_and_day,1);
    std::cout<<overlapCounter<<std::endl;
    boost::icl::interval_map<int, std::string> overlapCounter1;
    auto night_and_day1 = boost::icl::interval<int>::closed(1,4); 
    overlapCounter1 += make_pair(night_and_day1,std::string("1"));
    overlapCounter1 += make_pair(night_and_day1,std::string("2"));
    for(const auto & i : overlapCounter1) {
        std::cout<<i.first.lower() << " -- "<<i.first.upper() << " <<<>>> " <<i.second<<std::endl;
    }
    std::cout<<overlapCounter1<<std::endl;
    boost::icl::interval_map<int, std::vector<std::string>> overlapCounter2;
    std::set<std::string> aa ;
    aa.insert("1");
    std::set<std::string> bb;
    bb.insert("2");
    overlapCounter2 += make_pair(night_and_day1,a);
    overlapCounter2 += make_pair(night_and_day1,b);
    for(const auto & i : overlapCounter1) {
       // std::cout<<i.first.lower() << " -- "<<i.first.upper() << " <<<>>> " <<i.second<<std::endl;
    }
    std::cout<<overlapCounter2<<std::endl;

    //overlapCounter += make_pair(day_and_night,1); //overlapping in 'day' [07:00, 20.00)
    //overlapCounter += make_pair(next_morning, 1); //touching
    //overlapCounter += make_pair(next_evening, 1); //disjoint
/*
    boost::icl::interval_set<int> party;
    party += boost::icl::interval<int>::right_open(1, 3);
    party += boost::icl::interval<int>::right_open(2, 3);
    party += boost::icl::interval<int>::left_open(3, 4);
    party += boost::icl::interval<int>::right_open(4, 6);

     boost::icl::separate_interval_set<int> party1;
    party1 += boost::icl::interval<int>::right_open(1, 3);
    party1 += boost::icl::interval<int>::right_open(2, 3);
    party1 += boost::icl::interval<int>::right_open(2, 4);

    party1 += boost::icl::interval<int>::left_open(5, 7);
    party1 += boost::icl::interval<int>::right_open(6, 9);
    for(const  auto & i : party ) {
        std::cout<<" "<<i<<std::endl;
    }
    std::cout<<"-------------------------"<<std::endl;
    for(const  auto & i : party1 ) {
        std::cout<<" "<<i<<std::endl;
    }

    boost::icl::interval_map<int , guests> party3;
    party3+=make_pair(boost::icl::interval<int>::left_open(0,1) , "01"); 
    party3+=make_pair(boost::icl::interval<int>::left_open(1,2) , "02"); 
    party3+=make_pair(boost::icl::interval<int>::left_open(2,4) , "03"); 
    party3+=make_pair(boost::icl::interval<int>::left_open(3,5) , "04"); 
    party3+=make_pair(boost::icl::interval<int>::left_open(4,6) , "05"); 
    party3+=make_pair(boost::icl::interval<int>::left_open(5,7) , "06"); 

*/
    return 0;
    /*
    interval<seconds>::type talk_show(make_seconds("22:45:30"), make_seconds("23:30:50"));
    interval_set<seconds> myTvProgram;
    myTvProgram.add(news).add(talk_show);

    // Iterating over elements (seconds) would be silly ...
    for(interval_set<seconds>::iterator telecast = myTvProgram.begin();
            telecast != myTvProgram.end(); ++telecast)
        //...so this iterates over intervals
        TV.switch_on(*telecast);
        */
}
